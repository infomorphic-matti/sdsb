# sdsb - Self-Determination for Secure Boot
This program is designed to provide a simpler framework for creating kernels with different initramfs generators (such as [`mkinitcpio`][mkinitcpio], [`dracut`][dracut] and [`booster`][booster]), embedding them in [Unified Kernel Images][uki], and generating a configured bootloader to load these images from boot. 

It uses [`sbctl`][sbctl] (or potentially some other framework) to use your own Secure Boot keys to lock down the entire boot chain. The idea is to do this in a relatively declarative manner that can ideally replace the various messy ad-hoc scripts used for generating bootloader configurations which have mixed support for non-mkinitcpio generators on Arch or other distributions.

This program is significantly influenced by the contents of the [Controlling Secure Boot][control-sb] page by Rod Smith.

Contributions to add compatibility for other platforms (various BSDs or other stranger OSes) is very welcome, though I'm unlikely to be able to test them myself. Contributions must be made under a license compatible with `GPL-v3.0-or-later`.

# Architecture 
SDSB is designed as a full solution for declarative statement of boot structure, initramfs generation, as well as automatic integration of secureboot signing and key management. It is furthermore designed to allow modifying components of objects of the same type piecewise - e.g. creating a platform for Manjaro Linux installs by creating a config file that adjusts a specification for Arch Linux.

The data and configuration for this program is defined by different *object types*. It uses the [XDG Base Directory Standard][xdg-base] to provide an override hierarchy. Object types categorised as data definitions are stored in `$XDG_DATA_DIRS` and `$XDG_DATA_HOME` and resolved with `$XDG_DATA_HOME` as the highest priority and then the other data directories. Object types categorised as config definitions are stored similarly except in `$XDG_CONFIG_HOME` and `$XDG_CONFIG_DIRS`. 

Data object types typically define a category of something or information on how to call a given program in a generic way. Config data objects typically provide information on the specifics of an individual system, usually parametrizing a specific named data-type object. For example:
* A data-type object might be called something like `initramfs/mkinitcpio.toml`, and provide information on how to call `mkinitcpio` (an initramfs generator) with a specific config file/dir location and output location, how to detect if it's installed, and it's capabilities (e.g. "can you set the kernel command line in the call to the program or is it embedded in the config file").
* A config-type object might provide information on a linux install and provide parameters for various different types of initramfs generators, e.g. the commandline to boot the system, or the location of config files for specific initramfs generators.
* This program would then use this information to orchestrate the construction of an initramfs image (or several) in a controlled manner to then be combined with other things like the linux kernel versions or similar. 

Specified objects have a path relative to the relevant top-level directory inside their XDG resolution path. These paths are separated by dots, and are "namespaced" on the object type (i.e. two objects from different object-types can have the "same" path, and it will refer to different things, though it's not recommended to do this). 

Each object is resolved via an overload mechanism applied from the lowest-priority TOML file in it's path upward towards the highest-priority toml file in it's path. Furthermore, objects can inherit from other objects - only the highest-priority specified inheritance within a *single* object path is applied, and it means the object definitions from the inherited object are essentially applied "below" the bottommost part of this object specification. For full information on the override and inheritance semantics, see [the Override Semantics section of this document](#override-semantics).

The available data-type object types are:
* [Platforms](#platforms) - data directory is `platform`
    Roughly corresponds to "something that acts as the main component in a bundled, signable EFI executable", or rather, a series of mechanisms to specify the method of locating all needed components to generate such a thing.
* [Platform-Specific Data Types](#platform-specific-data) - data for specific platforms:
    * Linux-specific data types - data types for Linux platforms:
        - [`initramfs` Generators](#platform-specific-data-linux-initramfs-generators)
            The data directory for this is `platform.specific/linux/initramfs-gen`. These provide a mechanism for generating an initramfs image.


# Architecture - Standard Layout
When defining data-types (and config types), whether using the inheritance system or not, it makes sense to arrange them in a specific and consistent manner. To do so, we lay out standards for naming and file structure:
* All names should be all-lowercase and kebab-case. That is, they should look like `linux`, `some-other-name-here`, `test-name-3`. In particular, spaces and other special characters that aren't `_` should be replaced with a dash (and if it is ok, replace underscores with dashes as well).
* Objects which are conceptually derived from each other or "subcategories" of each other should be in subfolders - even if they don't use the inheritance/override mechanism directly.
* In the data folders of various object types (like `platform` or similar), if the platform has multiple "variants" that it's allowed to be (e.g. how platforms might be "linux" or "bootloader"), then each possible type should have it's own top-level folder.
* If the data object type doesn't have top level "variants" then a top-level set of subfolders is unnecessary.

This means that, for example, the following layout inside one of the platform folders in the data-type object path might be used:

```txt
platform/
- linux/
  - arch/
    - manjaro.toml
  - arch.toml
  - debian/
    - mint-debian-edition.toml
    - ubuntu/
      - mint.toml
  - debian.toml
  - gentoo.toml
  - fedora.toml
- bootloader/
  - refind.toml
  - grub.toml
```

Then, when you need to specify a platform, it could be referred to, for example, like follows:
* `linux.arch` for Arch Linux
* `linux.arch.manjaro` for Manjaro Linux
* `bootloader.refind` for the rEFInd bootloader
- `linux.debian.ubuntu.mint` and `linux.debian.mint-debian-edition` for normal Linux Mint and Linux Mint Debian Edition 

Note that even though this layout lacks information on `linux.debian.ubuntu` itself (there is no `ubuntu.toml`), the toml file for `linux.debian.ubuntu.mint` was still put inside a directory. This is important to make sure that if in future you bundle more platforms, you avoid naming conflicts and general weirdness.

Furthermore, note that this doesn't impose any requirement on, for example, the two Linux Mint specification files to inherit from the associated `linux.debian.ubuntu` or `linux.debian` specifications. They might, but the directory structure imposes no intrinsic constraints on overriding or inheritance from other paths. It just makes the actual structure much more organised and coherent.

## Note on *x*-specific data-types
There are various types of data-objects which are only relevant to some subset of another object type. For instance, `initramfs` generators are specific to Linux platforms only. There may in future be instances where some data-object types are only relevant to a subset or a subset of another.

Any object-specific data object type has a object-directory path that looks like the following, sharing a root directory with the object type containing the subset it is relevant to:
`[top-level-obj-type].specific/[subset-identifier]/[subset-relevant-object-type]`

For example, initramfs generator object definitions are located in `platform.specific/linux/initramfs-gen/`. A hypothetical object type specific to initramfs generators would have it's root directory suffix at `platform.specific/linux/initramfs-gen.specific/[subset-specifier]/[highly-specific-object-type]` 

# Platforms
A *platform* represents a type of system, which roughly corresponds to where relevant files are, the processes required to turn that platform into thinks like kernel image locations or how to apply initramfs to it (for linux installs), generation programs, etc.

For example, one platform would be *Arch Linux*. It's properties include, for example, the fact that the kernel data, modules, images, versions, etc. are included in the location `/usr/lib/modules/<kernel-version>`, or the location of tools to combine a kernel, commandline, and splash image to produce a unified kernel image, or the location of intel and AMD microcode - as well as what other platforms or platform types it can manage.

Platforms can have different types. For example, they can be of type `linux` or `bootloader` or potentially other things, and these have different parameters - "searching for kernels" is only relevant to the construction of EFI images for linux installs, for example.

Each platform can also document it's ability - or rather, describe mechanisms to determine if an install of it *has* that capability - to construct images of other types of platform or specific platforms.

For example, an arch linux install may be able to construct rEFInd or GRUB or systemd-boot images (depending on if they're installed). Similar conditional things may be used for platform-specific information like initramfs generators or UKI generators, as well as the more generic question of how to determine if one install of a given platform can construct images for another install of a given platform.

This program should be bundled with platform information. An example of the general mechanism for determining where each platform specification should be placed can be seen in [the section on standard layout](#architecture-standard-layout).

# Platform-Specific Data
Platform specific data object types are very important. They're often necessary to complete the ability for a given platform installation to generate images for itself or other platforms.

## Platform-Specific Data (Linux) - initramfs generators
InitRAMFS is the system that most linux distributions use to boot (it's an evolution of `initrd`). It provides infrastructure to load the root filesystem, and often also contains things like kernel modules needed for early boot.

Existing systems typically generate the initramfs periodically, and it usually is done on every kernel update because of the need to embed kernel modules. `initramfs` generators as specified within this program 

# Installs
Each *install* represents an instantiation of a specific platform - for example, if you have one install of arch linux, and one install of linux mint, then each one of those represents an instantiation of a specific platform. Typically, installs are associated with some kind of mountpoint or location, but may also be associated with a specific file (for example in platform types which act as a shim for an opaque EFI image). This depends on types of images. 

Installs may or may not be capable of generating EFI images (or otherwise processing or generating them) for installs of a different platform. This may differ for each install of a specific platform.


# Data Object Components 
Lots of data object file types have various common components associated with them. For instance, many config files need a coherent way to specify how to locate and run an executable on a given platform, or need a generic way to translate various parameters they have into arguments for an executable.

To avoid repetition, the documentation above references several generic types of objects that represent a fixed sub-component of configuration.

## Data Object Components - Executable
An executable component is a structure that describes how to run an executable on an installed system. Different systems might put executables in different places, or their location might be unconventional, so this provides a way to specify that.

This can be specified in two alternate forms:
* `single` form - a table with a key "single" set to the name and/or path of the program to run. This is automatically postfixed onto `/usr/bin/env` or an equivalent for the relevant system (within the root of the system we want to execute this executable). If the path or name you provided is an absolute path, then it will automatically be re-rooted to the system where this executable is to be located.
* `extended` form - a table with a key "extended" containing an array that is used as the start of the commandline. It is also passed to `/usr/bin/env` or the system-equivalent for a given run location. Unlike in `single` mode, this mechanism doesn't parse paths that have been passed in to reroot them appropriately (so you should always use relative paths). This is because, unlike `simple` mode, the arguments here can be all kinds of things other than some form of relative or absolute path or name.

# Override Semantics
There are two forms of override file:
* Same-Object override files. These are files in the XDG data/config paths for a single object name. They have a top level key `override=true`, and means the files act as overrides for the previous config built from lower priority files, rather than replacing them entirely. The lowest-priority file cannot be an override (you will get a warning and the base file will be parsed as-if it is not an override file).
* Inheritance files. These are marked with a top-level key of `inherit="<object path of other object of same type>"`. This allows one object to build on top of another existing one, and even bottom-level files in the XDG config/data paths are allowed. Cycles are not allowed - an object can't inherit from itself, even indirectly. 

Inheritance acts more like a property of the object than overrides - that is, overrides are applied *before* an inheritance is applied, so only the highest-priority inheritance is applied. In particular, the correct inheritance for an object is identified first, then constructed as a "prerequisite chain" of the current object's stack of override files as well as the latest base file (this occurs recursively - inheritance is unbounded as long as there are no cycles). 

Inheritance of an object implies inheriting from the object constructed from all override files for that object.

Override files have some unique behaviour:
* All details (and in particular, certain constants) can be found in [`crate::structured_delta::constants`], for up-to-date info, in the case that these docs go out of date and you can't get things working.
* Optional entries in base files typically are indicated via the presence or absence of a field in a table. However, in override files, an absence of a field just means it isn't overwritten. To indicate deletion of a field, set it to a string value of "<DEL>".
* Fields and subfields where the name of the table indicates one of several possible structures for the inner value - such as the top-level `[linux]` or `[bootloader]` key in platform files - have slightly more complex override behaviour. 
   * If the key is used in an override file, and it's the *same* as the previous accumulation, it acts as override. 
   * If it's *different* to the structure it's applying to, then it acts like a new base file structure (and you need to fill in fields like it is).
   * If one file switches to a new key, and another goes back to the original, this acts like a reset. It's as if the first key specification doesn't exist. 
   * For people familiar with `serde`, this is essentially the idea that the delta mechanism here works on externally tagged enums. Other types of enum - which embed the tag as a key in a table or sequentially try possible variants - behave like raw maps.
* Sequential items (toml arrays) present a more complex override mechanism. Different array fields might be sensibly overridden in different ways:
   * Some might be best overridden as a simple "each override provides a new value" system - that is, overriding the whole array at once
   * Some are best overridden by appending values on the end or on the start
   * Some are best treated as "sets" of values that are order-independent with values being added or removed.
   * Sometimes you want to laser-focus on a specific value by index

   This is further complicated by the fact that arrays may contain different types of value - tables, strings, numbers, etc.

   To solve this, the override mechanism used by this program follows structured behaviour. For base files, defining arrays is simple - just set the values. For override files, array keys that are overridden can take several forms:
   * Normal deletion - normal deletion always applies to array fields like any other.
   * Simple array override - just setting the key to a new array value will override the whole array.
   * Map override - this allows setting the array key to a table, and then using the keys in that table to dictate a mechanism to modify the base array:
       * Map overrides can exist in two different modes - `set` mode, and `array` mode.
       * If the key `"set"` is set to `true`, then the override mode is `set`. If it's set to `false` or unspecified, the override mode is `array`.
       * When performing overrides on a single array - that is, without it being overridden with a simple array override - only one of these modes is allowed. Switching halfway through will result in an error. This is to prevent user error.
       * In `array` mode: 
           * First, any numerical keys (of the form `"0"`, `"1"`, `"6"`, ..., etc.) within the length of the existing array to be overridden, are checked for. Any that are present act just like map overrides - including the ability to delete via the use of `"<DEL>"`, which means the value at the given index will be deleted from the array.
           * The keys `"lstrip"` and `"rstrip"`, if set to numbers, can be used to remove the first and last `k` values. 
           * The key `"append"` can be set to an array of items to put on the end of the array, and `"prepend"` can be set to an array of items to put on the start of the array.
           * All deletions are performed simultaneously - that is, if you delete an index via `"<DEL>"`, and also delete it via `"lstrip"` and `"rstrip"`, you don't need to worry about indices shifting out from under you. Both should be conceptualised as applying to the array before any modifications happen.
        * In `set` mode:
           * The elements of the array must all be simple values (not tables). Must be: strings, integers (NOT decimal values/floats), or booleans.  
           * The key `"insert"` can be set to an array of items that should be inserted into the set-array *if not already present*.
           * The key `"remove"` can be set to an array of items that should be removed from the set-array if they are present. This takes priority over `"insert"` (a key in both will be removed, not inserted).



### Platform Definitions
Platforms are stored in the `$XDG_DATA_HOME` and `$XDG_DATA_DIRS` locations as per xdg standard. The suggested layout for this data is a two level primary structure, where the first level is the platform type (`linux`, `bootloader`, etc.), and the second level contains config files like `arch.toml` or `refind.toml`. Directories can exist like `arch/`, which allows derivative distributions (e.g. Manjaro) to be placed in nested subdirectories accordingly, e.g. `linux/arch/manjaro.toml` can exist simultaneously as `linux/arch.toml`. They are both completely independent, but this provides a useful organisational structure which should be followed as much as possible for simplicity and uniformity for users.



[mkinitcpio]: https://wiki.archlinux.org/title/Mkinitcpio
[dracut]: https://wiki.archlinux.org/title/Dracut
[booster]: https://wiki.archlinux.org/title/Booster
[sbctl]: https://github.com/Foxboron/sbctl

[uki]: https://wiki.archlinux.org/title/Unified_kernel_image
[control-sb]: https://www.rodsbooks.com/efi-bootloaders/controlling-sb.html

[xdg-base]: https://wiki.archlinux.org/title/XDG_Base_Directory

