//! Module providing parsing for the CLI using BPAF. Includes lots of help information and doc
//! generation

use core::borrow::Borrow;

use bpaf::{construct, Parser};
use slog::Logger;

use crate::logutils::RunningCommandPath;

pub mod analyze;
pub mod gendocs;
pub mod search_path_specifiers;

pub const VERSION: &str = env!("CARGO_PKG_VERSION");
pub const PROGRAM_NAME: &str = env!("CARGO_BIN_NAME");

/// Provides a sidechannel into each command parser creation function recursively. Used at the top
/// level.
pub mod sidechannel {
    /// This indicates what purpose the constructor functions for a subcommand are performing. In
    /// particular, it allows hooking in to the constructor functions to generate docs :)
    pub trait RawSubcommandDefinitionHook {
        /// Seed needed for initial creation.
        type Seed;
        type SubcommandHandler<'a>: RawSubcommandDefinitionHook<Error = Self::Error>
        where
            Self: 'a;
        type Error;

        /// Create a top-level of this
        fn root_command(command_name: &'static str, seed: Self::Seed) -> Self;

        /// Indicate that a subcommand of the currently-being-defined command - with the given name - is about to be
        /// defined. Return a new handler for further subcommands of that command.
        fn starting_subcommand<'s>(
            &'s mut self,
            subcommand_name: &'static str,
        ) -> Self::SubcommandHandler<'s>;

        /// Finalise the current (sub)command with the [`bpaf::OptionParser`] that is about to be turned
        /// into another command :)
        ///
        /// Note that this should also be called on the part of this created with [`RawSubcommandDefinitionHook::root_command`]
        ///
        /// This is usually fallible unless you set the Error type param to ! or Infallible.
        fn end_command<Q>(self, parser: &bpaf::OptionParser<Q>) -> Result<(), Self::Error>;
    }

    /// Simplification wrapper for [`RawSubcommandDefinitionHook`] that provides an automatic
    /// management of the "stack" of subcommands. Combine with [`StackHook`]
    pub trait SubcommandStackDefinitionHook {
        /// Seed needed for initial creation.
        type Seed;
        type SubcommandHandler<'a>: SubcommandStackDefinitionHook<Error = Self::Error>
        where
            Self: 'a;
        type Error;

        /// Create the top-level stack value of this.
        fn root_command(command_name: &'static str, seed: Self::Seed) -> Self;

        /// Indicate that a subcommand of the currently-being-defined command - with the given name
        /// plus information on the current stack - is about to be defined. Return a new handler for
        /// further subcommands of this subcommand to be defined (or for it to immediately be
        /// finished).
        fn starting_subcommand<'s>(
            &'s mut self,
            current_command_stack: &[&'static str],
            subcommand_name: &'static str,
        ) -> Self::SubcommandHandler<'s>;

        /// Finalise the current command with the [`bpaf::OptionParser`] that is about to be turned
        /// into another [`bpaf::parsers::ParseCommand`] (or similar concept).
        ///
        /// This should also be called on the root command when defined. It is usually fallible unless
        /// you make it infallible.
        ///
        /// Arguments:
        /// * self - self-explanatory
        /// * path - the path to the current subcommand, which includes the root level command as
        ///     the first element
        /// * leaf - if true, indicates that there have been no further subcommands of this one.
        /// * parser - the actual options that are about to be turned into the (sub) command at
        /// `path`
        fn end_command<Q>(
            self,
            path: Vec<&'static str>,
            leaf: bool,
            parser: &bpaf::OptionParser<Q>,
        ) -> Result<(), Self::Error>;
    }

    pub struct StackHook<SimpleHook> {
        curr_stack: Vec<&'static str>,
        curr_value: SimpleHook,
        leaf: bool,
    }

    impl<SimpleHook: SubcommandStackDefinitionHook> RawSubcommandDefinitionHook
        for StackHook<SimpleHook>
    {
        type Seed = SimpleHook::Seed;
        type SubcommandHandler<'a> = StackHook<SimpleHook::SubcommandHandler<'a>> where Self: 'a;

        type Error = SimpleHook::Error;

        fn root_command(command_name: &'static str, seed: Self::Seed) -> Self {
            Self {
                curr_stack: vec![command_name],
                curr_value: SimpleHook::root_command(command_name, seed),
                leaf: true,
            }
        }

        fn starting_subcommand<'s>(
            &'s mut self,
            subcommand_name: &'static str,
        ) -> Self::SubcommandHandler<'s> {
            // A subcommand is about to be defined... no longer a leaf :)
            self.leaf = false;
            let new_stack = self
                .curr_stack
                .iter()
                .cloned()
                .chain(std::iter::once(subcommand_name))
                .collect::<Vec<_>>();
            StackHook {
                curr_stack: new_stack,
                curr_value: self
                    .curr_value
                    .starting_subcommand(&self.curr_stack, subcommand_name),
                leaf: true,
            }
        }

        fn end_command<Q>(self, parser: &bpaf::OptionParser<Q>) -> Result<(), Self::Error> {
            let StackHook {
                curr_stack,
                curr_value,
                leaf,
            } = self;
            curr_value.end_command(curr_stack, leaf, parser)
        }
    }

    impl RawSubcommandDefinitionHook for () {
        type Seed = ();

        type SubcommandHandler<'a> = () where Self: 'a;

        type Error = Infallible;

        #[inline]
        fn root_command(_command_name: &'static str, _seed: Self::Seed) -> Self {}

        #[inline]
        fn starting_subcommand<'s>(
            &'s mut self,
            _subcommand_name: &'static str,
        ) -> Self::SubcommandHandler<'s> {
        }

        #[inline]
        fn end_command<Q>(self, _parser: &bpaf::OptionParser<Q>) -> Result<(), Self::Error> {
            Ok(())
        }
    }

    use std::convert::Infallible;

    pub use self::{
        RawSubcommandDefinitionHook as Hook, SubcommandStackDefinitionHook as SimpleHook,
    };
}

/// Trait for command definitions providing an indication that they internally indicate a log
/// level.
pub trait LogLevelProvider {
    fn get_log_level(&self) -> slog::FilterLevel;
}

/// Trait for command definitions that provides e.g. information on subcommand path or otherwise.
pub trait CommandInfo {
    /// Push the path of this command onto the command path
    ///
    /// Designed with recursion in mind e.g. top-level command pushes it's own path, then whatever
    /// subcommand has been selected.
    fn append_as_subcommand(&self, command_path: &mut Vec<&'static str>);
}

/// Command parser component for setting the log levels.
pub fn log_level() -> impl Parser<slog::FilterLevel> {
    bpaf::batteries::verbose_and_quiet_by_number(4, 0, 6).map(|a| match a {
        0 => slog::FilterLevel::Off,
        1 => slog::FilterLevel::Critical,
        2 => slog::FilterLevel::Error,
        3 => slog::FilterLevel::Warning,
        4 => slog::FilterLevel::Info,
        5 => slog::FilterLevel::Debug,
        6 => slog::FilterLevel::Trace,
        _ => unreachable!("the verbose and quiet function auto-clamps")
    }).with_group_help(|existing_docs| {
        let mut wrapper = bpaf::Doc::default();
        wrapper.emphasis("Control the level of logging [default: INFO (L4), min: OFF (L0), max: TRACE (L6)]:");
        wrapper.text(" ");
        wrapper.meta(existing_docs, true);
        wrapper
    })
}

pub enum SDSBCmd {
    GenDocs(gendocs::GenDocs),
}

impl LogLevelProvider for SDSBCmd {
    fn get_log_level(&self) -> slog::FilterLevel {
        match self {
            SDSBCmd::GenDocs(v) => v.get_log_level(),
        }
    }
}

impl CommandInfo for SDSBCmd {
    fn append_as_subcommand(&self, command_path: &mut Vec<&'static str>) {
        command_path.push(PROGRAM_NAME);
        match self {
            SDSBCmd::GenDocs(v) => v.append_as_subcommand(command_path),
        }
    }
}

/// Ready-to-run SDSB options. For the top level, we use [`bpaf::OptionParser`] - parsecommand is
/// really meant for subcommands.
///
/// Uses [`PROGRAM_NAME`] as the command name.
pub fn sdsb_options<H: sidechannel::Hook>(
    seed: H::Seed,
) -> Result<bpaf::OptionParser<SDSBCmd>, H::Error> {
    let mut command_hook = H::root_command(PROGRAM_NAME, seed);
    let gendocs_command = gendocs::gendocs_cmd(&mut command_hook)?.map(SDSBCmd::GenDocs);

    let sdsb = construct!([gendocs_command])
        .to_options()
        .descr("Declaratively control the entire linux boot infrastructure and use secure-boot with your own keys.")
        .version(VERSION)
        .fallback_to_usage();
    command_hook.end_command(&sdsb)?;
    Ok(sdsb)
}

/// Actually run the top-level command.
pub fn sdsb_run<CmdLine: Borrow<[&'static str]>>(
    logger: Logger,
    command_specification: SDSBCmd,
    running_cmd: RunningCommandPath<CmdLine>,
) -> anyhow::Result<()> {
    match command_specification {
        SDSBCmd::GenDocs(information) => gendocs::gendocs_run(logger, information, running_cmd),
    }
}

#[cfg(test)]
mod test {
    use crate::cmdline;

    /// Test the bpaf cli for consistency and validity.  
    #[test]
    fn cli_validity() {
        let sdsb_parser = cmdline::sdsb_options::<()>(()).expect("This is an infallible option");
        sdsb_parser.check_invariants(true);
    }
}

// SDSB - Manage initramfs generators, kernels, and self-controlled secure boot.
// Copyright (C) 2023 Matti Bryce <mattibryce at protonmail dot com>
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <https://www.gnu.org/licenses/>.
