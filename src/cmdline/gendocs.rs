//! Module for generating documentation for all of the different subcommands and more general
//! usage.
//!
//! TODO: Build the MdHTML and PureMD documentation generators
//! TODO: Implement a method to parse markdown into [`bpaf::Doc`] hierarchical structures - this
//! would enable much better documentation generation in manual pages and similar, pulled from
//! README.md and others:
//! * Pretty sure that .doc() and .doc_em() allow adding documentation chunks that act as
//!   subsections to the current chunk - doc_em() implies the first line of the provided sub-doc is
//!   a header and it will be emphasised accordingly
//! * Build a separate library for this.
//! TODO: Contribute to [`bpaf`] a means to switch on/off the generation of all subcommand docs
//! within their parent command docs. Right now it causes significant jank in manual pages, and,
//! though I haven't implemented it for the HTML/MD stuff yet I would guess this is also the case
//! for that.   

use core::borrow::Borrow;
use std::path::{Path, PathBuf};

use bpaf::{construct, Parser};
use chain_trans::Trans;
use slog::{slog_info, slog_o};

use super::{sidechannel, CommandInfo, LogLevelProvider};
use crate::cmdline::{self, sidechannel::Hook};

/// Subcommand for documentation generation
pub const GENDOC_SUBCOMMAND_NAME: &str = "doc";

/// Mode to generate documentation.
#[derive(Debug, Clone, Copy, PartialEq, Eq)]
pub enum GenDocsMode {
    MarkdownHtml,
    PureMarkdown,
    Man,
}

/// Part of the command information to access when specifying components of help or other things for the given
/// [`GenDocsMode`] - used for declarativity and keeping information self-contained.
#[derive(Debug, Clone, Copy, PartialEq, Eq)]
enum GenDocModeSubcommandComponent {
    /// Default suffix for the output directory, when calculating default values.
    ///
    /// Note that the behaviour with manpage mode is somewhat different to that of md/html mode,
    /// but they both have a conceptual default suffix.
    OutputDirectoryDefaultSuffix,
    /// Help for the output directory argument.
    OutputDirectoryArgHelp,
    /// Description of the subcommand.
    Description,
    /// Name of the subcommand.
    SubcommandName,
}

impl GenDocsMode {
    /// Retrieve relevant information as specified by [`GenDocModeSubcommandComponent`]
    fn get_info(&self, component: GenDocModeSubcommandComponent) -> &'static str {
        use GenDocModeSubcommandComponent as Info;
        match self {
            GenDocsMode::MarkdownHtml => match component {
                Info::OutputDirectoryDefaultSuffix => "md-html",
                Info::OutputDirectoryArgHelp => concat!(
                    "Top-level output directory to generate documentation in. ", 
                    "Subcommand documentation is placed in recursive subdirectories."
                ),
                Info::Description => "Generate documentation in the form of markdown with embedded HTML, in a directory.",
                Info::SubcommandName => "md",
            },
            GenDocsMode::PureMarkdown => match component {
                Info::OutputDirectoryDefaultSuffix => "md",
                Info::OutputDirectoryArgHelp => concat!(
                    "Top-level output directory to generate documentation in. ", 
                    "Subcommand documentation is placed in recursive subdirectories."
                ),
                Info::Description => "Generate documentation in the form of pure markdown, in a directory.",
                Info::SubcommandName => "mdpure",
            },
            GenDocsMode::Man => match component {
                Info::OutputDirectoryDefaultSuffix => "usr/share/man",
                Info::OutputDirectoryArgHelp => concat!(
                    "Output directory to generate documentation in. ",
                    "When unspecified, the default for manual pages is glued on to the environment variable ",
                    "'PREFIX', as `$PREFIX/usr/share/man/`. Manual pages are put into subdirectories ",
                    "corresponding to their categories."
                ),
                Info::Description => "Generate documentation in the form of manual pages, in a directory",
                Info::SubcommandName => "man",
            },
        }
    }

    /// Get the fallback output directory by default for this type of doc generation.
    ///
    /// May pull from the system environment (PREFIX, if present) in the case of ManPages
    fn fallback_output_directory(&self) -> PathBuf {
        use GenDocModeSubcommandComponent as Info;
        let default_suffix: &Path = self
            .get_info(Info::OutputDirectoryDefaultSuffix)
            .trans(Path::new);
        match self {
            GenDocsMode::MarkdownHtml | GenDocsMode::PureMarkdown => default_suffix.to_owned(),
            GenDocsMode::Man => {
                let mut prefix =
                    PathBuf::from(std::env::var_os("PREFIX").unwrap_or_else(|| "/".into()));
                prefix.push(default_suffix);
                prefix
            }
        }
    }
}

pub struct GenDocs {
    pub mode: GenDocsMode,
    /// Output directory to write the documentation to. Make sure to prefix each relevant component
    /// appropriately.
    pub output_directory: PathBuf,
    pub include_root: bool,
    pub include_non_leaves: bool,
    pub verbosity: slog::FilterLevel,
    /// Merely warn on errors in writing files
    pub ignore_writing_errors: bool,
}

/// Make a subcommand for a specific variant of [`GenDocsMode`] that produces the full gendocs.  
pub fn make_gendocs_subcommand<SDH: sidechannel::Hook>(
    subcommand_mode: GenDocsMode,
    sdh: &mut SDH,
) -> Result<bpaf::parsers::ParseCommand<GenDocs>, SDH::Error> {
    use GenDocModeSubcommandComponent as Info;
    let name = subcommand_mode.get_info(Info::SubcommandName);
    let subcommand_hook = sdh.starting_subcommand(name);
    let output_directory = bpaf::short('o')
        .long("output-directory")
        .argument::<PathBuf>("OUTPUT_DIRECTORY")
        .help(subcommand_mode.get_info(Info::OutputDirectoryArgHelp))
        .fallback(subcommand_mode.fallback_output_directory());
    let log_level = super::log_level();
    let include_root = bpaf::long("exclude-root").flag(false, true).help("Do not generate documentation files for the root command. If the generated documentation is too messy, this may be desirable.");
    let include_non_leaves = bpaf::long("include-non-leaves").flag(true, false).help("Generate documentation files for non-leaf commands (that is, commands which primarily act to contain other subcommands). By default, this is not done because it can be messy.");
    let ignore_writing_errors = bpaf::long("ignore-errors")
        .switch()
        .help("Warn in case of errors writing the documentation, rather than aborting.");
    let mode = bpaf::pure(subcommand_mode);
    let parser =
        construct!(GenDocs { mode, verbosity(log_level), output_directory, ignore_writing_errors, include_root, include_non_leaves })
            .to_options();
    let parser = parser
        .descr(subcommand_mode.get_info(Info::Description))
        .version(super::VERSION);
    subcommand_hook.end_command(&parser)?;
    Ok(parser.command(name))
}

/// Module for hooks to generate recursive subcommand documentation as appropriate.
pub mod documentation_hooks {
    use std::{ops::ControlFlow, os::unix::prelude::OsStrExt, path::Path};

    pub use manpages::ManPageGenerator;
    use slog::{slog_debug, slog_error, slog_warn, Logger};
    /// Attempt to create the parent directory for one of the various document generators.
    ///
    /// This handles logging and such, and returns a [`ControlFlow`]. If the control flow is `break`,
    /// it means you must terminate the function with the relevant value (even if it is `Ok`, that
    /// just indicates that we have been told to ignore errors and have instead returned `Ok(())`
    /// from the function early).
    ///
    /// `full_file_path` is the path of the actual file you are going to try to write.
    fn try_create_parent_directories(
        logger: &Logger,
        ignore_writing_errors: bool,
        full_file_path: &Path,
    ) -> ControlFlow<Result<(), std::io::Error>> {
        // Create the containing directory if it makes sense to do so:
        match full_file_path.parent() {
            None => {
                slog_debug!(logger, "No parent directory to create (absolute path)");
                ControlFlow::Continue(())
            }
            Some(p) if p.as_os_str().as_bytes().is_empty() => {
                slog_debug!(logger, "No parent directory to create (relative path)");
                ControlFlow::Continue(())
            }
            Some(parent_dir) => {
                if let Err(e) = std::fs::create_dir_all(parent_dir) {
                    match ignore_writing_errors {
                        true => {
                            slog_warn!(logger, "Could not create parent directories (at {path}), skipping this command", path = parent_dir.display(); "error" => e);
                            ControlFlow::Break(Ok(()))
                        }
                        false => {
                            slog_error!(logger, "Could not create parent directories (at {path})", path = parent_dir.display(); "error" => &e);
                            ControlFlow::Break(Err(e))
                        }
                    }
                } else {
                    ControlFlow::Continue(())
                }
            }
        }
    }

    /// Attempt to open the given file path for writing for the given settings, and perform
    /// appropriate logging.
    ///
    /// Break => return the value even if it claims to be "ok" (because we are ignoring errors.
    /// Continue => continue.
    fn try_open_file_for_writing(
        logger: &Logger,
        ignore_writing_errors: bool,
        full_file_path: &Path,
    ) -> ControlFlow<Result<(), std::io::Error>, std::fs::File> {
        match std::fs::OpenOptions::new()
            .write(true)
            .create(true)
            .open(full_file_path)
        {
            Ok(f) => ControlFlow::Continue(f),
            Err(e) if ignore_writing_errors => {
                slog_warn!(logger, "Could not open path {path} for writing - skipping this command", path = full_file_path.display(); "error" => e);
                ControlFlow::Break(Ok(()))
            }
            Err(e) => {
                slog_error!(logger, "Could not open path {path} for writing", path = full_file_path.display(); "error" => &e);
                ControlFlow::Break(Err(e))
            }
        }
    }

    /// Create a function that wraps the output of a generic writer function with logging and automatic handling of errors
    /// either via ignoring them (and skipping the file) or via actually erroring out. The
    /// resulting function turns a simple `Result` into a Continue/Break combo
    ///
    /// Note that we could make this generic over [`std::error::Error`], but [`slog`] implements
    /// some special traits for [`std::io::Error`].  
    fn wrap_write_output<'all, T>(
        logger: &'all Logger,
        ignore_writing_errors: bool,
        full_file_path: &'all Path,
    ) -> impl FnMut(Result<T, std::io::Error>) -> ControlFlow<Result<(), std::io::Error>, T> + 'all
    {
        move |v| match v {
            Ok(r) => ControlFlow::Continue(r),
            Err(e) if ignore_writing_errors => {
                slog_warn!(logger, "Could not write to file {path} - skipping this command", path = full_file_path.display(); "error" => e);
                ControlFlow::Break(Ok(()))
            }
            Err(e) => {
                slog_error!(logger, "Could not write to file {path}", path = full_file_path.display(); "error" => &e);
                ControlFlow::Break(Err(e))
            }
        }
    }

    mod manpages {
        use std::{
            fs::File,
            io::Write,
            ops::ControlFlow,
            os::unix::prelude::OsStrExt,
            path::{Path, PathBuf},
        };

        use bpaf::doc::Section;
        use chain_trans::Trans;
        use flate2::Compression;
        use slog::{slog_info, slog_o};

        use super::{try_create_parent_directories, try_open_file_for_writing};
        use crate::{
            cmdline::{
                gendocs::{
                    documentation_hooks::wrap_write_output, GenDocs, GENDOC_SUBCOMMAND_NAME,
                },
                sidechannel, PROGRAM_NAME,
            },
            logutils::CmdPathExt,
        };

        /// Taken directly from [`bpaf::buffer::Section`]
        fn section_as_str<'a>(section: &Section<'a>) -> &'a str {
            match section {
                Section::General => "1",
                Section::SystemCall => "2",
                Section::LibraryFunction => "3",
                Section::SpecialFile => "4",
                Section::FileFormat => "5",
                Section::Game => "6",
                Section::Misc => "7",
                Section::Sysadmin => "8",
                Section::Custom(s) => s,
            }
        }

        /// Get the manpage section for the given (sub)command path
        fn command_section(subcommand_path: &[&'static str]) -> Section<'static> {
            match subcommand_path {
                [PROGRAM_NAME, rest @ ..] => match rest {
                    [GENDOC_SUBCOMMAND_NAME, ..] => Section::General,
                    [..] => Section::Sysadmin,
                },
                [..] => panic!("Subcommand not of the normal program?"),
            }
        }

        /// Get the full path (relative to the base directory of the config) of the given (gzip-compressed) manpage
        /// for the specified subcommand.
        fn relative_manpage_file_location(subcommand_path: &[&'static str]) -> PathBuf {
            let manpage_section = command_section(subcommand_path);
            let manpage_section_id = section_as_str(&manpage_section);
            let manpage_full_path = format!(
                "man{manpage_section_id}/{}.{manpage_section_id}.gz",
                subcommand_path.join("-")
            );
            manpage_full_path.into()
        }

        /// Generates manual pages inside a base folder (using subfolders that look like
        /// `man<section-as-number>/<manpage-name.<section-as-number>.gz`
        #[derive(Clone, Debug)]
        pub struct ManPageGenerator<'p> {
            ignore_writing_errors: bool,
            include_root: bool,
            include_non_leaves: bool,
            base_output_directory: &'p Path,
            local_logger: slog::Logger,
        }

        impl<'p> sidechannel::SimpleHook for ManPageGenerator<'p> {
            type Seed = (&'p GenDocs, slog::Logger);

            type SubcommandHandler<'a> = Self where Self: 'a;

            type Error = std::io::Error;

            fn root_command(_command_name: &'static str, (seed, logseed): Self::Seed) -> Self {
                Self {
                    ignore_writing_errors: seed.ignore_writing_errors,
                    base_output_directory: &seed.output_directory,
                    local_logger: logseed,
                    include_root: seed.include_root,
                    include_non_leaves: seed.include_non_leaves,
                }
            }

            #[inline]
            fn starting_subcommand<'s>(
                &'s mut self,
                _current_command_stack: &[&'static str],
                _subcommand_name: &'static str,
            ) -> Self::SubcommandHandler<'s> {
                self.clone()
            }

            // This is where all the important stuff happens.
            fn end_command<Q>(
                self,
                command_path: Vec<&'static str>,
                leaf: bool,
                parser: &bpaf::OptionParser<Q>,
            ) -> Result<(), Self::Error> {
                let command_app_str = command_path.join("-");
                let logger = self.local_logger.new(slog_o!(command_path
                    .clone()
                    .into_documenting_command_path()));
                let manpage_section_type = command_section(&command_path);
                let full_file_path = self
                    .base_output_directory
                    .to_owned()
                    .trans_mut(|v| v.push(relative_manpage_file_location(&command_path)));

                let is_root = command_path.len() <= 1;
                if is_root && !self.include_root {
                    slog_info!(
                        logger,
                        "Not creating root documentation at {path}",
                        path = full_file_path.display()
                    );
                    return Ok(());
                }

                let ignore_leaf_check = is_root;
                // If it's a root and we have included it above, it shouldn't be "counted" as a
                // non-leaf.
                if !ignore_leaf_check && !leaf && !self.include_non_leaves {
                    slog_info!(
                        logger,
                        "Not creating non-leaf documentation at {path}",
                        path = full_file_path.display()
                    );
                    return Ok(());
                }

                if let ControlFlow::Break(r) = try_create_parent_directories(
                    &logger,
                    self.ignore_writing_errors,
                    full_file_path.as_path(),
                ) {
                    return r;
                }

                let manpage = parser.render_manpage(
                    &command_app_str,
                    manpage_section_type,
                    None,
                    None,
                    Some(&format!(
                        "Secure-Boot Self Determination - {command_app_str}"
                    )),
                );
                let gzcompressor = flate2::GzBuilder::new().filename(
                    full_file_path
                        .file_stem()
                        .expect("file path should always be a file path lol")
                        .as_bytes()
                        .to_owned(),
                );
                let file: File = match try_open_file_for_writing(
                    &logger,
                    self.ignore_writing_errors,
                    full_file_path.as_path(),
                ) {
                    std::ops::ControlFlow::Continue(f) => f,
                    std::ops::ControlFlow::Break(r) => return r,
                };
                slog_info!(
                    logger,
                    "Generating manpage at {path}",
                    path = full_file_path.display()
                );
                let mut gzfile = gzcompressor.write(file, Compression::default());
                let mut wrapper = wrap_write_output(
                    &logger,
                    self.ignore_writing_errors,
                    full_file_path.as_path(),
                );
                if let ControlFlow::Break(r) = (|| {
                    gzfile.write_all(manpage.as_bytes()).trans(&mut wrapper)?;
                    gzfile.flush().trans(&mut wrapper)
                })() {
                    return r;
                }
                slog_info!(
                    logger,
                    "Finished generating manpage at {path}",
                    path = full_file_path.display()
                );
                Ok(())
            }
        }
    }
}

impl LogLevelProvider for GenDocs {
    fn get_log_level(&self) -> slog::FilterLevel {
        self.verbosity
    }
}

impl CommandInfo for GenDocs {
    fn append_as_subcommand(&self, command_path: &mut Vec<&'static str>) {
        command_path.push(GENDOC_SUBCOMMAND_NAME);
        command_path.push(
            self.mode
                .get_info(GenDocModeSubcommandComponent::SubcommandName),
        );
    }
}

/// Create the document generation subcommand
pub fn gendocs_cmd<SDH: sidechannel::Hook>(
    parent_hook: &mut SDH,
) -> Result<bpaf::parsers::ParseCommand<GenDocs>, SDH::Error> {
    let mut subcommand_hook = parent_hook.starting_subcommand(GENDOC_SUBCOMMAND_NAME);
    let manpages = make_gendocs_subcommand(GenDocsMode::Man, &mut subcommand_hook)?;
    let markdown_html = make_gendocs_subcommand(GenDocsMode::MarkdownHtml, &mut subcommand_hook)?;
    let pure_markdown = make_gendocs_subcommand(GenDocsMode::PureMarkdown, &mut subcommand_hook)?;
    let options = construct!([manpages, markdown_html, pure_markdown])
        .to_options()
        .descr("Generate documentation for this program in various formats")
        .version(super::VERSION)
        .fallback_to_usage();
    subcommand_hook.end_command(&options)?;
    Ok(options.command(GENDOC_SUBCOMMAND_NAME))
}

/// Run the gendocs command
pub(crate) fn gendocs_run<CmdPath: Borrow<[&'static str]>>(
    logger: slog::Logger,
    information: GenDocs,
    _running_cmd: crate::logutils::RunningCommandPath<CmdPath>,
) -> Result<(), anyhow::Error> {
    let logger = logger.new(slog_o! {
        "output-directory" => information.output_directory.display().to_string()
    });
    match information.mode {
        GenDocsMode::MarkdownHtml => {
            slog_info!(logger, "Generating mixed markdown/html documentation.");
            unimplemented!()
        }
        GenDocsMode::PureMarkdown => {
            slog_info!(logger, "Generating pure markdown documentation.");
            unimplemented!()
        }
        GenDocsMode::Man => {
            slog_info!(logger, "Generating manual pages.");
            // This generates all the command options again. Importantly, however, documentation is
            // generated via the hook.
            let _regenerated_command_options = super::sdsb_options::<
                cmdline::sidechannel::StackHook<documentation_hooks::ManPageGenerator>,
            >((&information, logger.clone()))?;
            slog_info!(logger, "Generation complete.");
            Ok(())
        }
    }
}

// SDSB - Manage initramfs generators, kernels, and self-controlled secure boot.
// Copyright (C) 2023 Matti Bryce <mattibryce at protonmail dot com>
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <https://www.gnu.org/licenses/>.
