//! Module providing parser components for the purpose of selecting  the config and data loading
//! folders.

use std::path::PathBuf;

use bpaf::{construct, doc::Style, Parser};
use chain_trans::Trans;

use crate::{
    objects,
    resolve::search_provider::{self as sp, IntoIteratorExt, SearchProvider},
    XDG_NAME,
};

/// Specifier for local/config-only/data-only search paths
///
/// This also can act as a standalone search provider.
#[derive(Debug, Clone, PartialEq, Eq)]
enum SinglePathProvidingSpecifier {
    Local(sp::SingleLocalSearchProvider),
    ConfigOnly(sp::SingleConfigSearchProvider),
    DataOnly(sp::SingleDataSearchProvider),
}

impl From<sp::SingleLocalSearchProvider> for SinglePathProvidingSpecifier {
    #[inline]
    fn from(value: sp::SingleLocalSearchProvider) -> Self {
        Self::Local(value)
    }
}

impl From<sp::SingleDataSearchProvider> for SinglePathProvidingSpecifier {
    #[inline]
    fn from(value: sp::SingleDataSearchProvider) -> Self {
        Self::DataOnly(value)
    }
}

impl From<sp::SingleConfigSearchProvider> for SinglePathProvidingSpecifier {
    #[inline]
    fn from(value: sp::SingleConfigSearchProvider) -> Self {
        Self::ConfigOnly(value)
    }
}

impl SinglePathProvidingSpecifier {
    /// Make a [`SinglePathProvidingSpecifier::Local`] with the given path
    #[inline]
    pub fn make_local(path: PathBuf) -> Self {
        Self::Local(sp::LocalDirectory::from(path).into())
    }

    /// Make a [`SinglePathProvidingSpecifier::DataOnly`] with the given path
    #[inline]
    pub fn make_data_only(path: PathBuf) -> Self {
        Self::DataOnly(sp::DataDirectory::from(path).into())
    }

    /// Make a [`SinglePathProvidingSpecifier::ConfigOnly`] with the given path
    #[inline]
    pub fn make_config_only(path: PathBuf) -> Self {
        Self::ConfigOnly(sp::ConfigDirectory::from(path).into())
    }
}

impl SearchProvider for SinglePathProvidingSpecifier {
    type Error = sp::DirectorySearcherError;

    #[inline]
    fn find_entries<ForType: objects::ObjectType>(
        &self,
        object_path: objects::path::ObjectPathRef<'_, ForType>,
    ) -> Result<Vec<sp::PerCfgDataEntry>, Self::Error> {
        match self {
            SinglePathProvidingSpecifier::Local(v) => v.find_entries(object_path),
            SinglePathProvidingSpecifier::ConfigOnly(v) => v.find_entries(object_path),
            SinglePathProvidingSpecifier::DataOnly(v) => v.find_entries(object_path),
        }
    }
}

/// Single argument that parses a local search provider.
fn local_search_provider() -> impl bpaf::Parser<SinglePathProvidingSpecifier> {
    let config = sp::LOCAL_DIRECTORY_CONFIG_SUFFIX;
    let data = sp::LOCAL_DIRECTORY_DATA_SUFFIX;
    bpaf::long("local")
        .argument::<PathBuf>("LOCAL_DIRECTORY")
        .help(format!("Add a local directory with two subdirectories - {config} for config definitions and {data} for data definitions - to the object resolution path").as_str())
        .map(SinglePathProvidingSpecifier::make_local)
}

/// Single argument that parses a config-only directory search provider.
fn config_search_provider() -> impl bpaf::Parser<SinglePathProvidingSpecifier> {
    bpaf::long("config")
        .argument::<PathBuf>("CONFIG_ONLY_LOCAL_DIRECTORY")
        .help("Add a config-only (i.e. only containing configuration-category types) directory to the object-resolution path")
        .map(SinglePathProvidingSpecifier::make_config_only)
}

/// Single argument that parses a data-only directory search provider.
fn data_search_provider() -> impl Parser<SinglePathProvidingSpecifier> {
    bpaf::long("data")
        .argument::<PathBuf>("DATA_ONLY_LOCAL_DIRECTORY")
        .help("Add a data-only (i.e. only containing data-category types) directory to the object-resolution path")
        .map(SinglePathProvidingSpecifier::make_data_only)
}

/// Combines the various data/local/config specifier arguments in another single argument.
fn single_cli_specified_provider() -> impl Parser<SinglePathProvidingSpecifier> {
    construct!([
        local_search_provider(),
        config_search_provider(),
        data_search_provider()
    ])
}

/// Specifier for multi-arguments/ordered argument parsing.
#[derive(Debug, Clone)]
enum SearchPathModifier {
    ResetNoXdg,
    ResetXdg,
    Append(SinglePathProvidingSpecifier),
}

/// Single argument that produces an arbitrary path modifier.
fn single_modification_specifier() -> impl Parser<SearchPathModifier> {
    let reset_flag = bpaf::long("reset").help("Reset the object-resolution path to be completely empty. Must be followed by an indicator for xdg or non-xdg.").req_flag(());
    let no_xdg_flag = bpaf::long("no-xdg").help("Indicate that XDG directories should not be at the start of the object-resolution path. You should probably provide some local directories if you use this.").req_flag(SearchPathModifier::ResetNoXdg);
    let xdg_flag = bpaf::long("xdg").help("Indicate that the object-resolution path should start with the XDG standard directories.").req_flag(SearchPathModifier::ResetXdg);
    let xdg_or_no_xdg = construct!([no_xdg_flag, xdg_flag]);
    let full_reset_parser = construct!(reset_flag, xdg_or_no_xdg)
        .adjacent()
        .map(|(_, v)| v);
    let full_append_parser = single_cli_specified_provider().map(SearchPathModifier::Append);
    construct!([full_reset_parser, full_append_parser])
}

#[derive(Debug, Clone, PartialEq, Eq)]
pub struct SearchPathSpecification {
    use_xdg: bool,
    non_xdg_search_providers: Vec<SinglePathProvidingSpecifier>,
}

impl SearchPathSpecification {
    fn apply_modifier(&mut self, modifier: SearchPathModifier) {
        match modifier {
            SearchPathModifier::ResetNoXdg => {
                *self = Self::default().trans_mut(|v| v.use_xdg = false);
            }
            SearchPathModifier::ResetXdg => {
                *self = Self::default().trans_mut(|v| v.use_xdg = true);
            }
            SearchPathModifier::Append(new_provider) => {
                self.non_xdg_search_providers.push(new_provider);
            }
        }
    }

    /// Attempt to create a search provider matching this specification.
    pub fn try_create_search_provider(
        &self,
    ) -> Result<impl sp::SearchProvider + '_, xdg::BaseDirectoriesError> {
        let xdg = if self.use_xdg {
            Some(
                xdg::BaseDirectories::with_prefix(XDG_NAME)
                    .map(sp::DirectoryHierarchySearcher::from)?,
            )
        } else {
            None
        };
        let main_path = self
            .non_xdg_search_providers
            .iter()
            .into_overriding_search_provider();
        Ok(xdg.override_this_with(main_path))
    }
}

impl Default for SearchPathSpecification {
    fn default() -> Self {
        Self {
            use_xdg: true,
            non_xdg_search_providers: vec![],
        }
    }
}

/// Parses the search path from the arguments.
pub fn path_extractor() -> impl Parser<SearchPathSpecification> {
    let single = single_modification_specifier();
    single.many().with_group_help(|inner_documentation| {
        bpaf::Doc::default().trans_mut(move |d| {
            d.emphasis("SPECIFYING OBJECT RESOLUTION PATH");
            d.text(" ");
            d.text("\
The object-resolution-path controls where, and in what order, the definitions for various objects \
used by this program are loaded from (to be combined via the override system documented in the README.)

By default, data and config are loaded from the appropriate locations and priority order as specified by the XDG Base \
Directory Specification. These arguments allow you to provide further, higher-priority data and config sources, or \
reset the loading path to avoid using XDG all together.

The available specifiers are as follows:\
");
            d.meta(inner_documentation, true);
            d.doc(&[("\
Each argument of this kind provided appends a new, higher-priority information source of the respective type to the object-\
resolution path. The ", Style::Text),  ("--reset", Style::Literal), (" command wipes the loading path, and must be followed by either ", Style::Text), 
("--xdg", Style::Literal), (" or ", Style::Text), ("--no-xdg", Style::Literal), 
(" to indicate whether or not XDG is used as the 'base' of the object-resolution path.", Style::Text)].as_slice().into()
            )
        })
    }).fallback(vec![]).map(|modifiers| {
        let mut providerspec = SearchPathSpecification::default();
        modifiers.into_iter().for_each(|modifier| providerspec.apply_modifier(modifier));
        providerspec
    })
}

#[cfg(test)]
mod test {
    use bpaf::Parser;

    use super::path_extractor;
    use crate::cmdline::search_path_specifiers::SearchPathSpecification;

    /// Test that ordering is maintained when parsing complex sequences of search path specifiers.
    #[test]
    fn ordering_single_reset() {
        use super::SinglePathProvidingSpecifier as SPP;

        let options = path_extractor().to_options();
        let test_args_no_reset = &[
            "--local", "local0", "--config", "config0", "--local", "local1", "--data", "data0",
        ];

        let output = options
            .run_inner(test_args_no_reset)
            .expect("These should be valid args");
        assert_eq!(
            output,
            SearchPathSpecification {
                use_xdg: true,
                non_xdg_search_providers: vec![
                    SPP::make_local("local0".into()),
                    SPP::make_config_only("config0".into()),
                    SPP::make_local("local1".into()),
                    SPP::make_data_only("data0".into())
                ]
            }
        )
    }

    #[test]
    fn ordering_multi_resets() {
        use super::SinglePathProvidingSpecifier as SPP;

        let options = path_extractor().to_options();
        let test_args = &[
            "--local", "local00", "--config", "config00", "--local", "local01",
            // First reset chunk
            "--reset", "--no-xdg", "--config", "config10", "--local", "local10", "--local",
            "local11", "--data", "data10", // Second reset chunk
            "--reset", "--no-xdg", "--data", "data20", "--data", "data21", "--config", "config20",
            "--local", "local20", "--data", "data22",
        ];
        let parsed_multi = options
            .run_inner(test_args)
            .expect("The provided arguments are valid");
        assert_eq!(
            parsed_multi,
            SearchPathSpecification {
                use_xdg: false,
                non_xdg_search_providers: vec![
                    SPP::make_data_only("data20".into()),
                    SPP::make_data_only("data21".into()),
                    SPP::make_config_only("config20".into()),
                    SPP::make_local("local20".into()),
                    SPP::make_data_only("data22".into())
                ]
            }
        )
    }

    /// Ensure that specifying nothing results in simple XDG mode.
    #[test]
    fn unspecified_does_actually_work() {
        let options = path_extractor().to_options();
        let parsed = options.run_inner(&[]).expect("Should be valid");
        assert_eq!(parsed, SearchPathSpecification::default())
    }
}

// SDSB - Manage initramfs generators, kernels, and self-controlled secure boot.
// Copyright (C) 2023 Matti Bryce <mattibryce at protonmail dot com>
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <https://www.gnu.org/licenses/>.
