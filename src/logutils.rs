//! File containing tools to make logging with `slog` easier.
//! Contains functions for generating standardised representations of subcommand-paths.

use std::{
    borrow::Borrow,
    fmt::{Display, Write},
};

use chain_trans::Trans;
use slog::Value;

/// Current running command path key.
pub const RUNNING_COMMAND_KEY: &str = "running-command";

/// Command that is being documented key
pub const DOCUMENTING_COMMAND_KEY: &str = "documenting-command";

/// The display mode for the command path. Used to e.g. construct strings.
#[derive(Debug, Clone, Copy, PartialEq, Eq, PartialOrd, Ord, Hash)]
pub enum CommandPathFormatMode {
    /// Build a command path where each component is separated by `-`.
    ///
    /// Useful for constructing file names and such.
    DashSep,
    /// Command path should be split with a space.
    SpaceSep,
}

impl CommandPathFormatMode {
    /// Extra characters per subcommand
    #[inline]
    pub fn extra_chars_per_path_elem(&self) -> usize {
        match self {
            Self::SpaceSep | Self::DashSep => 1,
        }
    }

    /// Emit the given element to the given formatter as appropriate.
    #[inline]
    pub fn emit_formatted_element(
        &self,
        f: &mut std::fmt::Formatter<'_>,
        next_path_element: &str,
        element_is_first: bool,
        _element_is_last: bool,
    ) -> std::fmt::Result {
        match self {
            CommandPathFormatMode::DashSep => {
                if !element_is_first {
                    f.write_char('-')?;
                    f.write_str(next_path_element)
                } else {
                    f.write_str(next_path_element)
                }
            }
            CommandPathFormatMode::SpaceSep => {
                if !element_is_first {
                    f.write_char(' ')?;
                    f.write_str(next_path_element)
                } else {
                    f.write_str(next_path_element)
                }
            }
        }
    }
}

/// Raw representation of a path to a (sub) command (like `sdsb doc man`) in a more structured
/// form. Built from something that can be borrowed as a slice of strings.
#[derive(Debug, Clone, Copy, PartialEq, Eq, PartialOrd, Ord, Hash)]
pub struct RawCommandPath<T: ?Sized> {
    /// Separation mode
    pub format_mode: CommandPathFormatMode,
    /// Path to the command
    pub command_path: T,
}

impl<'str, T: ?Sized + Borrow<[&'str str]>> slog::Value for RawCommandPath<T> {
    fn serialize(
        &self,
        _record: &slog::Record,
        key: slog::Key,
        serializer: &mut dyn slog::Serializer,
    ) -> slog::Result {
        serializer.emit_str(key, &self.build_string())
    }
}

impl<'str, T: ?Sized + Borrow<[&'str str]>> RawCommandPath<T> {
    /// Get the size of the string this will emit when converted.
    pub fn string_size_heuristic(&self) -> usize {
        let path_elements = self.command_path.borrow().len();
        let extra_chars_per_elem = self.format_mode.extra_chars_per_path_elem();
        let new_string_size = self
            .command_path
            .borrow()
            .iter()
            .map(move |v| v.len())
            .sum::<usize>()
            + extra_chars_per_elem * path_elements;
        new_string_size
    }

    /// Construct the string representation of this path
    pub fn build_string(&self) -> String {
        let mut formatted_path = String::with_capacity(self.string_size_heuristic());
        formatted_path
            .write_fmt(format_args!("{self}"))
            .expect("Not exactly writing to io or anything");
        formatted_path
    }
}

impl<'str, T: ?Sized + Borrow<[&'str str]>> Display for RawCommandPath<T> {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        let path_elements = self.command_path.borrow().len();
        for (idx, path_elem) in self.command_path.borrow().iter().enumerate() {
            self.format_mode.emit_formatted_element(
                f,
                path_elem,
                idx == 0,
                idx == path_elements - 1,
            )?;
        }
        Ok(())
    }
}

impl<'str, T: ?Sized + Borrow<[&'str str]>> RawCommandPath<T> {
    /// Perform raw construction
    #[inline]
    pub fn new(format: CommandPathFormatMode, untyped_path: T) -> Self
    where
        T: Sized,
    {
        Self {
            format_mode: format,
            command_path: untyped_path,
        }
    }
}

impl<'str, Source: Borrow<[&'str str]>> From<(Source, CommandPathFormatMode)>
    for RawCommandPath<Source>
{
    fn from((owned_slice, format): (Source, CommandPathFormatMode)) -> Self {
        Self {
            format_mode: format,
            command_path: owned_slice,
        }
    }
}

/// Wrapper type for [`RawCommandPath`] that indicates that it indicates the current running
/// command. This automatically bundles in the [`RUNNING_COMMAND_KEY`], so you don't need to
/// manually do a `key => value`, only a `value`. Also embeds a standardised format  
#[derive(Debug, Clone, Copy, PartialEq, Eq, PartialOrd, Ord)]
pub struct RunningCommandPath<T: ?Sized>(RawCommandPath<T>);

impl<'str, T: ?Sized + Borrow<[&'str str]>> RunningCommandPath<T> {
    #[inline]
    pub fn new(b: T) -> Self
    where
        T: Sized,
    {
        Self((b, CommandPathFormatMode::SpaceSep).into())
    }
}

/// Basically just copy the [slog::SingleKV] structure's impl
impl<'str, T: ?Sized + Borrow<[&'str str]>> slog::KV for RunningCommandPath<T> {
    #[inline]
    fn serialize(
        &self,
        record: &slog::Record,
        serializer: &mut dyn slog::Serializer,
    ) -> slog::Result {
        self.0.serialize(record, RUNNING_COMMAND_KEY, serializer)
    }
}

/// Wrapper type for [`RawCommandPath`] that indicates that it indicates the current running
/// command. This automatically bundles in the [`DOCUMENTING_COMMAND_KEY`], so you don't need to
/// manually do a `key => value`, only a `value`
#[derive(Debug, Clone, Copy, PartialEq, Eq, PartialOrd, Ord)]
pub struct DocumentingCommandPath<T: ?Sized>(RawCommandPath<T>);

impl<'str, T: ?Sized + Borrow<[&'str str]>> DocumentingCommandPath<T> {
    #[inline]
    pub fn new(b: T) -> Self
    where
        T: Sized,
    {
        Self((b, CommandPathFormatMode::DashSep).into())
    }
}

/// Basically just copy the [slog::SingleKV] structure's impl
impl<'str, T: ?Sized + Borrow<[&'str str]>> slog::KV for DocumentingCommandPath<T> {
    #[inline]
    fn serialize(
        &self,
        record: &slog::Record,
        serializer: &mut dyn slog::Serializer,
    ) -> slog::Result {
        self.0
            .serialize(record, DOCUMENTING_COMMAND_KEY, serializer)
    }
}

/// Extension trait for creating various kinds of commandpaths from vectors/slices/etc.
pub trait CmdPathExt<'str>: Borrow<[&'str str]> {
    /// Convert this untyped command path into a [`RunningCommandPath`], which embeds the standard
    /// logging key [`RUNNING_COMMAND_KEY`] to avoid needing to provide a key when logging.
    #[inline]
    fn into_running_command_path(self) -> RunningCommandPath<Self>
    where
        Self: Sized,
    {
        self.trans(RunningCommandPath::new)
    }

    /// Convert this untyped command path into a [`DocumentingCommandPath`], which embeds the standard
    /// logging key [`DOCUMENTING_COMMAND_KEY`] to avoid needing to provide a key when logging.
    #[inline]
    fn into_documenting_command_path(self) -> DocumentingCommandPath<Self>
    where
        Self: Sized,
    {
        self.trans(DocumentingCommandPath::new)
    }
}

impl<'str, T: ?Sized + Borrow<[&'str str]>> CmdPathExt<'str> for T {}

/// Function that allows the easy creation of a null logger that discards all records.
///
/// Why? This is highly useful when you want to only optionally provide a function with a logger.
#[inline]
pub fn null_logger() -> slog::Logger {
    slog::Logger::root(slog::Discard, slog::slog_o! {})
}

// SDSB - Manage initramfs generators, kernels, and self-controlled secure boot.
// Copyright (C) 2023 Matti Bryce <mattibryce at protonmail dot com>
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <https://www.gnu.org/licenses/>.
