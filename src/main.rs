#![doc = include_str!("../README.md")]
#![cfg_attr(
    feature = "backtrace",
    feature(error_generic_member_access),
    feature(error_reporter)
)]

extern crate alloc;

use slog::{slog_o, Drain};

use crate::{
    cmdline::{CommandInfo, LogLevelProvider},
    logutils::CmdPathExt,
};

/// Name for XDG subdirectory to use for config/data/etc.
///
/// This is declared separately to the program name because of the potential of the name changing
/// in future (unlikely, but it's better to keep these things separate).
pub const XDG_NAME: &str = "sdsb";

pub mod cmdline;
pub mod config_components;
pub mod gen;
pub mod install;
pub mod logutils;
pub mod objects;
pub mod platform;
pub mod resolve;
pub mod structured_delta;
pub mod utils;

fn main() -> anyhow::Result<()> {
    let cli = cmdline::sdsb_options::<()>(()).expect("infallible");
    let cmd = cli.run();

    let logging_verbosity = cmd.get_log_level();
    let running_command_path = {
        let mut v = vec![];
        cmd.append_as_subcommand(&mut v);
        v
    }
    .into_running_command_path();

    let decorator = slog_term::TermDecorator::new().build();
    let drain = slog_term::FullFormat::new(decorator)
        .use_utc_timestamp()
        .build()
        .filter(move |r| logging_verbosity.accepts(r.level()))
        .fuse();
    let drain = slog_async::Async::new(drain).build_no_guard().fuse();
    let logger = slog::Logger::root(drain, slog_o!(running_command_path.clone()));
    cmdline::sdsb_run(logger, cmd, running_command_path)
}

// SDSB - Manage initramfs generators, kernels, and self-controlled secure boot.
// Copyright (C) 2023 Matti Bryce <mattibryce at protonmail dot com>
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <https://www.gnu.org/licenses/>.
