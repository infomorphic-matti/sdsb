//! Core module for the object system.

use alloc::{rc::Rc, sync::Arc};
use core::{any, fmt::Display};

pub mod cli;
pub mod internal_references;
pub mod multi;
pub mod path;

/// Used to specify what part of XDG the object type can be defined in.
#[derive(Clone, Copy, Hash, PartialEq, Eq, Debug)]
pub enum XDGCategory {
    Data,
    Config,
}

/// Allow a type to provide information on it's internal references (by paths) to various
/// different [`ObjectType`] values. This is used to enable validation of various objects.
pub trait ObjectReferential {
    /// Provide a visitor with information on object references in this object.
    ///
    /// Should return an error if the visitor returns one too. Objects do not know their own paths,
    /// you don't need to try to visit your own path.
    fn describe_referenced_paths<V: internal_references::ReferencedObjectVisitor>(
        &self,
        path_visitor: &mut V,
    ) -> Result<(), V::Error>;
}

/// Implement [`ObjectReferential`] for references to [`ObjectReferential`] types
impl<T: ?Sized + ObjectReferential> ObjectReferential for &T {
    #[inline]
    fn describe_referenced_paths<V: internal_references::ReferencedObjectVisitor>(
        &self,
        path_visitor: &mut V,
    ) -> Result<(), V::Error> {
        T::describe_referenced_paths(*self, path_visitor)
    }
}

/// Implement [`ObjectReferential`] for mutable references to [`ObjectReferential`] types
impl<T: ?Sized + ObjectReferential> ObjectReferential for &mut T {
    #[inline]
    fn describe_referenced_paths<V: internal_references::ReferencedObjectVisitor>(
        &self,
        path_visitor: &mut V,
    ) -> Result<(), V::Error> {
        T::describe_referenced_paths(self, path_visitor)
    }
}

/// Implement [`ObjectReferential`] for boxed [`ObjectReferential`] types
impl<T: ?Sized + ObjectReferential> ObjectReferential for Box<T> {
    #[inline]
    fn describe_referenced_paths<V: internal_references::ReferencedObjectVisitor>(
        &self,
        path_visitor: &mut V,
    ) -> Result<(), V::Error> {
        T::describe_referenced_paths(self, path_visitor)
    }
}

/// Implement [`ObjectReferential`] for Rc'd [`ObjectReferential`] types
impl<T: ?Sized + ObjectReferential> ObjectReferential for Rc<T> {
    #[inline]
    fn describe_referenced_paths<V: internal_references::ReferencedObjectVisitor>(
        &self,
        path_visitor: &mut V,
    ) -> Result<(), V::Error> {
        T::describe_referenced_paths(self, path_visitor)
    }
}

/// Implement [`ObjectReferential`] for Arc'd [`ObjectReferential`] types
impl<T: ?Sized + ObjectReferential> ObjectReferential for Arc<T> {
    #[inline]
    fn describe_referenced_paths<V: internal_references::ReferencedObjectVisitor>(
        &self,
        path_visitor: &mut V,
    ) -> Result<(), V::Error> {
        T::describe_referenced_paths(self, path_visitor)
    }
}

/// Indicates that this type is a loadable configuration type for the sdsb. May be applied only to
/// toplevel object types and not non-distinct subcomponents.
pub trait ObjectType:
    for<'de> serde::Deserialize<'de> + serde::Serialize + ObjectReferential + any::Any
{
    /// Category of data objects of this type are stored in.
    const XDG_CATEGORY: XDGCategory;

    /// The storage path of this category relative to the appropriate locations in
    /// XDG_CATEGORY.
    ///
    /// This is an array of the components along the path.
    const RELATIVE_STORAGE_PATH: &'static [&'static str];

    /// Useful debugger name of this object type (e.g. 'initramfs', 'platform') -
    /// mostly for logging purposes.
    ///
    /// Also used to derive CLI argument names.
    const OBJECT_TYPE_HUMAN_NAME: &'static str;

    /// Metavar name to use for paths when working with commandline arguments. Should be in all caps,
    /// e.g. `PLATFORM_OBJECT_PATH`
    const OBJECT_PATH_METAVAR: &'static str;

    /// Like [`ObjectType::OBJECT_TYPE_HUMAN_NAME`], but for this specific instantiation of the
    /// object. For example, a linux platform might return "linux-kernel" for this value, or an
    /// bootloader platform might return "bootloader" for this.
    ///
    /// By default, this returns the value of [`ObjectType::OBJECT_TYPE_HUMAN_NAME`]. It's
    /// primarily a tool intended for logging, and both the type-wide human name and the specific
    /// instantiated human name will be used.
    ///
    /// For some object types it doesn't make much sense to make this different per-object for
    /// instance if the object type only has one general variant it can take.
    #[inline]
    fn object_human_name(&self) -> &'static str {
        Self::OBJECT_TYPE_HUMAN_NAME
    }

    /// Provide help information for a CLI argument that intends to take a path to this type of
    /// object.  
    fn object_path_cli_help() -> bpaf::Doc;
}

/// Metadata for dynamic [`ObjectType`]
///
/// Provides clean Display information for embedding in error messages
#[derive(Debug, Clone, Copy, PartialEq, Eq, PartialOrd, Ord, Hash)]
pub enum ObjectTypeDynMeta {
    Named {
        object_type_human_name: &'static str,
        object_type_name: &'static str,
    },
    Id(any::TypeId),
}

impl ObjectTypeDynMeta {
    #[inline]
    pub fn from_type<T: ObjectType>() -> Self {
        Self::Named {
            object_type_human_name: T::OBJECT_TYPE_HUMAN_NAME,
            object_type_name: any::type_name::<T>(),
        }
    }

    #[inline]
    pub fn type_id_fallback(type_id: any::TypeId) -> Self {
        Self::Id(type_id)
    }

    /// Create a reference to this which will display the metadata in a form suitable for logging.
    #[inline]
    pub(crate) fn log_display_mode(&self) -> ObjectTypeDynMetaLogDisplay<'_> {
        ObjectTypeDynMetaLogDisplay(self)
    }
}

impl Display for ObjectTypeDynMeta {
    #[inline]
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        match self {
            Self::Named {
                object_type_human_name,
                object_type_name,
            } => f.write_fmt(format_args!(
                "{object_type_human_name} [{object_type_name}]"
            )),
            ObjectTypeDynMeta::Id(raw_id) => {
                f.write_fmt(format_args!("<unknown name> [raw_type_id:{raw_id:?}]"))
            }
        }
    }
}

/// Implements specific form of `Display`, object for logging only. Used internally as a component
/// in slog::Value impls.
#[derive(Clone, Debug, Copy)]
pub(crate) struct ObjectTypeDynMetaLogDisplay<'a>(pub &'a ObjectTypeDynMeta);

impl<'a> Display for ObjectTypeDynMetaLogDisplay<'a> {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        match self.0 {
            ObjectTypeDynMeta::Named {
                object_type_human_name,
                object_type_name: _,
            } => f.write_fmt(format_args!("{object_type_human_name}")),
            ObjectTypeDynMeta::Id(id) => f.write_fmt(format_args!("unknownid_{id:?}")),
        }
    }
}

/// Provides a mechanism to filter and extract from certain object types. For example, this may be
/// implemented on the associated data in a top-level [`ObjectType`] enum to enable filtering for
/// only entries of a certain subtype and extracting references to it.
pub trait ObjectComponentType {
    /// The top level object type this might be extracted from.
    type TopLevel: ObjectType;
    type RefError<'r>;
    type MutError<'m>;
    type Error;

    fn extract_ref(top_level: &Self::TopLevel) -> Result<&Self, Self::RefError<'_>>;
    fn extract_mut(top_level: &mut Self::TopLevel) -> Result<&mut Self, Self::MutError<'_>>;
    fn extract(top_level: Self::TopLevel) -> Result<Self, Self::Error>
    where
        Self: Sized,
        Self::TopLevel: Sized;
}

// SDSB - Manage initramfs generators, kernels, and self-controlled secure boot.
// Copyright (C) 2023 Matti Bryce <mattibryce at protonmail dot com>
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <https://www.gnu.org/licenses/>.
