//! Small module for simple CLI component definitions related to objects.

use super::{path, ObjectType};

/// Create an argument parser that parses an object path of the given type.
/// * `alternate_argument_name` - if [`Some`], then this is an alternate argument name to use. By
///   default, [`ObjectType::OBJECT_TYPE_HUMAN_NAME`] is used to construct the argument name.
/// * `alternate_argument_help` - if [`Some`], then this is an alternate help message to use
///   for the argument. By default, the help value is generated from
///   [`ObjectType::object_path_cli_help()`].
///
/// This uses [`ObjectType::OBJECT_PATH_METAVAR`] for the metavariable.
pub fn single_object_path_argument<ForType: ObjectType>(
    alternate_argument_name: Option<&'static str>,
    alternate_argument_help: Option<bpaf::Doc>,
) -> impl bpaf::Parser<path::ObjectPathBuf<ForType>> {
    let arg_name = alternate_argument_name.unwrap_or(ForType::OBJECT_TYPE_HUMAN_NAME);
    bpaf::long(arg_name)
        .help(alternate_argument_help.unwrap_or_else(ForType::object_path_cli_help))
        .argument(ForType::OBJECT_PATH_METAVAR)
}

// SDSB - Manage initramfs generators, kernels, and self-controlled secure boot.
// Copyright (C) 2023 Matti Bryce <mattibryce at protonmail dot com>
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <https://www.gnu.org/licenses/>.
