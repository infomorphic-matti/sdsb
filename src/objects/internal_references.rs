//! Provide a system for objects to describe what other objects they reference by path.
//!
//! This is very important as it allows:
//! * prefetching and pre-resolving of all required object paths, before performing actions
//! * error reporting before we actually end up doing things (e.g. for file parse errors).

use super::{
    path::{ObjectPathRef, TypeErasedObjectPathRef},
    ObjectType, ObjectTypeDynMeta,
};

/// Trait for error types that are capable of being produced by "referenced object visitors"
///
/// In particular, they should allow the option to construct an error stack - this allows, when
/// resolving objects, for a visitor to interrupt the resolution with an error in resolving some
/// referenced object.
pub trait ReferencedObjectVisitorError {
    /// Record that the current shallowest object has failed.
    ///
    /// This should be used to build up a stack from the top (root). It should not be called with
    /// the deepest failed object path (that should be included in the base error).
    fn record_current_object_failure<ForType: ObjectType>(
        &mut self,
        current_object_path: &ObjectPathRef<'_, ForType>,
    ) where
        Self: Sized,
    {
        self.record_current_object_failure_dynamic(
            &current_object_path.erase(),
            Some(ObjectTypeDynMeta::from_type::<ForType>()),
        )
    }

    /// Record that the current shallowest object (with type erased path and optional typename
    /// information - if unspecified it should be derived from id) that has failed due to this error.
    ///
    /// This should be used to build up a stack from the top (root). It should not be called with
    /// the deepest error path.
    fn record_current_object_failure_dynamic(
        &mut self,
        current_object_path: &TypeErasedObjectPathRef<'_>,
        object_type_meta: Option<ObjectTypeDynMeta>,
    );
}

impl<T: ReferencedObjectVisitorError> ReferencedObjectVisitorError for &mut T {
    #[inline]
    fn record_current_object_failure_dynamic(
        &mut self,
        current_object_path: &TypeErasedObjectPathRef<'_>,
        object_type_meta: Option<ObjectTypeDynMeta>,
    ) {
        T::record_current_object_failure_dynamic(self, current_object_path, object_type_meta)
    }

    #[inline]
    fn record_current_object_failure<ForType: ObjectType>(
        &mut self,
        current_object_path: &ObjectPathRef<'_, ForType>,
    ) where
        Self: Sized,
    {
        T::record_current_object_failure(self, current_object_path)
    }
}

impl<T: ReferencedObjectVisitorError> ReferencedObjectVisitorError for Box<T> {
    #[inline]
    fn record_current_object_failure_dynamic(
        &mut self,
        current_object_path: &TypeErasedObjectPathRef<'_>,
        object_type_meta: Option<ObjectTypeDynMeta>,
    ) {
        self.as_mut()
            .record_current_object_failure_dynamic(current_object_path, object_type_meta)
    }

    #[inline]
    fn record_current_object_failure<ForType: ObjectType>(
        &mut self,
        current_object_path: &ObjectPathRef<'_, ForType>,
    ) where
        Self: Sized,
    {
        self.as_mut()
            .record_current_object_failure(current_object_path)
    }
}

/// Trait to allow various object types to provide information on what paths to *other* object
/// types they contain. This is can be used by recursive resolvers.
pub trait ReferencedObjectVisitor {
    type Error: ReferencedObjectVisitorError;
    /// Indicate that the currently visited object contains a reference to the specified path.
    ///
    /// This is allowed to return an error. Objects should propagate this error to callers. This
    /// should also be called on the top level object path as well.
    fn visit_path<ForType: ObjectType>(
        &mut self,
        object_path: ObjectPathRef<'_, ForType>,
    ) -> Result<(), Self::Error>;
}

impl<T: ReferencedObjectVisitor> ReferencedObjectVisitor for &mut T {
    type Error = T::Error;

    #[inline]
    fn visit_path<ForType: ObjectType>(
        &mut self,
        object_path: ObjectPathRef<'_, ForType>,
    ) -> Result<(), Self::Error> {
        T::visit_path(self, object_path)
    }
}

impl<T: ReferencedObjectVisitor> ReferencedObjectVisitor for Box<T> {
    type Error = T::Error;

    #[inline]
    fn visit_path<ForType: ObjectType>(
        &mut self,
        object_path: ObjectPathRef<'_, ForType>,
    ) -> Result<(), Self::Error> {
        T::visit_path(self.as_mut(), object_path)
    }
}

pub mod null_visitor {
    use std::convert::Infallible;

    use super::{ReferencedObjectVisitor, ReferencedObjectVisitorError};
    use crate::objects::{
        path::{ObjectPathRef, TypeErasedObjectPathRef},
        ObjectType, ObjectTypeDynMeta,
    };

    /// Visitor that doesn't do anything with the other paths. Useful for the case where there's no
    /// need to validate internal paths
    #[derive(Debug, Clone, Copy, PartialEq, Eq, PartialOrd, Ord, Hash, Default)]
    pub struct NonVisitor;

    impl ReferencedObjectVisitorError for Infallible {
        #[inline]
        fn record_current_object_failure<ForType: ObjectType>(
            &mut self,
            _current_object_path: &ObjectPathRef<'_, ForType>,
        ) where
            Self: Sized,
        {
        }

        #[inline]
        fn record_current_object_failure_dynamic(
            &mut self,
            _current_object_path: &TypeErasedObjectPathRef<'_>,
            _object_type_meta: Option<ObjectTypeDynMeta>,
        ) {
        }
    }

    impl ReferencedObjectVisitor for NonVisitor {
        type Error = Infallible;

        fn visit_path<ForType: ObjectType>(
            &mut self,
            _object_path: ObjectPathRef<'_, ForType>,
        ) -> Result<(), Self::Error> {
            Ok(())
        }
    }
}

// SDSB - Manage initramfs generators, kernels, and self-controlled secure boot.
// Copyright (C) 2023 Matti Bryce <mattibryce at protonmail dot com>
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <https://www.gnu.org/licenses/>.
