//! Utilities for creating multi-variant types for various [`super::ObjectType`] types.
//!
//! This includes tools for creating CLI args with object paths as well as a macro to enable
//! creating "Multi-Type" versions of these.

/// Macro that can create a family of types, structures, and functions to handle arguments and
/// parameters that may take one of several object types.  
///
/// Of note:
/// * Multi-Path-Type enum - an enumeration over the different [`ObjectPathBuf`][objpathbuf] types for
///   each [`ObjectType`][objtype] -> A [`ObjectPathRef`][objpathref]
///   * Suggested derives for the Buf form:
///     `#[derive(Debug, Clone, PartialEq, Eq, PartialOrd, Ord, Hash)]`
///   * Suggested derives for the Ref form:
///     `#[derive(Debug, Clone, Copy, PartialEq, Eq, PartialOrd, Ord, Hash)]`
/// * Multi-Path-Type parser - a bpaf parser that can parse any of the different path types from
///   CLI into the enum created by this.
/// * Multi-Type enum - an enumeration over the actual [`ObjectType`][objtype]s, which integrates
///   with the Multi-Path-Type enum, and multi-type resolver dbs.
///   * There is the by-value one, the by-ref one, and the by-mut-ref one.
///   * By-ref is recommended to `#[derive(Clone, Copy)]` at the very least.
///
/// Per-Object-Type info:
/// * variant name: The name of the variant associated with this object type in each of the
///   different defined object types.
/// * obj: The actual object type
/// * (optional): path_cli_argument_name - If specified, provide a non-default argument name for specifying
///   a path to this object type in the generated BPAF CLI parser.
/// * (optional): path_cli_help - If specified, this expression is assumed to be a function of the
///   form  `FnOnce() -> some T implementing Into<bpaf::Doc>` to produce the CLI help for the
///   commandline argument used to specify the given object type path in the generated multi-path
///   CLI parser.
///
/// [objpathbuf]: `super::obj_path::ObjectPathBuf`
/// [objpathref]: `super::obj_path::ObjectPathRef`
/// [objtype]: `super::ObjectType`
#[macro_export]
macro_rules! multi_object_definitions {
    (
        names {
            $(#[$multi_path_buf_meta:meta])*
            pathbuf: $multi_path_buf_vis:vis enum $multi_path_buf_name:ident,
            $(#[$multi_path_ref_meta:meta])*
            pathref: $multi_path_ref_vis:vis enum $multi_path_ref_name:ident,
            $(#[$multi_type_meta:meta])*
            multitype: $multi_type_vis:vis enum $multi_type_name:ident,
            $(#[$multi_type_ref_meta:meta])*
            multityperef: $multi_type_ref_vis:vis enum $multi_type_ref_name:ident,
            $(#[$multi_type_mut_meta:meta])*
            multitypemut: $multi_type_mut_vis:vis enum $multi_type_mut_name:ident,
            $(#[$multi_type_path_parser_meta:meta])*
            pathparser: $multi_path_parser_vis:vis fn $multi_path_parser:ident $(,)?
        } $(,)?
        $(ty {
            variant: $object_type_variant_name:ident,
            obj: $object_type:ty,
            $(path_cli_argument_name: $object_cli_custom_arg_name:literal,)?
            $(path_cli_help: $object_cli_documentation_generator_fn:expr,)?
        })*
    ) => {
        $(#[$multi_path_buf_meta])*
        $multi_path_buf_vis enum $multi_path_buf_name {
            $($object_type_variant_name($crate::objects::path::ObjectPathBuf::<$object_type>),)*
        }

        impl $multi_path_buf_name {
            pub fn erase(self) -> $crate::objects::path::TypeErasedObjectPathBuf {
                use $multi_path_buf_name as S;
                match self {
                    $(S::$object_type_variant_name(inner_path) => inner_path.erase(),)*
                }
            }

            #[doc = concat!("Same as [`", stringify!($multi_path_buf_name), "::erase`], but automatically create some object type metadata too for nice errors.")]
            pub fn erase_with_meta(self) -> ($crate::objects::path::TypeErasedObjectPathBuf, $crate::objects::ObjectTypeDynMeta) {
                use $multi_path_buf_name as S;
                match self {
                    $(S::$object_type_variant_name(inner_path) => inner_path.erase_with_meta(),)*
                }
            }

            /// Automatically derive object type metadata for this path.
            #[inline]
            pub fn derive_meta(&self) -> $crate::objects::ObjectTypeDynMeta {
                use $multi_path_buf_name as S;
                match self {
                    $(S::$object_type_variant_name(inner_path) => inner_path.derive_meta(),)*
                }
            }

            /// Get the XDG Category of the path contained by this
            #[inline]
            pub fn xdg_category(&self) -> $crate::objects::XDGCategory {
                use $multi_path_buf_name as S;
                use $crate::objects::ObjectType as OT;
                match self {
                   $(S::$object_type_variant_name(_) => <$object_type as OT>::XDG_CATEGORY,)*
                }
            }

            #[doc = concat!("Convert this into a [`", stringify!($multi_path_ref_name), "`]")]
            pub fn as_ref(&self) -> $multi_path_ref_name<'_> {
                use $multi_path_buf_name as S;
                use $multi_path_ref_name as R;
                use $crate::objects::path::ObjectPathBuf as OPB;
                match self {
                    $(S::$object_type_variant_name(inner_path) => R::$object_type_variant_name(OPB::as_ref(inner_path)),)*
                }
            }
        }

        // path borrowing.
        impl $crate::objects::path::PathBorrow for $multi_path_buf_name {
            type BorrowedPath<'q> = $multi_path_ref_name<'q> where Self: 'q;

            #[inline]
            fn borrow_path(&self) -> Self::BorrowedPath<'_> {
                self.as_ref()
            }
        }


        // Path erasure
        impl $crate::objects::path::ErasablePath for $multi_path_buf_name {
            type Erased = $crate::objects::path::TypeErasedObjectPathBuf;

            #[inline]
            fn erase_with_meta(self) -> (Self::Erased, $crate::objects::ObjectTypeDynMeta) {
                Self::erase_with_meta(self)
            }
        }

        // From impls :)
        $(impl ::core::convert::From<$crate::objects::path::ObjectPathBuf::<$object_type>>
            for $multi_path_buf_name {
            #[inline]
            fn from(v: $crate::objects::path::ObjectPathBuf::<$object_type>) -> Self {
                Self::$object_type_variant_name(v)
            }
        })*

        // TryFrom impl for the object paths.
        impl ::core::convert::TryFrom<$crate::objects::path::TypeErasedObjectPathBuf>
            for $multi_path_buf_name {
            type Error = $crate::objects::path::TypeErasedObjectPathBuf;

            fn try_from(erased: $crate::objects::path::TypeErasedObjectPathBuf) -> ::core::result::Result<Self, Self::Error> {
                /* TODO:
                 * Const type ID is not stable yet or we would do this to allow the compiler to
                 * optimize the match
                $(const $object_type_variant_name: ::core::any::TypeId = ::core::any::TypeId::of::<$object_type>();)*
                match erased.object_type_id() {
                    $($object_type_variant_name => erased.with_object_type::<$object_type>().map(Self::$object_type_variant_name),)*
                    _ => Err(erased)
                }
                */
                $(let erased = match erased.with_object_type::<$object_type>() {
                    Ok(devirtualized) => return Ok(Self::$object_type_variant_name(devirtualized)),
                    Err(still_erased) => still_erased
                };)*
                Err(erased)
            }
        }

        // From impl for the TypeErasedObjectPathBuf using the multipathbuf
        impl ::core::convert::From<$multi_path_buf_name> for $crate::objects::path::TypeErasedObjectPathBuf {
            fn from(v: $multi_path_buf_name) -> $crate::objects::path::TypeErasedObjectPathBuf {
                use $multi_path_buf_name as S;
                use $crate::objects::path::TypeErasedObjectPathBuf as TE;
                match v {
                    $(S::$object_type_variant_name(inner_path) => <TE as ::core::convert::From::<_>>::from(inner_path),)*
                }
            }
        }


        $(#[$multi_path_ref_meta])*
        $multi_path_ref_vis enum $multi_path_ref_name<'a> {
            $($object_type_variant_name($crate::objects::path::ObjectPathRef::<'a, $object_type>),)*
        }

        impl<'l> $multi_path_ref_name<'l> {
            pub fn to_owned(&self) -> $multi_path_buf_name {
                use $multi_path_ref_name as S;
                use $multi_path_buf_name as T;
                use $crate::objects::path::ObjectPathRef as OPR;
                match self {
                    $(S::$object_type_variant_name(v) => T::$object_type_variant_name(OPR::to_owned(v)),)*
                }
            }

            /// the length of the underlying path string - should always be non-zero
            pub fn str_len(&self) -> usize {
                use $multi_path_ref_name as S;
                use $crate::objects::path::ObjectPathRef as OPR;
                match self {
                   $(S::$object_type_variant_name(v) => OPR::str_len(v),)*
                }
            }

            /// Build a relative path on the filesystem from the components of this object path as well as
            /// the object type's base path. This constructs a pathbuf that `includes` the file
            /// suffix.
            ///
            /// You can easily find the path of the directory variant of this object location (for e.g.
            /// when searching for children) using [`PathBuf::set_extension`] with an empty string.
            ///
            /// (note that it would be possible for rust to implement some kind of `without_extension` on
            /// simple [`std::path::Path`] references, but this is not currently a thing, unfortunately :(
            /// )
            pub fn build_relative_fs_path(&self) -> ::std::path::PathBuf {
                use $multi_path_ref_name as S;
                use $crate::objects::path::ObjectPathRef as OPR;
                match self {
                    $(S::$object_type_variant_name(v) => OPR::build_relative_fs_path(v),)*
                }
            }

            pub fn erase(&self) -> $crate::objects::path::TypeErasedObjectPathRef<'l> {
                use $multi_path_ref_name as S;
                match self {
                    $(S::$object_type_variant_name(inner_path) => inner_path.erase(),)*
                }
            }

            #[doc = concat!("Same as [`", stringify!($multi_path_ref_name), "::erase`], but automatically create some object type metadata too for nice errors.")]
            pub fn erase_with_meta(&self) -> ($crate::objects::path::TypeErasedObjectPathRef<'l>, $crate::objects::ObjectTypeDynMeta) {
                use $multi_path_ref_name as S;
                match self {
                    $(S::$object_type_variant_name(inner_path) => inner_path.erase_with_meta(),)*
                }
            }

            /// Automatically derive object type metadata for this path.
            #[inline]
            pub fn derive_meta(&self) -> $crate::objects::ObjectTypeDynMeta {
                use $multi_path_ref_name as S;
                match self {
                    $(S::$object_type_variant_name(inner_path) => inner_path.derive_meta(),)*
                }
            }

            /// Get the XDG Category of the path contained by this
            #[inline]
            pub fn xdg_category(&self) -> $crate::objects::XDGCategory {
                use $multi_path_ref_name as S;
                use $crate::objects::ObjectType as OT;
                match self {
                   $(S::$object_type_variant_name(_) => <$object_type as OT>::XDG_CATEGORY,)*
                }
            }
        }

        // path borrowing.
        impl<'r> $crate::objects::path::PathBorrow for $multi_path_ref_name<'r> {
            type BorrowedPath<'q> = $multi_path_ref_name<'q> where Self: 'q;

            #[inline]
            fn borrow_path(&self) -> Self::BorrowedPath<'_> {
                *self
            }
        }

        // Path erasure
        impl <'r> $crate::objects::path::ErasablePath for $multi_path_ref_name<'r> {
            type Erased = $crate::objects::path::TypeErasedObjectPathRef<'r>;

            #[inline]
            fn erase_with_meta(self) -> (Self::Erased, $crate::objects::ObjectTypeDynMeta) {
                Self::erase_with_meta(&self)
            }
        }

        // Specific referential path erasure.
        impl <'r> $crate::objects::path::SpecificErasablePathRef<'r> for $multi_path_ref_name<'r> {
            #[inline]
            fn erase_with_meta(self) -> (
                $crate::objects::path::TypeErasedObjectPathRef<'r>,
                $crate::objects::ObjectTypeDynMeta
            ) {
                Self::erase_with_meta(&self)
            }

        }

        // From impls :)
        $(impl<'l> ::core::convert::From<$crate::objects::path::ObjectPathRef::<'l, $object_type>>
            for $multi_path_ref_name<'l> {
            #[inline]
            fn from(v: $crate::objects::path::ObjectPathRef::<'l, $object_type>) -> Self {
                Self::$object_type_variant_name(v)
            }
        })*

        // TryFrom impl for the object path refs.
        impl<'l> ::core::convert::TryFrom<$crate::objects::path::TypeErasedObjectPathRef<'l>>
            for $multi_path_ref_name<'l> {
            type Error = $crate::objects::path::TypeErasedObjectPathRef<'l>;

            fn try_from(erased: $crate::objects::path::TypeErasedObjectPathRef<'l>) -> ::core::result::Result<Self, Self::Error> {
                /* TODO:
                 * Const type ID is not stable yet or we would do this to allow the compiler to
                 * optimize the match
                $(const $object_type_variant_name: ::core::any::TypeId = ::core::any::TypeId::of::<$object_type>();)*
                match erased.object_type_id() {
                    $($object_type_variant_name => erased.with_object_type::<$object_type>().map(Self::$object_type_variant_name),)*
                    _ => Err(erased)
                }
                */
                $(let erased = match erased.with_object_type::<$object_type>() {
                    Ok(devirtualized) => return Ok(Self::$object_type_variant_name(devirtualized)),
                    Err(still_erased) => still_erased
                };)*
                Err(erased)
            }
        }




        // Path parser
        $(#[$multi_type_path_parser_meta])*
        // This is because we use the type names as variable names for the different sub-parsers.
        #[allow(non_snake_case)]
        $multi_path_parser_vis fn $multi_path_parser() -> impl ::bpaf::Parser::<$multi_path_buf_name> {
            use ::bpaf::Parser;
            // Create the argument parsers - note that we don't do this all at once inside the
            // ::bpaf::contruct! macro, because of weird things (due to the order of recursive
            // expansion :/)
            $(let $object_type_variant_name = $crate::objects::cli::single_object_path_argument::<$object_type>(
                $crate::multi_object_definitions!(@optional_option $(some $object_cli_custom_arg_name)?),
                $crate::multi_object_definitions!(@optional_option $(some ::core::into::Into::into(($object_cli_documentation_generator_fn)()))?)

            ).map($multi_path_buf_name::$object_type_variant_name);)*
            ::bpaf::construct!([$($object_type_variant_name,)*])
        }

        // OBJECT MULTITYPE GENERATION

        $(#[$multi_type_meta])*
        $multi_type_vis enum $multi_type_name {
            $($object_type_variant_name($object_type),)*
        }

        $(#[$multi_type_ref_meta])*
        $multi_type_ref_vis enum $multi_type_ref_name<'l> {
            $($object_type_variant_name(&'l $object_type),)*
        }

        $(#[$multi_type_mut_meta])*
        $multi_type_mut_vis enum $multi_type_mut_name<'m> {
            $($object_type_variant_name(&'m mut $object_type),)*
        }

        // From impls
        $(impl ::core::convert::From::<$object_type> for $multi_type_name {
            #[inline]
            fn from(v: $object_type) -> $multi_type_name {
                use $multi_type_name as S;
                S::$object_type_variant_name(v)
            }
        })*

        $(impl<'l> ::core::convert::From::<&'l $object_type> for $multi_type_ref_name<'l> {
            #[inline]
            fn from(v: &'l $object_type) -> $multi_type_ref_name {
                use $multi_type_ref_name as S;
                S::$object_type_variant_name(v)
            }
        })*

        $(impl<'l> ::core::convert::From::<&'l mut $object_type> for $multi_type_mut_name<'l> {
            #[inline]
            fn from(v: &'l mut $object_type) -> $multi_type_mut_name {
                use $multi_type_mut_name as S;
                S::$object_type_variant_name(v)
            }
        })*

        impl $multi_type_name {
            /// Get the version of this that is by-ref
            pub fn as_ref(&self) -> $multi_type_ref_name<'_> {
                use $multi_type_name as S;
                use $multi_type_ref_name as T;
                match self {
                    $(S::$object_type_variant_name(v) => T::$object_type_variant_name(v),)*
                }
            }

            /// Get the version of this that is by-mut
            pub fn as_mut(&mut self) -> $multi_type_mut_name<'_> {
                use $multi_type_name as S;
                use $multi_type_mut_name as T;
                match self {
                    $(S::$object_type_variant_name(v) => T::$object_type_variant_name(v),)*
                }
            }
        }

        impl <'r> $multi_type_mut_name<'r> {
            /// Get this in ref-type form
            pub fn into_ref(self) -> $multi_type_ref_name<'r> {
                use $multi_type_mut_name as S;
                use $multi_type_ref_name as T;
                match self {
                   $(S::$object_type_variant_name(v_mut) => T::$object_type_variant_name(&*v_mut),)*
                }
            }

            /// Get this "as" a shorter-lifetime ref-type form
            pub fn as_ref(&self) -> $multi_type_ref_name<'_> {
                use $multi_type_mut_name as S;
                use $multi_type_ref_name as T;
                match self {
                   $(S::$object_type_variant_name(v_mut) => T::$object_type_variant_name(&*v_mut),)*
                }
            }
        }

        impl<'r> ::core::convert::From<$multi_type_mut_name<'r>> for $multi_type_ref_name<'r> {
            #[inline]
            fn from(v_mut: $multi_type_mut_name<'r>) -> Self {
                v_mut.into_ref()
            }
        }

        // MULTI-TYPE-PATH RESOLUTION - (refs)
        impl <'r, E, DB: ?Sized $(+ $crate::resolve::FlatResolver<$object_type, E>)*>
            $crate::resolve::FlatResolve<DB, E> for $multi_path_ref_name<'r> {
            type Output<'rdb> = $multi_type_mut_name<'rdb> where DB: 'rdb;

            fn resolve_and_load_unchecked_in_db<'rdb>(
                &self,
                resolver_db: &'rdb mut DB,
                logger: ::core::option::Option::<::slog::Logger>,
            ) -> ::core::result::Result<Self::Output<'rdb>, E> {
                use $multi_path_ref_name as S;
                use $multi_type_mut_name as T;
                match self {
                    $(S::$object_type_variant_name(object_path_ref) => {
                        let resolved = <DB as $crate::resolve::FlatResolver::<$object_type, E>>::resolve_and_load_unchecked(
                            resolver_db,
                            *object_path_ref,
                            logger
                        )?;
                        Ok(T::$object_type_variant_name(resolved))
                    },)*
                }
            }


            fn is_loaded_in(&self, db: &DB) -> bool {
                use $multi_path_ref_name as S;
                match self {
                   $(S::$object_type_variant_name(object_path_ref) => {
                        <DB as $crate::resolve::FlatResolver<$object_type, E>>::is_loaded(db, *object_path_ref)
                   },)*
                }
            }
        }


        // MULTI-TYPE-PATH RESOLUTION - (bufs)
        impl <E, DB: ?Sized $(+ $crate::resolve::FlatResolver<$object_type, E>)*>
            $crate::resolve::FlatResolve<DB, E> for $multi_path_buf_name {
            type Output<'rdb> = $multi_type_mut_name<'rdb> where DB: 'rdb;

            fn resolve_and_load_unchecked_in_db<'rdb>(
                &self,
                resolver_db: &'rdb mut DB,
                logger: ::core::option::Option::<::slog::Logger>,
            ) -> ::core::result::Result<Self::Output<'rdb>, E> {
                use $multi_path_buf_name as S;
                use $multi_type_mut_name as T;
                match self {
                    $(S::$object_type_variant_name(object_path_buf) => {
                        let resolved = <DB as $crate::resolve::FlatResolver::<$object_type, E>>::resolve_and_load_unchecked(
                            resolver_db,
                            object_path_buf.as_ref(),
                            logger
                        )?;
                        Ok(T::$object_type_variant_name(resolved))
                    },)*
                }
            }

            fn is_loaded_in(&self, db: &DB) -> bool {
                use $multi_path_buf_name as S;
                match self {
                   $(S::$object_type_variant_name(object_path_buf) => {
                        <DB as $crate::resolve::FlatResolver<$object_type, E>>::is_loaded(db, object_path_buf.as_ref())
                   },)*
                }
            }
        }

        // implementations of OBJECT REFERENTIALITY, for ref, mut, and value.

        impl $crate::objects::ObjectReferential for $multi_type_name {
            fn describe_referenced_paths<V: $crate::objects::internal_references::ReferencedObjectVisitor>(
                &self,
                path_visitor: &mut V
            ) -> Result<(), V::Error> {
                match self {
                    $(Self::$object_type_variant_name(obj) => $crate::objects::ObjectReferential::describe_referenced_paths(obj, path_visitor)),*
                }
            }
        }

        impl<'r> $crate::objects::ObjectReferential for $multi_type_ref_name<'r> {
            fn describe_referenced_paths<V: $crate::objects::internal_references::ReferencedObjectVisitor>(
                &self,
                path_visitor: &mut V
            ) -> Result<(), V::Error> {
                match self {
                    $(Self::$object_type_variant_name(obj) => <$object_type as $crate::objects::ObjectReferential>::describe_referenced_paths(obj, path_visitor)),*
                }
            }
        }

        impl<'r> $crate::objects::ObjectReferential for $multi_type_mut_name<'r> {
            fn describe_referenced_paths<V: $crate::objects::internal_references::ReferencedObjectVisitor>(
                &self,
                path_visitor: &mut V
            ) -> Result<(), V::Error> {
                match self {
                    $(Self::$object_type_variant_name(obj) => <$object_type as $crate::objects::ObjectReferential>::describe_referenced_paths(obj, path_visitor)),*
                }
            }
        }
    };
    // Resolve an optional (?) macro arg into an `Option`
    (@optional_option some $expr:expr) => {
        ::core::option::Option::Some($expr)
    };
    (@optional_option) => {
        ::core::option::Option::None
    }
}

multi_object_definitions! {
    names {
        /// Contains any well-recognised object type's path.
        #[derive(Debug, Clone, PartialEq, Eq, PartialOrd, Ord, Hash)]
        pathbuf: pub enum AnyObjectPathBuf,
        /// Contains any well-recognised object type's path, by ref
        #[derive(Debug, Clone, Copy, PartialEq, Eq, PartialOrd, Ord, Hash)]
        pathref: pub enum AnyObjectPathRef,
        /// Contains any well-recognised object type
        #[derive(Debug, Clone)]
        multitype: pub enum AnyObject,
        /// Contains a reference to any well-recognised object type.
        #[derive(Debug, Clone, Copy)]
        multityperef: pub enum AnyObjectRef,
        /// Contains a mutable reference to any well-recognised object type.
        #[derive(Debug)]
        multitypemut: pub enum AnyObjectMut,
        /// Provide a parser for a CLI argument that can take any object path.
        pathparser: pub fn any_object_path_argument
    }
    ty {
        variant: Platform,
        obj: crate::platform::Platform,
    }
    ty {
        variant: InitRAMFSGen,
        obj: crate::platform::linux::initramfs::InitRAMFSGen,
    }
}

// SDSB - Manage initramfs generators, kernels, and self-controlled secure boot.
// Copyright (C) 2023 Matti Bryce <mattibryce at protonmail dot com>
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <https://www.gnu.org/licenses/>.
