//! Module for object path stuff
use std::{any, marker::PhantomData, num::NonZeroUsize, path::PathBuf};

use chain_trans::Trans;
use thiserror::Error;

use super::{ObjectType, ObjectTypeDynMeta};

#[derive(Debug, Clone, Copy, PartialEq, Eq, Error)]
pub enum InvalidPathError {
    #[error("Provided path contains an empty segment (at position {at})")]
    EmptySegment { at: usize },
}

impl serde::de::Expected for InvalidPathError {
    fn fmt(&self, formatter: &mut std::fmt::Formatter) -> std::fmt::Result {
        match self {
            InvalidPathError::EmptySegment { at } => formatter.write_fmt(format_args!(
                "a path divided by '.'-separators containing no empty segments (empty segment at {at})"
            )),
        }
    }
}

/// Check if the path is valid or not. If not, return an error.
pub fn check_path_format(path: &str) -> Option<InvalidPathError> {
    path.split('.')
        .scan(0, |start_position, current_path_entry| {
            let start_position_of_current = *start_position;
            // 1 is for the '.' after this one, the
            *start_position += 1 + current_path_entry.len();
            Some((start_position_of_current, current_path_entry))
        })
        .try_fold(
            (),
            |_, (start_position_of_current, current_path_entry)| -> Result<(), InvalidPathError> {
                if !current_path_entry.is_empty() {
                    Ok(())
                } else {
                    Err(InvalidPathError::EmptySegment {
                        at: start_position_of_current,
                    })
                }
            },
        )
        .err()
}

#[cfg(test)]
mod test_path_validation {
    use super::{check_path_format, InvalidPathError};

    #[test]
    pub fn path_validation() {
        assert_eq!(check_path_format("a.b"), None);
        assert_eq!(check_path_format("a.djksaf.ccasdj.aaJJFAS"), None);
        assert_eq!(
            check_path_format(".skAKDmsaf.aa.d.fggg"),
            Some(InvalidPathError::EmptySegment { at: 0 })
        );
        assert_eq!(
            check_path_format("asdask.asdkmf.bfksd..sakSDA"),
            Some(InvalidPathError::EmptySegment { at: 20 })
        )
    }
}

/// Path to an object. Note that this uses [`Box<str>`] internally - mutability is the opposite of
/// desirable for these, and box lets us avoid being Weird about it.
///
/// Equivalent to a series of strings separated by a dot, with conditions. In particular, no
/// component can be empty. This means no dot at the start or the end, and no two+ dots adjacent to
/// each other. It also means that no "empty" path can exist. All object paths are implicitly
/// relative by construction.
///
/// Strings can be checked for validity with [`check_path_format`].
#[derive(Debug)]
pub struct ObjectPathBuf<ForType>(PhantomData<fn() -> ForType>, Box<str>);

impl<ForType> ObjectPathBuf<ForType> {
    /// Attempt to convert a string into an object path.
    ///
    /// If it fails, then return an error and the invalid value you passed.
    #[inline]
    pub fn try_from_string<I: Into<Box<str>> + AsRef<str>>(
        string: I,
    ) -> Result<Self, (InvalidPathError, I)> {
        match check_path_format(string.as_ref()) {
            Some(err) => Err((err, string)),
            None => Ok(unsafe { Self::from_string_unchecked(string.into()) }),
        }
    }

    /// Construct an object path from a string without checking for validity.
    ///
    /// # Safety
    /// Ensure that the path is valid before passing to this function. For an easy way to check
    /// this, look at [`check_path_format`]
    #[inline]
    pub unsafe fn from_string_unchecked(unchecked_path: impl Into<Box<str>>) -> Self {
        Self(Default::default(), unchecked_path.into())
    }

    /// Convert this to a raw path.
    #[inline]
    pub fn as_ref(&self) -> ObjectPathRef<'_, ForType> {
        // Validation criteria are the same for [`ObjectPathRef`] as for us, so this is fine :)
        ObjectPathRef(Default::default(), self.1.as_ref())
    }

    #[inline]
    pub fn as_str(&self) -> &str {
        self.1.as_ref()
    }

    #[inline]
    pub fn erase(self) -> TypeErasedObjectPathBuf
    where
        ForType: ObjectType,
    {
        let type_id = any::TypeId::of::<ForType>();
        TypeErasedObjectPathBuf {
            object_type: type_id,
            object_path: self.1,
        }
    }

    /// Same as [`ObjectPathBuf::erase`], but automatically create some object type metadata
    /// structure too, for nice errors.
    #[inline]
    pub fn erase_with_meta(self) -> (TypeErasedObjectPathBuf, ObjectTypeDynMeta)
    where
        ForType: ObjectType,
    {
        let obj_meta = self.derive_meta();
        (self.erase(), obj_meta)
    }

    /// Automatically derive object type metadata
    #[inline]
    pub fn derive_meta(&self) -> ObjectTypeDynMeta
    where
        ForType: ObjectType,
    {
        ObjectTypeDynMeta::from_type::<ForType>()
    }
}

mod objpathbuf_impls {
    use core::str::FromStr;
    use std::{borrow::Borrow, fmt::Display, hash::Hash, marker::PhantomData, ops::Deref};

    use super::{InvalidPathError, ObjectPathBuf, ObjectPathRef};
    use crate::objects::ObjectType;

    /// Custom implementation to avoid :Clone bound on the ForType
    impl<ForType> Clone for ObjectPathBuf<ForType> {
        #[inline]
        fn clone(&self) -> Self {
            Self(Default::default(), self.1.clone())
        }
    }

    // We implement rather than derive to avoid requirements being put on [`ForType`] for
    // PartialEq/Ord/Hash/etc.
    impl<ForType> PartialEq for ObjectPathBuf<ForType> {
        #[inline]
        fn eq(&self, other: &Self) -> bool {
            self.1.as_ref().eq(other.1.as_ref())
        }
    }

    impl<ForType> Eq for ObjectPathBuf<ForType> {}

    impl<ForType> PartialOrd for ObjectPathBuf<ForType> {
        #[inline]
        fn partial_cmp(&self, other: &Self) -> Option<std::cmp::Ordering> {
            Some(self.cmp(other))
        }
    }

    impl<ForType> Ord for ObjectPathBuf<ForType> {
        #[inline]
        fn cmp(&self, other: &Self) -> std::cmp::Ordering {
            self.1.as_ref().cmp(other.1.as_ref())
        }
    }

    impl<ForType> Hash for ObjectPathBuf<ForType> {
        #[inline]
        fn hash<H: std::hash::Hasher>(&self, state: &mut H) {
            self.0.hash(state);
            self.1.hash(state)
        }
    }

    impl<ForType: ObjectType> FromStr for ObjectPathBuf<ForType> {
        type Err = InvalidPathError;

        #[inline]
        fn from_str(s: &str) -> Result<Self, Self::Err> {
            Self::try_from(s.to_owned())
        }
    }

    impl<ForType: ObjectType> TryFrom<String> for ObjectPathBuf<ForType> {
        type Error = InvalidPathError;

        #[inline]
        fn try_from(value: String) -> Result<Self, Self::Error> {
            Self::try_from_string(value).map_err(|e| e.0)
        }
    }

    impl<ForType: ObjectType> Display for ObjectPathBuf<ForType> {
        #[inline]
        fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
            f.write_str(self.1.as_ref())
        }
    }

    impl<ForType: ObjectType> Deref for ObjectPathBuf<ForType> {
        type Target = str;

        #[inline]
        fn deref(&self) -> &Self::Target {
            &self.1
        }
    }

    impl<ForType> Borrow<str> for ObjectPathBuf<ForType> {
        #[inline]
        fn borrow(&self) -> &str {
            &self.1
        }
    }

    impl<ForType> PartialEq<str> for ObjectPathBuf<ForType> {
        #[inline]
        fn eq(&self, other: &str) -> bool {
            self.as_ref().eq(other)
        }
    }

    impl<'l, ForType> PartialEq<ObjectPathRef<'l, ForType>> for ObjectPathBuf<ForType> {
        #[inline]
        fn eq(&self, other: &ObjectPathRef<'l, ForType>) -> bool {
            self.as_ref().eq(other)
        }
    }

    impl<'de, ForType: ObjectType> serde::de::Deserialize<'de> for ObjectPathBuf<ForType> {
        fn deserialize<D>(deserializer: D) -> Result<Self, D::Error>
        where
            D: serde::Deserializer<'de>,
        {
            struct Visitor<ForType>(PhantomData<*const ForType>);

            impl<'de, ForType: ObjectType> serde::de::Visitor<'de> for Visitor<ForType> {
                type Value = ObjectPathBuf<ForType>;

                fn expecting(&self, formatter: &mut std::fmt::Formatter) -> std::fmt::Result {
                    formatter.write_str("a path using the '.'-separator with no empty segments")
                }

                #[inline]
                fn visit_string<E>(self, v: String) -> Result<Self::Value, E>
                where
                    E: serde::de::Error,
                {
                    ObjectPathBuf::try_from_string(v).map_err(|(e, v)| {
                        E::invalid_value(serde::de::Unexpected::Str(v.as_str()), &e)
                    })
                }

                #[inline]
                fn visit_str<E>(self, v: &str) -> Result<Self::Value, E>
                where
                    E: serde::de::Error,
                {
                    Self::visit_string(self, v.to_owned())
                }
            }

            deserializer.deserialize_string(Visitor(Default::default()))
        }
    }

    impl<ForType: ObjectType> serde::ser::Serialize for ObjectPathBuf<ForType> {
        #[inline]
        fn serialize<S>(&self, serializer: S) -> Result<S::Ok, S::Error>
        where
            S: serde::Serializer,
        {
            serializer.serialize_newtype_struct("ObjectPathBuf", &self.1)
        }
    }
}

/// Path to an object.
///
/// We do not do this as a Dynamically Sized Type - the PhantomData necessary interferes with the
/// direct transmutation that would be used Deref. However, paths are meant to be immutable-ish, so
/// it is fine to use a normal reference.
///
/// Edit2 - #[repr(transparent)] might allow for Zero-Sized Types as components, to exist when transmuting for
/// Dynamically-Sized-Types
/// TODO: switch all this infrastructure to DSTs at somepoint
///
/// Note - Copy and Clone behave weird with auto impls, I can't get copy on this type at all.
#[derive(Debug)]
pub struct ObjectPathRef<'a, ForType>(PhantomData<fn() -> ForType>, &'a str);

impl<'a, ForType> ObjectPathRef<'a, ForType> {
    /// Attempt to create a path reference from a string
    #[inline]
    pub fn try_from_str(path: &'a str) -> Result<Self, InvalidPathError> {
        match check_path_format(path) {
            Some(err) => Err(err),
            // SAFETY: checked
            None => unsafe { Ok(Self::from_str_unchecked(path)) },
        }
    }

    /// Construct this from a str ref, assuming that it is valid.
    ///
    /// # Safety
    /// Ensure the string passed is a valid object path - see: [`check_path_format`]
    #[inline]
    pub unsafe fn from_str_unchecked(path: &'a str) -> Self {
        Self(Default::default(), path)
    }

    #[inline]
    pub fn to_owned(&self) -> ObjectPathBuf<ForType> {
        unsafe { ObjectPathBuf::from_string_unchecked(self.1.to_owned()) }
    }

    #[inline]
    pub fn as_str(&self) -> &'a str {
        self.1
    }

    /// Remove the first component of this path and return it, as well as a new path encompassing
    /// any remaining path elements.
    ///
    /// Paths cannot be empty - if the second part of the tuple is [`None`], then this was the last
    /// component.
    pub fn split_shallowest_component(&self) -> (&'a str, Option<Self>) {
        match self.1.split_once('.') {
            Some((parent, remaining_children)) => {
                // Given that by construction we asserted that there are no empty components, both
                // halves must be valid paths (the first is just a single component, the second is
                // the leftover components :))
                (
                    parent,
                    // safety: splitting on `.` and getting result implies the second component is
                    // a valid path.
                    Some(unsafe { Self::from_str_unchecked(remaining_children) }),
                )
            }
            None => (self.1, None),
        }
    }

    /// Like [`Self::split_shallowest_component`], but work upwards from the deepest/most-specific
    /// part of the path rather than downwards.
    pub fn rsplit_deepest_component(&self) -> (Option<Self>, &'a str) {
        match self.1.rsplit_once('.') {
            Some((remaining_parent, child)) => {
                // # SAFETY:
                // Same reasoning as applies in shallowest, just reversed in direction
                (
                    Some(unsafe { Self::from_str_unchecked(remaining_parent) }),
                    child,
                )
            }
            None => (None, self.1),
        }
    }

    /// The length of the underlying path string.
    ///
    /// Should always be non-zero.
    #[inline]
    pub fn str_len(&self) -> usize {
        self.1.len()
    }

    /// Number of components in this path
    /// Count the number of components in this path. Note that this is guarunteed nonzero because
    /// of the properties of paths. However, this is an operation linear in the length of the path.
    pub fn components_count(&self) -> NonZeroUsize {
        self.1
            .split('.')
            .count()
            .trans(NonZeroUsize::try_from)
            .expect("Split always produces at least one value")
    }

    /// Create an iterator traversing the path components from the top level downward.
    #[inline]
    pub fn into_downward_component_iterator(
        &self,
    ) -> iter::ObjectPathComponentsDownward<'a, ForType>
    where
        ForType: ObjectType,
    {
        iter::ObjectPathComponentsDownward::from(*self)
    }

    /// Create an iterator traversing the path components from the bottom level upward
    #[inline]
    pub fn into_upward_component_iterator(&self) -> iter::ObjectPathComponentsUpward<'a, ForType>
    where
        ForType: ObjectType,
    {
        iter::ObjectPathComponentsUpward::from(*self)
    }

    /// Build a relative path on the filesystem from the components of this object path as well as
    /// the object type's base path. This constructs a pathbuf that `includes` the file suffix at
    /// [`super::CONFIG_FILE_FORMAT_SUFFIX`].
    ///
    /// You can easily find the path of the directory variant of this object location (for e.g.
    /// when searching for children) using [`PathBuf::set_extension`] with an empty string.
    ///
    /// (note that it would be possible for rust to implement some kind of `without_extension` on
    /// simple [`std::path::Path`] references, but this is not currently a thing, unfortunately :(
    /// )
    pub fn build_relative_fs_path(&self) -> PathBuf
    where
        ForType: ObjectType,
    {
        // Length of the base path - +1 is for the future /
        let object_type_base_path_len: usize = ForType::RELATIVE_STORAGE_PATH
            .iter()
            .map(|a| a.len() + 1)
            .sum();
        // length of self (the `.` are going to be replaced with `/`)
        let object_path_length = self.str_len();
        // `+2` is for the dot but also a generic buffer.
        let extension_len = crate::resolve::CONFIG_FILE_FORMAT_SUFFIX.len() + 2;
        let mut path =
            PathBuf::with_capacity(object_type_base_path_len + object_path_length + extension_len);
        let directory_components = ForType::RELATIVE_STORAGE_PATH
            .iter()
            .copied()
            .chain(self.into_downward_component_iterator());
        path.extend(directory_components);
        // Note that object name components cannot have `.` in them, so `set_extension` always
        // goes on the end rather than replacing some kind of weird name ^.^
        path.set_extension(crate::resolve::CONFIG_FILE_FORMAT_SUFFIX);
        path
    }

    /// Type-erase this object path reference
    #[inline]
    pub fn erase(&self) -> TypeErasedObjectPathRef<'a>
    where
        ForType: ObjectType,
    {
        TypeErasedObjectPathRef {
            object_type: any::TypeId::of::<ForType>(),
            object_path: self.1,
        }
    }

    /// Like [`ObjectPathRef::erase`] but also provide metadata
    #[inline]
    pub fn erase_with_meta(&self) -> (TypeErasedObjectPathRef<'a>, ObjectTypeDynMeta)
    where
        ForType: ObjectType,
    {
        let meta = self.derive_meta();
        (self.erase(), meta)
    }

    /// Automatically derive object type metadata
    #[inline]
    pub fn derive_meta(&self) -> ObjectTypeDynMeta
    where
        ForType: ObjectType,
    {
        ObjectTypeDynMeta::from_type::<ForType>()
    }
}

mod objpathref_impls {
    use std::{borrow::Borrow, fmt::Display, marker::PhantomData, ops::Deref};

    use super::ObjectPathRef;
    use crate::objects::ObjectType;

    impl<'q, ForType> core::hash::Hash for ObjectPathRef<'q, ForType> {
        fn hash<H: std::hash::Hasher>(&self, state: &mut H) {
            self.0.hash(state);
            self.1.hash(state)
        }
    }

    impl<'l, ForType> Copy for ObjectPathRef<'l, ForType> {}

    // Manual implementation to avoid requirement on ForType
    impl<'l, ForType> Clone for ObjectPathRef<'l, ForType> {
        #[inline]
        fn clone(&self) -> Self {
            *self
        }
    }

    impl<'l, ForType> Deref for ObjectPathRef<'l, ForType> {
        type Target = str;

        #[inline]
        fn deref(&self) -> &Self::Target {
            self.1
        }
    }

    impl<'l, ForType> AsRef<str> for ObjectPathRef<'l, ForType> {
        #[inline]
        fn as_ref(&self) -> &str {
            self.1
        }
    }

    impl<'l, ForType: ObjectType> Display for ObjectPathRef<'l, ForType> {
        #[inline]
        fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
            f.write_str(self.1)
        }
    }

    impl<'l, ForType: ObjectType> Borrow<str> for ObjectPathRef<'l, ForType> {
        #[inline]
        fn borrow(&self) -> &str {
            self.1
        }
    }

    /// Avoid imposing requirements for PartialEq/Eq on ForType
    impl<'l, ForType> PartialEq for ObjectPathRef<'l, ForType> {
        #[inline]
        fn eq(&self, other: &Self) -> bool {
            self.1.eq(other.1)
        }
    }

    /// Avoid imposing requirements for PartialEq/Eq on ForType
    impl<'l, ForType> Eq for ObjectPathRef<'l, ForType> {}

    /// Avoid imposing requirements for PartialOrd/Ord on `ForType`
    impl<'l, ForType> PartialOrd for ObjectPathRef<'l, ForType> {
        #[inline]
        fn partial_cmp(&self, other: &Self) -> Option<std::cmp::Ordering> {
            Some(self.cmp(other))
        }
    }

    impl<'l, ForType> Ord for ObjectPathRef<'l, ForType> {
        #[inline]
        fn cmp(&self, other: &Self) -> std::cmp::Ordering {
            self.as_str().cmp(other.as_str())
        }
    }

    impl<'l, ForType> PartialEq<str> for ObjectPathRef<'l, ForType> {
        #[inline]
        fn eq(&self, other: &str) -> bool {
            self.1.eq(other)
        }
    }

    impl<'de: 'a, 'a, ForType: ObjectType> serde::de::Deserialize<'de> for ObjectPathRef<'a, ForType> {
        fn deserialize<D>(deserializer: D) -> Result<Self, D::Error>
        where
            D: serde::Deserializer<'de>,
        {
            struct Visitor<'a, ForType>(PhantomData<&'a *const ForType>);

            impl<'de: 'a, 'a, ForType: ObjectType> serde::de::Visitor<'de> for Visitor<'a, ForType> {
                type Value = ObjectPathRef<'a, ForType>;

                fn expecting(&self, formatter: &mut std::fmt::Formatter) -> std::fmt::Result {
                    formatter.write_str("a path using the '.'-separator with no empty segments")
                }

                // We can't do other types of string because this is a borrowed type.
                fn visit_borrowed_str<E>(self, v: &'de str) -> Result<Self::Value, E>
                where
                    E: serde::de::Error,
                {
                    ObjectPathRef::try_from_str(v)
                        .map_err(|e| E::invalid_value(serde::de::Unexpected::Str(v), &e))
                }
            }

            // Note that there is no special method for specifically hinting that we need borrowed
            // data. That is fine though - any deserializers that can do it will give the borrowed
            // data.
            deserializer.deserialize_str(Visitor(Default::default()))
        }
    }

    impl<'a, ForType: ObjectType> serde::ser::Serialize for ObjectPathRef<'a, ForType> {
        fn serialize<S>(&self, serializer: S) -> Result<S::Ok, S::Error>
        where
            S: serde::Serializer,
        {
            serializer.serialize_newtype_struct("ObjectPathRef", self.1)
        }
    }
}

/// An [`ObjectPathBuf`] that has had it's associated [`ObjectType`] erased into an internal
/// [`any::TypeId`]. Can only be created from valid object paths.
#[derive(Debug, Clone, PartialEq, Eq, PartialOrd, Ord, Hash)]
pub struct TypeErasedObjectPathBuf {
    object_type: any::TypeId,
    object_path: Box<str>,
}

impl TypeErasedObjectPathBuf {
    /// If this path is for the given object type.
    #[inline]
    pub fn is_for_object_type<ForType: ObjectType>(&self) -> bool {
        self.object_type == any::TypeId::of::<ForType>()
    }

    /// Get the type ID of the object type used for this path.
    #[inline]
    pub fn object_type_id(&self) -> any::TypeId {
        self.object_type
    }

    /// Unpack the path back into a non-type-erased version. If it fails (is the wrong type) just
    /// returns this value again.
    #[inline]
    pub fn with_object_type<ForType: ObjectType>(self) -> Result<ObjectPathBuf<ForType>, Self> {
        if self.is_for_object_type::<ForType>() {
            // SAFETY: type erased paths can only be created from valid paths in the first place,
            // so the string is always a valid path.
            Ok(unsafe { ObjectPathBuf::from_string_unchecked(self.object_path) })
        } else {
            Err(self)
        }
    }

    /// Derive metadata without any human or type name in it.
    ///
    /// If you have more information (like another metadata object) you should use that. This is a
    /// fallback
    #[inline]
    pub fn derive_fallback_meta(&self) -> ObjectTypeDynMeta {
        ObjectTypeDynMeta::type_id_fallback(self.object_type)
    }

    #[inline]
    pub fn as_objpath_ref(&self) -> TypeErasedObjectPathRef<'_> {
        TypeErasedObjectPathRef {
            object_type: self.object_type,
            object_path: self.object_path.as_ref(),
        }
    }

    #[inline]
    pub fn as_str(&self) -> &str {
        &self.object_path
    }
}

/// An [`ObjectPathRef`] that has had it's associated [`ObjectType`] erased into an internal
/// [`any::TypeId`]. Can only be created from valid object paths (or refs).  
#[derive(Debug, Copy, Clone, PartialEq, Eq, PartialOrd, Ord, Hash)]
pub struct TypeErasedObjectPathRef<'path> {
    object_type: any::TypeId,
    object_path: &'path str,
}

impl<'path> TypeErasedObjectPathRef<'path> {
    /// If this path is for the given object type.
    #[inline]
    pub fn is_for_object_type<ForType: ObjectType>(&self) -> bool {
        self.object_type == any::TypeId::of::<ForType>()
    }

    /// Get the type ID of the object type used for this path.
    #[inline]
    pub fn object_type_id(&self) -> any::TypeId {
        self.object_type
    }

    /// Unpack the path back into a non-type-erased version. If it fails (is the wrong type) just
    /// returns this value again.
    #[inline]
    pub fn with_object_type<ForType: ObjectType>(
        self,
    ) -> Result<ObjectPathRef<'path, ForType>, Self> {
        if self.is_for_object_type::<ForType>() {
            // SAFETY: type erased paths can only be created from valid paths in the first place,
            // so the string is always a valid path.
            Ok(unsafe { ObjectPathRef::from_str_unchecked(self.object_path) })
        } else {
            Err(self)
        }
    }

    #[inline]
    pub fn to_owned(&self) -> TypeErasedObjectPathBuf {
        TypeErasedObjectPathBuf {
            object_type: self.object_type,
            object_path: self.object_path.to_owned().into(),
        }
    }

    /// Derive metadata without any human or type name in it.
    ///
    /// If you have more information (like another metadata object) you should use that. This is a
    /// fallback
    #[inline]
    pub fn derive_fallback_meta(&self) -> ObjectTypeDynMeta {
        ObjectTypeDynMeta::type_id_fallback(self.object_type)
    }

    #[inline]
    pub fn as_str(&self) -> &'path str {
        self.object_path
    }
}

mod erased_objpath_impls {
    use std::{any, fmt::Display};

    use super::{ObjectPathBuf, ObjectPathRef, TypeErasedObjectPathBuf, TypeErasedObjectPathRef};
    use crate::objects::ObjectType;

    impl<ForType: ObjectType> From<ObjectPathBuf<ForType>> for TypeErasedObjectPathBuf {
        #[inline]
        fn from(value: ObjectPathBuf<ForType>) -> Self {
            value.erase()
        }
    }

    impl<ForType: ObjectType> PartialEq<ObjectPathBuf<ForType>> for TypeErasedObjectPathBuf {
        fn eq(&self, other: &ObjectPathBuf<ForType>) -> bool {
            any::TypeId::of::<ForType>() == self.object_type
                && self.object_path.as_ref() == other.as_str()
        }
    }

    impl<'path, ForType: ObjectType> From<ObjectPathRef<'path, ForType>>
        for TypeErasedObjectPathRef<'path>
    {
        #[inline]
        fn from(value: ObjectPathRef<'path, ForType>) -> Self {
            value.erase()
        }
    }

    impl<'path, ForType: ObjectType> PartialEq<ObjectPathRef<'path, ForType>>
        for TypeErasedObjectPathRef<'path>
    {
        fn eq(&self, other: &ObjectPathRef<'path, ForType>) -> bool {
            any::TypeId::of::<ForType>() == self.object_type && self.object_path == other.as_str()
        }
    }

    impl Display for TypeErasedObjectPathBuf {
        #[inline]
        fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
            f.write_str(self.object_path.as_ref())
        }
    }

    impl<'path> Display for TypeErasedObjectPathRef<'path> {
        #[inline]
        fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
            f.write_str(self.object_path)
        }
    }

    impl AsRef<str> for TypeErasedObjectPathBuf {
        #[inline]
        fn as_ref(&self) -> &str {
            &self.object_path
        }
    }

    impl<'p> AsRef<str> for TypeErasedObjectPathRef<'p> {
        #[inline]
        fn as_ref(&self) -> &str {
            self.object_path
        }
    }
}

/// Like [`std::borrow::Borrow`] but for object paths specifically (with allowances for the lifetime params and lack of some requirements.
pub trait PathBorrow {
    /// Path, borrowed into something like [`ObjectPathRef`].
    type BorrowedPath<'q>
    where
        Self: 'q;

    /// Borrow the path
    fn borrow_path(&self) -> Self::BorrowedPath<'_>;
}

pub mod path_borrowing_trait_impls {
    use super::{
        ObjectPathBuf, ObjectPathRef, PathBorrow, TypeErasedObjectPathBuf, TypeErasedObjectPathRef,
    };

    impl<'r, T: 'r + ?Sized + PathBorrow> PathBorrow for &'r T {
        type BorrowedPath<'q> = T::BorrowedPath<'q>
    where
        Self: 'q;

        #[inline]
        fn borrow_path(&self) -> Self::BorrowedPath<'_> {
            <T as PathBorrow>::borrow_path(self)
        }
    }

    impl<'r, T: 'r + ?Sized + PathBorrow> PathBorrow for &'r mut T {
        type BorrowedPath<'q> = T::BorrowedPath<'q>
    where
        Self: 'q;

        #[inline]
        fn borrow_path(&self) -> Self::BorrowedPath<'_> {
            <T as PathBorrow>::borrow_path(self)
        }
    }

    impl<ForType> PathBorrow for ObjectPathBuf<ForType> {
        type BorrowedPath<'q> = ObjectPathRef<'q, ForType> where Self: 'q;

        #[inline]
        fn borrow_path(&self) -> Self::BorrowedPath<'_> {
            self.as_ref()
        }
    }

    impl<'r, ForType> PathBorrow for ObjectPathRef<'r, ForType> {
        type BorrowedPath<'q> = ObjectPathRef<'q, ForType> where Self: 'q;

        #[inline]
        fn borrow_path(&self) -> Self::BorrowedPath<'_> {
            *self
        }
    }

    impl PathBorrow for TypeErasedObjectPathBuf {
        type BorrowedPath<'q> = TypeErasedObjectPathRef<'q>
        where
            Self: 'q;

        #[inline]
        fn borrow_path(&self) -> Self::BorrowedPath<'_> {
            self.as_objpath_ref()
        }
    }

    impl<'r> PathBorrow for TypeErasedObjectPathRef<'r> {
        type BorrowedPath<'q> = TypeErasedObjectPathRef<'q>
        where
            Self: 'q;

        #[inline]
        fn borrow_path(&self) -> Self::BorrowedPath<'_> {
            *self
        }
    }
}

/// Path capable of being erased to specifically a [`TypeErasedObjectPathRef`]. Implement for all
/// types that also implement [`ErasablePath`] with the erased type of [`TypeErasedObjectPathRef`]
///
/// This is a separate trait because expressing certain specific forms of trait bound using GATs is
/// not possible in very specific circumstances involving higher-rank trait bounds.
///
/// It is designed to be useful in combination with [`PathBorrow`]
pub trait SpecificErasablePathRef<'r> {
    /// Erase this given path type, with dynamic object metadata (if not directly
    /// providable, then you should derive an id-only metadata).
    fn erase_with_meta(self) -> (TypeErasedObjectPathRef<'r>, ObjectTypeDynMeta);
}

/// Path capable of being erased.
pub trait ErasablePath {
    type Erased;

    /// Erase this given path type, with dynamic object metadata (if not directly
    /// providable, then you should derive an id-only metadata).
    fn erase_with_meta(self) -> (Self::Erased, ObjectTypeDynMeta);
}

pub mod path_erasing_trait_impls {
    use super::{
        ErasablePath, ObjectPathBuf, ObjectPathRef, SpecificErasablePathRef,
        TypeErasedObjectPathBuf, TypeErasedObjectPathRef,
    };
    use crate::objects::ObjectType;

    impl<ForType: ObjectType> ErasablePath for ObjectPathBuf<ForType> {
        type Erased = TypeErasedObjectPathBuf;

        #[inline]
        fn erase_with_meta(self) -> (Self::Erased, crate::objects::ObjectTypeDynMeta) {
            self.erase_with_meta()
        }
    }

    impl<'q, ForType: ObjectType> ErasablePath for ObjectPathRef<'q, ForType> {
        type Erased = TypeErasedObjectPathRef<'q>;

        #[inline]
        fn erase_with_meta(self) -> (Self::Erased, crate::objects::ObjectTypeDynMeta) {
            ObjectPathRef::erase_with_meta(&self)
        }
    }

    impl<'q, ForType: ObjectType> SpecificErasablePathRef<'q> for ObjectPathRef<'q, ForType> {
        #[inline]
        fn erase_with_meta(
            self,
        ) -> (
            TypeErasedObjectPathRef<'q>,
            crate::objects::ObjectTypeDynMeta,
        ) {
            ObjectPathRef::erase_with_meta(&self)
        }
    }

    impl ErasablePath for TypeErasedObjectPathBuf {
        type Erased = Self;

        #[inline]
        fn erase_with_meta(self) -> (Self::Erased, crate::objects::ObjectTypeDynMeta) {
            let meta = self.derive_fallback_meta();
            (self, meta)
        }
    }

    impl<'q> ErasablePath for TypeErasedObjectPathRef<'q> {
        type Erased = Self;

        #[inline]
        fn erase_with_meta(self) -> (Self::Erased, crate::objects::ObjectTypeDynMeta) {
            (self, self.derive_fallback_meta())
        }
    }

    impl<'q> SpecificErasablePathRef<'q> for TypeErasedObjectPathRef<'q> {
        #[inline]
        fn erase_with_meta(self) -> (Self, crate::objects::ObjectTypeDynMeta) {
            (self, self.derive_fallback_meta())
        }
    }

    impl ErasablePath for (TypeErasedObjectPathBuf, crate::objects::ObjectTypeDynMeta) {
        type Erased = TypeErasedObjectPathBuf;

        #[inline]
        fn erase_with_meta(self) -> (Self::Erased, crate::objects::ObjectTypeDynMeta) {
            self
        }
    }

    impl<'q> ErasablePath
        for (
            TypeErasedObjectPathRef<'q>,
            crate::objects::ObjectTypeDynMeta,
        )
    {
        type Erased = TypeErasedObjectPathRef<'q>;

        #[inline]
        fn erase_with_meta(self) -> (Self::Erased, crate::objects::ObjectTypeDynMeta) {
            self
        }
    }

    impl<'q> SpecificErasablePathRef<'q>
        for (
            TypeErasedObjectPathRef<'q>,
            crate::objects::ObjectTypeDynMeta,
        )
    {
        #[inline]
        fn erase_with_meta(
            self,
        ) -> (
            TypeErasedObjectPathRef<'q>,
            crate::objects::ObjectTypeDynMeta,
        ) {
            self
        }
    }
}

#[cfg(test)]
mod objpath_tests {
    use chain_trans::Trans;

    use super::ObjectPathRef;
    use crate::resolve::test_utils::FakeConfigObject;

    #[test]
    fn splitting() {
        assert_eq!(
            ObjectPathRef::<FakeConfigObject>::try_from_str("aaa.bbb.ccc.ddddd")
                .unwrap()
                .split_shallowest_component(),
            (
                "aaa",
                Some("bbb.ccc.ddddd".trans(ObjectPathRef::try_from_str).unwrap())
            )
        );

        assert_eq!(
            ObjectPathRef::<FakeConfigObject>::try_from_str("aaaa")
                .unwrap()
                .split_shallowest_component(),
            ("aaaa", None)
        );

        assert_eq!(
            ObjectPathRef::<FakeConfigObject>::try_from_str("aaa.bbb.ccc.ddddd")
                .unwrap()
                .rsplit_deepest_component(),
            (
                Some("aaa.bbb.ccc".trans(ObjectPathRef::try_from_str).unwrap()),
                "ddddd"
            )
        );

        assert_eq!(
            ObjectPathRef::<FakeConfigObject>::try_from_str("aaa")
                .unwrap()
                .rsplit_deepest_component(),
            (None, "aaa")
        );
    }

    #[test]
    fn object_paths() {
        // Ensure that named object paths are sensible.
        let object_path =
            ObjectPathRef::<FakeConfigObject>::try_from_str("my.fake.object").unwrap();
        let cfg_relative_path = object_path.build_relative_fs_path();
        let collected_components: Vec<_> = cfg_relative_path
            .components()
            .map(|c| c.as_os_str())
            .collect();
        assert_eq!(
            collected_components,
            vec!["test", "fake", "my", "fake", "object.toml"]
        )
    }
}

pub mod iter {
    //! Module for object path iterators.

    use std::num::NonZeroUsize;

    use chain_trans::Trans;

    use super::ObjectPathRef;
    use crate::objects::ObjectType;

    /// Iterate over the components of an object path, traversing from the shallowest component
    /// (first one) to the deepest component (last one).
    #[derive(Debug, Clone, PartialEq, Eq)]
    pub struct ObjectPathComponentsDownward<'path, ForType: ObjectType> {
        remaining_children: Option<ObjectPathRef<'path, ForType>>,
    }

    impl<'path, ForType: ObjectType> From<ObjectPathRef<'path, ForType>>
        for ObjectPathComponentsDownward<'path, ForType>
    {
        #[inline]
        fn from(value: ObjectPathRef<'path, ForType>) -> Self {
            Self {
                remaining_children: Some(value),
            }
        }
    }

    impl<'path, ForType: ObjectType> Iterator for ObjectPathComponentsDownward<'path, ForType> {
        type Item = &'path str;

        fn next(&mut self) -> Option<Self::Item> {
            let remaining_children = self.remaining_children.take()?;
            let (next_child, remaining_children) = remaining_children.split_shallowest_component();
            self.remaining_children = remaining_children;
            Some(next_child)
        }

        fn size_hint(&self) -> (usize, Option<usize>) {
            self.remaining_children
                .as_ref()
                .map(ObjectPathRef::components_count)
                .map_or(0usize, NonZeroUsize::into)
                .trans(|remaining_children| (remaining_children, Some(remaining_children)))
        }
    }

    /// Iterate over the components of an object path, traversing from the deepest component to the
    /// shallowest component
    #[derive(Debug, Clone, PartialEq, Eq)]
    pub struct ObjectPathComponentsUpward<'path, ForType: ObjectType> {
        remaining_parents: Option<ObjectPathRef<'path, ForType>>,
    }

    impl<'path, ForType: ObjectType> From<ObjectPathRef<'path, ForType>>
        for ObjectPathComponentsUpward<'path, ForType>
    {
        #[inline]
        fn from(value: ObjectPathRef<'path, ForType>) -> Self {
            Self {
                remaining_parents: Some(value),
            }
        }
    }

    impl<'path, ForType: ObjectType> Iterator for ObjectPathComponentsUpward<'path, ForType> {
        type Item = &'path str;

        fn next(&mut self) -> Option<Self::Item> {
            let remaining_parents = self.remaining_parents.take()?;
            let (next_parent, remaining_parents) = remaining_parents.split_shallowest_component();
            self.remaining_parents = remaining_parents;
            Some(next_parent)
        }

        fn size_hint(&self) -> (usize, Option<usize>) {
            self.remaining_parents
                .as_ref()
                .map(ObjectPathRef::components_count)
                .map_or(0usize, NonZeroUsize::into)
                .trans(|remaining_parents| (remaining_parents, Some(remaining_parents)))
        }
    }
}

/// Module implementing slog logger useful things for object paths.
pub mod logger_impls {
    use super::{ObjectPathBuf, ObjectPathRef, TypeErasedObjectPathBuf, TypeErasedObjectPathRef};
    use crate::objects::{ObjectType, ObjectTypeDynMeta};

    /// Newtype struct for when logger traits can't be defined directly.
    #[derive(Debug, Clone)]
    pub struct ObjPathDisplay<T>(pub T);

    impl<ForType: ObjectType> slog::Value for ObjectPathBuf<ForType> {
        #[inline]
        fn serialize(
            &self,
            record: &slog::Record,
            key: slog::Key,
            serializer: &mut dyn slog::Serializer,
        ) -> slog::Result {
            let r = self.as_ref();
            r.serialize(record, key, serializer)
        }
    }

    impl<'a, ForType: ObjectType> slog::Value for ObjectPathRef<'a, ForType> {
        fn serialize(
            &self,
            _record: &slog::Record,
            key: slog::Key,
            serializer: &mut dyn slog::Serializer,
        ) -> slog::Result {
            let type_human_name = ForType::OBJECT_TYPE_HUMAN_NAME;
            let path = self.as_str();
            serializer.emit_arguments(key, &format_args!("{type_human_name}::{path}"))
        }
    }

    impl slog::Value for ObjPathDisplay<(TypeErasedObjectPathBuf, ObjectTypeDynMeta)> {
        #[inline]
        fn serialize(
            &self,
            record: &slog::Record,
            key: slog::Key,
            serializer: &mut dyn slog::Serializer,
        ) -> slog::Result {
            let v = ObjPathDisplay(&self.0);
            v.serialize(record, key, serializer)
        }
    }

    impl<'path> slog::Value for ObjPathDisplay<(TypeErasedObjectPathRef<'path>, ObjectTypeDynMeta)> {
        #[inline]
        fn serialize(
            &self,
            record: &slog::Record,
            key: slog::Key,
            serializer: &mut dyn slog::Serializer,
        ) -> slog::Result {
            let v = ObjPathDisplay(&self.0);
            v.serialize(record, key, serializer)
        }
    }

    impl<'a> slog::Value for ObjPathDisplay<&'a (TypeErasedObjectPathBuf, ObjectTypeDynMeta)> {
        fn serialize(
            &self,
            _record: &slog::Record,
            key: slog::Key,
            serializer: &mut dyn slog::Serializer,
        ) -> slog::Result {
            let dynmeta = self.0 .1.log_display_mode();
            let path = self.0 .0.as_str();
            serializer.emit_arguments(key, &format_args!("{dynmeta}::{path}"))
        }
    }

    impl<'a, 'path: 'a> slog::Value
        for ObjPathDisplay<&'a (TypeErasedObjectPathRef<'path>, ObjectTypeDynMeta)>
    {
        fn serialize(
            &self,
            _record: &slog::Record,
            key: slog::Key,
            serializer: &mut dyn slog::Serializer,
        ) -> slog::Result {
            let dynmeta = self.0 .1.log_display_mode();
            let path = self.0 .0.as_str();
            serializer.emit_arguments(key, &format_args!("{dynmeta}::{path}"))
        }
    }

    /// Trait for converting local object path types into slog-displayable ones.
    pub trait IntoLogDisplay {
        type Output: slog::Value;

        fn into_log_display(self) -> Self::Output;
    }

    impl<'path> IntoLogDisplay for (TypeErasedObjectPathRef<'path>, ObjectTypeDynMeta) {
        type Output = ObjPathDisplay<Self>;

        #[inline]
        fn into_log_display(self) -> Self::Output {
            ObjPathDisplay(self)
        }
    }

    impl IntoLogDisplay for (TypeErasedObjectPathBuf, ObjectTypeDynMeta) {
        type Output = ObjPathDisplay<Self>;

        #[inline]
        fn into_log_display(self) -> Self::Output {
            ObjPathDisplay(self)
        }
    }

    /// Implementation that extracts fallback id info
    impl<'path> IntoLogDisplay for TypeErasedObjectPathRef<'path> {
        type Output = <(Self, ObjectTypeDynMeta) as IntoLogDisplay>::Output;

        #[inline]
        fn into_log_display(self) -> Self::Output {
            let fallback_meta = self.derive_fallback_meta();
            (self, fallback_meta).into_log_display()
        }
    }

    /// Implementation that extracts fallback id info
    impl IntoLogDisplay for TypeErasedObjectPathBuf {
        type Output = <(Self, ObjectTypeDynMeta) as IntoLogDisplay>::Output;

        #[inline]
        fn into_log_display(self) -> Self::Output {
            let fallback_meta = self.derive_fallback_meta();
            (self, fallback_meta).into_log_display()
        }
    }

    impl<ForType: ObjectType> IntoLogDisplay for ObjectPathBuf<ForType> {
        type Output = Self;

        #[inline]
        fn into_log_display(self) -> Self::Output {
            self
        }
    }

    impl<'path, ForType: ObjectType> IntoLogDisplay for ObjectPathRef<'path, ForType> {
        type Output = Self;

        #[inline]
        fn into_log_display(self) -> Self::Output {
            self
        }
    }
}

// SDSB - Manage initramfs generators, kernels, and self-controlled secure boot.
// Copyright (C) 2023 Matti Bryce <mattibryce at protonmail dot com>
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <https://www.gnu.org/licenses/>.
