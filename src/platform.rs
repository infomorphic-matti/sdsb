//! Module for extracting data for specific platforms, like Arch Linux, Ubuntu, etc.
//!
//! This means things like the install location of linux kernels and extracting version information
//! from them, the paths for vmlinuz raw binaries, etc., or the locations of various bootloaders.
//!
//! For non-linux distributions different data may be needed. For now, we mostly just implement
//! linux information. These modules extract platform information from files on disk loaded via
//! XDG, but data is also embedded in the binary to generate this kind of information for
//! packagers automatically. If incomplete, adding your own files to the location is the solution.
//!
//! If implemented by this program, it's also possible to enable automatic extraction of locations
//! from various packages in your distribution's package manager.

use serde::{Deserialize, Serialize};

use crate::objects;

pub mod bootloader;
pub mod linux;

/// Platform data-type object.
#[derive(Debug, Deserialize, Serialize, Hash, Clone)]
#[serde(rename_all = "kebab-case")]
pub enum Platform {
    Linux(linux::Linux),
    Bootloader(bootloader::Bootloader),
}

impl objects::ObjectReferential for Platform {
    fn describe_referenced_paths<V: objects::internal_references::ReferencedObjectVisitor>(
        &self,
        path_visitor: &mut V,
    ) -> Result<(), V::Error> {
        match self {
            Platform::Linux(l) => l.describe_referenced_paths(path_visitor),
            Platform::Bootloader(b) => b.describe_referenced_paths(path_visitor),
        }
    }
}

impl objects::ObjectType for Platform {
    const XDG_CATEGORY: objects::XDGCategory = objects::XDGCategory::Data;

    const RELATIVE_STORAGE_PATH: &'static [&'static str] = &["platform"];

    const OBJECT_TYPE_HUMAN_NAME: &'static str = "platform";

    const OBJECT_PATH_METAVAR: &'static str = "PLATFORM_OBJECT_PATH";

    fn object_human_name(&self) -> &'static str {
        match self {
            Platform::Linux(_) => "linux",
            Platform::Bootloader(_) => "bootloader",
        }
    }

    #[inline]
    fn object_path_cli_help() -> bpaf::Doc {
        "Specify a platform-type object by object-path.".into()
    }
}

// SDSB - Manage initramfs generators, kernels, and self-controlled secure boot.
// Copyright (C) 2023 Matti Bryce <mattibryce at protonmail dot com>
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <https://www.gnu.org/licenses/>.
