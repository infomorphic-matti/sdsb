//! Specific structures, data, and types for linux-type platforms.

use serde::{Deserialize, Serialize};

use self::initramfs::InitRAMFSGen;
use crate::objects;

pub mod initramfs;

/// Represents a linux-type platform.
#[derive(Debug, Serialize, Deserialize, Hash, Clone)]
#[serde(rename_all = "kebab-case")]
pub struct Linux {
    /// If specified, provides the path to the specification of the "default" initramfs generator
    /// used for this linux distribution.
    default_initramfs_generator: Option<objects::path::ObjectPathBuf<InitRAMFSGen>>,
}

impl objects::ObjectReferential for Linux {
    fn describe_referenced_paths<V: objects::internal_references::ReferencedObjectVisitor>(
        &self,
        path_visitor: &mut V,
    ) -> Result<(), V::Error> {
        self.default_initramfs_generator
            .as_ref()
            .map(|p| path_visitor.visit_path(p.as_ref()))
            .unwrap_or(Ok(()))
    }
}

// SDSB - Manage initramfs generators, kernels, and self-controlled secure boot.
// Copyright (C) 2023 Matti Bryce <mattibryce at protonmail dot com>
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <https://www.gnu.org/licenses/>.
