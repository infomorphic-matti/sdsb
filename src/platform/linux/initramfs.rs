//! Module for specifying initramfs-generators.

use serde::{Deserialize, Serialize};

use crate::objects;

#[derive(Debug, Deserialize, Serialize, Clone, Hash)]
#[serde(rename = "initramfs-gen", rename_all = "kebab-case")]
pub struct InitRAMFSGen {}

impl objects::ObjectReferential for InitRAMFSGen {
    fn describe_referenced_paths<
        V: crate::objects::internal_references::ReferencedObjectVisitor,
    >(
        &self,
        _path_visitor: &mut V,
    ) -> Result<(), V::Error> {
        todo!()
    }
}

impl objects::ObjectType for InitRAMFSGen {
    const XDG_CATEGORY: objects::XDGCategory = objects::XDGCategory::Data;

    const RELATIVE_STORAGE_PATH: &'static [&'static str] =
        &["platform.specific", "linux", "initramfs-gen"];

    const OBJECT_TYPE_HUMAN_NAME: &'static str = "initramfs-generator";

    const OBJECT_PATH_METAVAR: &'static str = "INITRAMFS_GENERATOR_OBJECT_PATH";

    #[inline]
    fn object_path_cli_help() -> bpaf::Doc {
        "Specify an initramfs generator object by object-path.".into()
    }
}

// SDSB - Manage initramfs generators, kernels, and self-controlled secure boot.
// Copyright (C) 2023 Matti Bryce <mattibryce at protonmail dot com>
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <https://www.gnu.org/licenses/>.
