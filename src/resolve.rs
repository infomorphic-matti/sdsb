//! Module pertaining towards resolving config and data files from XDG.
//!
//! Provides traits and types for overrides and inheritance.

use core::{fmt::Display, iter::once, marker::PhantomData, slice};
use std::{borrow::Cow, hash::BuildHasher};

use chain_trans::Trans;
use slog::{slog_error, slog_o};

use self::db::DbTraversalError;
use crate::{
    logutils,
    objects::{
        self,
        internal_references::ReferencedObjectVisitorError,
        path::{
            self, logger_impls::IntoLogDisplay, ObjectPathRef, PathBorrow, SpecificErasablePathRef,
            TypeErasedObjectPathBuf,
        },
        ObjectReferential, ObjectType,
    },
};

pub mod db;
pub mod search_provider;
pub mod single_type_db;

/// Suffix for the config files this program uses, excluding the "." that connects it to the normal
/// file name.
pub const CONFIG_FILE_FORMAT_SUFFIX: &str = "toml";

/// Key inside the top-level table of a toml file that controls same-object override.
pub const OVERRIDE_INDICATOR_KEY: &str = "override";

/// Key inside the top-level table of toml files that controls inheritance from other objects. Acts
/// like an overridable property
pub const INHERIT_PATH_KEY: &str = "inherit";

/// Something capable of resolving paths of the given type into object references of the same.
/// These should not perform recursive checks on the referenced data
pub trait FlatResolver<ForType, Error> {
    /// Attempt to load the given path from the filesystem. A logger may or may not be provided.
    /// This should not attempt to recurse and load the paths referenced by the loaded object.
    fn resolve_and_load_unchecked(
        &mut self,
        obj_path: ObjectPathRef<'_, ForType>,
        logger: Option<slog::Logger>,
    ) -> Result<&mut ForType, Error>;

    /// Check if the given path is currently loaded in the db.
    fn is_loaded(&self, obj_path: ObjectPathRef<'_, ForType>) -> bool;
}

impl<ForType: ObjectType, SP: search_provider::SearchProvider, H: BuildHasher>
    FlatResolver<ForType, Box<single_type_db::ResolverDBFullError<SP>>>
    for single_type_db::ResolverDB<ForType, SP, H>
where
    SP::Error: 'static,
{
    #[inline]
    fn resolve_and_load_unchecked(
        &mut self,
        obj_path: ObjectPathRef<'_, ForType>,
        logger: Option<slog::Logger>,
    ) -> Result<&mut ForType, Box<single_type_db::ResolverDBFullError<SP>>> {
        self.resolve_and_load_unchecked(obj_path, logger)
    }

    fn is_loaded(&self, obj_path: ObjectPathRef<'_, ForType>) -> bool {
        self.resolve_if_loaded(obj_path).is_some()
    }
}

/// Provides a means to delegate [`FlatResolver`] resolution of given object types to a field or other expression
/// within a larger structure. Allows providing generics, and a seperate &mut vs & extractor of the
/// inner type to delegate to.
#[macro_export]
macro_rules! object_type_resolver_delegate {
    (impl $generics:tt FlatResolver for $supertype:ty where $where_clause:tt error $error_ty:ty {
        $(<$path_ty:ty> => $self_id:ident => $mut_self_extractor:expr => $self_extractor:expr),*
    }) => {
        $($crate::object_type_resolver_delegate! {@generic_impl
            $generics $path_ty | $supertype where $where_clause error $error_ty {
                #[inline]
                fn resolve_and_load_unchecked(&mut $self_id, obj_path: $crate::objects::path::ObjectPathRef<'_, $path_ty>, logger: Option<::slog::Logger>)
                    -> ::core::result::Result<&mut $path_ty, $error_ty> {
                    $crate::resolve::FlatResolver::<$path_ty, $error_ty>::resolve_and_load_unchecked($mut_self_extractor, obj_path, logger)
                }

                #[inline]
                fn is_loaded(&$self_id, obj_path: $crate::objects::path::ObjectPathRef<'_, $path_ty>) -> bool {
                    $crate::resolve::FlatResolver::<$path_ty, $error_ty>::is_loaded($self_extractor, obj_path)
                }
            }
        })*
    };
    (@generic_impl [$($generics:tt)*] $path_ty:ty | $supertype:ty where [$($where_clauses:tt)*] error $error_ty:ty { $($contents:tt)* } ) => {
        impl <$($generics)*> $crate::resolve::FlatResolver<$path_ty, $error_ty> for $supertype where $($where_clauses)* {
            $($contents)*
        }
    }
}

/// Trait for something that can be resolved in the given resolver in a "flat" manner (i.e. without
/// checking the internal references). Should be e.g. implemented on object paths, or multi-paths,
/// or similar.
pub trait FlatResolve<FlatResolver: ?Sized, Error> {
    /// Output of the resolver - lt is that of  &mut <FlatResolver>
    type Output<'rdb>
    where
        FlatResolver: 'rdb;

    /// Resolve and load the current path data into the given flat resolver and return output. This should not
    /// perform self-referential recursive resolving on the output.
    fn resolve_and_load_unchecked_in_db<'rdb>(
        &self,
        resolver_db: &'rdb mut FlatResolver,
        logger: Option<slog::Logger>,
    ) -> Result<Self::Output<'rdb>, Error>;

    /// Check if the current path is loaded into the db
    fn is_loaded_in(&self, resolver_db: &FlatResolver) -> bool;
}

impl<'q, ForType: ObjectType, Error, DB: ?Sized + FlatResolver<ForType, Error>>
    FlatResolve<DB, Error> for objects::path::ObjectPathRef<'q, ForType>
{
    type Output<'rdb> = &'rdb mut ForType where DB: 'rdb;

    #[inline]
    fn resolve_and_load_unchecked_in_db<'rdb>(
        &self,
        resolver_db: &'rdb mut DB,
        logger: Option<slog::Logger>,
    ) -> Result<Self::Output<'rdb>, Error> {
        resolver_db.resolve_and_load_unchecked(*self, logger)
    }

    #[inline]
    fn is_loaded_in(&self, resolver_db: &DB) -> bool {
        resolver_db.is_loaded(*self)
    }
}

impl<ForType: ObjectType, Error, DB: FlatResolver<ForType, Error>> FlatResolve<DB, Error>
    for objects::path::ObjectPathBuf<ForType>
{
    type Output<'rdb> = &'rdb mut ForType where DB: 'rdb;

    #[inline]
    fn resolve_and_load_unchecked_in_db<'rdb>(
        &self,
        resolver_db: &'rdb mut DB,
        logger: Option<slog::Logger>,
    ) -> Result<Self::Output<'rdb>, Error> {
        resolver_db.resolve_and_load_unchecked(self.as_ref(), logger)
    }

    #[inline]
    fn is_loaded_in(&self, resolver_db: &DB) -> bool {
        resolver_db.is_loaded(self.as_ref())
    }
}

/// Ref-Impl for FlatResolve
impl<'q, T, Error, DB> FlatResolve<DB, Error> for &'q T
where
    T: FlatResolve<DB, Error> + ?Sized + 'q,
{
    type Output<'rdb> = <T as FlatResolve<DB, Error>>::Output<'rdb>
    where
        DB: 'rdb;

    #[inline]
    fn resolve_and_load_unchecked_in_db<'rdb>(
        &self,
        resolver_db: &'rdb mut DB,
        logger: Option<slog::Logger>,
    ) -> Result<Self::Output<'rdb>, Error> {
        <T as FlatResolve<DB, Error>>::resolve_and_load_unchecked_in_db(*self, resolver_db, logger)
    }

    #[inline]
    fn is_loaded_in(&self, resolver_db: &DB) -> bool {
        <T as FlatResolve<DB, Error>>::is_loaded_in(*self, resolver_db)
    }
}

/// Mut-Impl for FlatResolve
impl<'q, T, Error, DB> FlatResolve<DB, Error> for &'q mut T
where
    T: FlatResolve<DB, Error> + ?Sized + 'q,
{
    type Output<'rdb> = <T as FlatResolve<DB, Error>>::Output<'rdb>
    where
        DB: 'rdb;

    #[inline]
    fn resolve_and_load_unchecked_in_db<'rdb>(
        &self,
        resolver_db: &'rdb mut DB,
        logger: Option<slog::Logger>,
    ) -> Result<Self::Output<'rdb>, Error> {
        <T as FlatResolve<DB, Error>>::resolve_and_load_unchecked_in_db(*self, resolver_db, logger)
    }

    #[inline]
    fn is_loaded_in(&self, resolver_db: &DB) -> bool {
        <T as FlatResolve<DB, Error>>::is_loaded_in(*self, resolver_db)
    }
}

/// Resolve the given base paths as well as any contained references to other paths, into the given
/// database
///
/// Will log errors.
pub fn resolve_and_load_recursive<DB: ?Sized, Resolvable: ToOwned, Error>(
    db: &mut DB,
    base_paths: &[Resolvable],
    base_logger: Option<slog::Logger>,
) -> Result<(), Error>
where
    Error: ReferencedObjectVisitorError + DbTraversalError + Display,
    Resolvable: FlatResolve<DB, Error> + PathBorrow,
    //for<'q> <Resolvable as PathBorrow>::BorrowedPath<'q>: ErasablePath<Erased = TypeErasedObjectPathRef<'q>>,
    for<'q> <Resolvable as PathBorrow>::BorrowedPath<'q>: SpecificErasablePathRef<'q>,
    for<'q> <Resolvable as FlatResolve<DB, Error>>::Output<'q>: ObjectReferential,
    <Resolvable as ToOwned>::Owned:
        std::convert::TryFrom<path::TypeErasedObjectPathBuf, Error = path::TypeErasedObjectPathBuf>,
{
    /// Layer of the stack.
    struct StackLayer<'r, PendingPathData: ToOwned> {
        /// Pending paths for this layer of the stack (pending before the next layer up is done).
        pub pending_resolves: Vec<Cow<'r, PendingPathData>>,
        /// The currently-being-resolved path data for this layer of the stack. If there are other
        /// layers deeper down, they should be resolved fully before moving past the currently
        /// resolving layer.
        pub currently_resolving: Cow<'r, PendingPathData>,
    }

    /*
    #[derive(Debug, Clone, Copy, PartialEq, Eq, PartialOrd, Ord, Hash)]
    enum CurrentlyResolvingStatus {
        /// The given action on the currently-resolving path of the given layer fully resolved it.
        FinishedResolving,
        /// The given action did not finish resolving the currently-resolving path
        IncompleteResolving,
    }
    */

    impl<'r, PendingPathData: ToOwned> StackLayer<'r, PendingPathData> {
        /// Create the root of the stack.
        pub fn root(root_object_paths: &'r [PendingPathData]) -> Option<Self> {
            let (current_resolve, pending_resolves) = root_object_paths.split_last()?;
            Some(Self {
                pending_resolves: pending_resolves.iter().map(Cow::Borrowed).collect(),
                currently_resolving: current_resolve.trans(Cow::Borrowed),
            })
        }

        /// Create a new layer of next- and currently- resolving paths
        pub fn new_layer_raw(
            mut layer_object_paths: Vec<Cow<'r, PendingPathData>>,
        ) -> Option<Self> {
            // Get the next and currently resolving paths. Note that the order *does not matter*,
            // as such, we can pop off the end to pick whichever next one to resolve. If the paths are empty,
            // then naturally there is no more path to resolve and None should be returned.
            let current_resolve = layer_object_paths.pop()?;
            Some(Self {
                pending_resolves: layer_object_paths,
                currently_resolving: current_resolve,
            })
        }

        /// Replace the currently-resolving pathdata with the next one that needs to be resolved.
        ///
        /// If the stack layer is then empty, this returns None
        pub fn move_to_next_to_resolve(self) -> Option<Self> {
            let StackLayer {
                mut pending_resolves,
                currently_resolving: _,
            } = self;
            let maybe_new_current_resolve = pending_resolves.pop();
            maybe_new_current_resolve.map(|new_current_resolve| Self {
                pending_resolves,
                currently_resolving: new_current_resolve,
            })
        }

        /*
         * Note: this function would be much more useful if we implemented this loading process
         * using recursion instead of a stack + loop (which is more janky but easier to manage
         * w.r.t. modification of the object path db).
        /// Attempt to perform actions with the currently-resolving path. The function should
        /// return an indicator as to whether the currently resolving path is finished resolving
        /// and should be removed from the stack layer. This then modifies the current layer to
        /// move to the next pending path data (if possible, if not returns none)
        pub fn with_currently_resolving<T>(
            self,
            with_currently_resolving: impl FnOnce(&PendingPathData) -> (CurrentlyResolvingStatus, T),
        ) -> (Option<Self>, T) {
            let (status, data) = with_currently_resolving(self.currently_resolving.as_ref());
            match status {
                CurrentlyResolvingStatus::IncompleteResolving => (Some(self), data),
                CurrentlyResolvingStatus::FinishedResolving => self
                    .move_to_next_to_resolve()
                    .trans(|stack_layer| (stack_layer, data)),
            }
        }
        */

        /// Remove paths from the current stack layer until the given criterion returns false for
        /// the first time. If all paths are eliminated, then this stack layer is considered
        /// "complete"/"done".
        pub fn pop_paths_matching_criterion(
            mut self,
            mut should_pop: impl FnMut(&PendingPathData) -> bool,
        ) -> Option<Self> {
            while should_pop(&self.currently_resolving) {
                self = self.move_to_next_to_resolve()?;
            }
            Some(self)
        }

        /// Consume all stack layers to modify-and-return a full error with appropriate stack data
        /// attached. Error passed in should have no existing stack data and should not include
        /// whatever "current" stack layer contains the object path that caused the error.
        pub fn provide_stack_data_to_error<E: ReferencedObjectVisitorError>(
            object_stack: Vec<Self>,
            mut error: E,
        ) -> E
        where
            PendingPathData: PathBorrow,
            for<'q> <PendingPathData as PathBorrow>::BorrowedPath<'q>: SpecificErasablePathRef<'q>,
        {
            // Construct the stack failure.
            for next_deepest_stack_layer in object_stack {
                let (erased_path, object_type_meta) = next_deepest_stack_layer
                    .currently_resolving
                    .as_ref()
                    .borrow_path()
                    .erase_with_meta();
                error.record_current_object_failure_dynamic(&erased_path, Some(object_type_meta));
            }
            error
        }
    }

    let mut object_stack = vec![{
        match StackLayer::root(base_paths) {
            Some(v) => v,
            // No paths have been indicated as needing to be loaded.
            None => return Ok(()),
        }
    }];

    // This is the current logger, updated to use the current root object
    let mut logger = base_logger.clone().unwrap_or_else(logutils::null_logger);

    let final_result = 'stack_processor: loop {
        let Some(deepest_stack_element) = object_stack.pop() else {
            // This is finished now.
            return Ok(());
        };

        // Clear out all already-loaded paths
        // If empty then all paths in this layer are properly loaded and we should move to the next
        // layer up.
        let stack_element_with_unloaded_resolving_path = match deepest_stack_element
            .pop_paths_matching_criterion(|path: &Resolvable| path.is_loaded_in(db))
        {
            Some(v) => v,
            None => continue 'stack_processor,
        };

        // LOGGING: to make logging effective, we log with an associated path of the current root
        // object. This means that we update the logger each time we detect that we're processing a
        // root-level path.
        //
        // We put this after the "stack element with unloaded resolving path" bit, because
        // otherwise this would lag behind the current root path as the fully loaded previous path
        // would not be removed.
        //
        // This is why the check is for ==0 rather than ==1, because the root layer is currently
        // removed.

        if object_stack.is_empty() {
            let current_root_path = stack_element_with_unloaded_resolving_path
                .currently_resolving
                .borrow_path();
            let root_path_log_display = current_root_path
                .erase_with_meta()
                .trans(|(a, b)| (a.to_owned(), b))
                .into_log_display();
            logger = base_logger
                .clone()
                .map_or_else(logutils::null_logger, |base_logger| {
                    base_logger.new(slog_o! {
                        "loading-root-object-path" => root_path_log_display
                    })
                });
        }

        // load the path. If there is an error, then we build up a stack of error sources.
        //
        // This loads the object and then schedules any sub-paths that need loading onto the stack
        // one layer deeper. Only once all unloaded subpaths have been processed, is the next
        // unloaded path processed once again.
        //
        // Cycles are no problem with this mechanism, because once an object is considered loaded,
        // then it doesn't need to be traversed by any subsequent references to the path of that
        // object.
        match stack_element_with_unloaded_resolving_path
            .currently_resolving
            .as_ref()
            .resolve_and_load_unchecked_in_db(db, Some(logger.clone()))
        {
            Ok(loaded_object) => {
                /// Visitor that stores the paths referenced by an object as it provides them.
                ///
                /// Note that if errors occur, they will be associated with the inner path that was
                /// referenced. As such, it's a necessary step to, when applying object stack
                /// information, to add the current object back to the stack as this visitor's
                /// errors embed only the path referenced *by* that object inside their own errors.
                struct PathVisitor<'r, PendingPathData: ToOwned, Error> {
                    pub referenced_paths: Vec<Cow<'r, PendingPathData>>,
                    pub _v: PhantomData<Error>,
                }

                impl<'r, PendingPathData: ToOwned, Error> PathVisitor<'r, PendingPathData, Error> {
                    pub fn new() -> Self {
                        Self {
                            referenced_paths: Vec::default(),
                            _v: PhantomData,
                        }
                    }

                    /// Make the next, deeper stack layer containing all paths referenced by the
                    /// object.
                    fn make_next_stack_layer(self) -> Option<StackLayer<'r, PendingPathData>> {
                        StackLayer::new_layer_raw(self.referenced_paths)
                    }
                }

                impl<'r, PendingPathData: ToOwned, Error> Default for PathVisitor<'r, PendingPathData, Error> {
                    fn default() -> Self {
                        Self::new()
                    }
                }

                impl<
                        'r,
                        PendingPathData: ToOwned,
                        Error: DbTraversalError + ReferencedObjectVisitorError,
                    > objects::internal_references::ReferencedObjectVisitor
                    for PathVisitor<'r, PendingPathData, Error>
                where
                    <PendingPathData as ToOwned>::Owned: TryFrom<
                        objects::path::TypeErasedObjectPathBuf,
                        Error = objects::path::TypeErasedObjectPathBuf,
                    >,
                {
                    type Error = Error;

                    fn visit_path<ForType: ObjectType>(
                        &mut self,
                        object_path: ObjectPathRef<'_, ForType>,
                    ) -> Result<(), Self::Error> {
                        let (erased_owned, meta) = object_path.to_owned().erase_with_meta();
                        let unerased =
                            <<PendingPathData as ToOwned>::Owned as TryFrom<_>>::try_from(
                                erased_owned,
                            )
                            .map_err(|erased_path| {
                                DbTraversalError::unknown_path_type_dynamic(
                                    erased_path.as_objpath_ref(),
                                    Some(meta),
                                )
                            })?;
                        self.referenced_paths.push(Cow::Owned(unerased));
                        Ok(())
                    }
                }

                let mut pending = PathVisitor::<'_, Resolvable, Error>::default();
                match loaded_object.describe_referenced_paths(&mut pending) {
                    Ok(()) => {
                        // We unconditionally add the object stack layer back - if it's completely
                        // resolved due to lack of referenced paths, then the object will be peeled
                        // off the current stack layer for whatever the next one is (if any).
                        let current_object_and_siblings =
                            stack_element_with_unloaded_resolving_path;
                        let maybe_next_layer = pending.make_next_stack_layer();
                        let reappends = once(current_object_and_siblings).chain(maybe_next_layer);
                        object_stack.extend(reappends);
                    }
                    // Note: we don't use map_err because this process captures and destroys the
                    // object stack.
                    Err(error) => {
                        object_stack.push(stack_element_with_unloaded_resolving_path);
                        break 'stack_processor Err(StackLayer::provide_stack_data_to_error(
                            object_stack,
                            error,
                        ));
                    }
                }
            }
            Err(deepest_failure) => {
                let deepest_failure =
                    StackLayer::provide_stack_data_to_error(object_stack, deepest_failure);
                break 'stack_processor Err(deepest_failure);
            }
        }
    };
    match final_result {
        Ok(v) => Ok(v),
        Err(e) => {
            slog_error!(logger, "error resolving object recursively"; "error" => %e);
            Err(e)
        }
    }
}

// Sized is required for when the resolvable path type because it gets converted
pub trait Resolve<DB: ?Sized, Error>:
    FlatResolve<DB, Error> + PathBorrow + ToOwned + Sized
where
    Error: ReferencedObjectVisitorError + DbTraversalError + Display,
    for<'q> <Self as PathBorrow>::BorrowedPath<'q>: SpecificErasablePathRef<'q>,
    for<'q> Self::Output<'q>: ObjectReferential,
    <Self as ToOwned>::Owned: TryFrom<TypeErasedObjectPathBuf, Error = TypeErasedObjectPathBuf>,
{
    fn resolve_and_load_single_in<'rdb>(
        &self,
        resolver_db: &'rdb mut DB,
        logger: Option<slog::Logger>,
    ) -> Result<Self::Output<'rdb>, Error> {
        resolve_and_load_recursive(resolver_db, slice::from_ref(self), logger.clone())?;
        self.resolve_and_load_unchecked_in_db(resolver_db, logger)
    }
}

#[cfg(test)]
pub mod test_utils {
    use crate::objects::{self, ObjectReferential, ObjectType, XDGCategory};

    /// A mock/fake config object that implements [`super::ObjectType`] for testing.
    ///
    /// It's [`ObjectType::XDG_CATEGORY`] is [`XDGCategory::Config`], and it's
    /// [`ObjectType::RELATIVE_STORAGE_PATH`] is `test/fake`
    #[derive(
        Debug,
        Default,
        Clone,
        Copy,
        PartialEq,
        Eq,
        PartialOrd,
        Ord,
        Hash,
        serde::Serialize,
        serde::Deserialize,
    )]
    pub struct FakeConfigObject;

    impl ObjectReferential for FakeConfigObject {
        #[inline]
        fn describe_referenced_paths<V: objects::internal_references::ReferencedObjectVisitor>(
            &self,
            _path_visitor: &mut V,
        ) -> Result<(), V::Error> {
            Ok(())
        }
    }

    impl ObjectType for FakeConfigObject {
        const XDG_CATEGORY: XDGCategory = XDGCategory::Config;

        const RELATIVE_STORAGE_PATH: &'static [&'static str] = &["test", "fake"];

        const OBJECT_TYPE_HUMAN_NAME: &'static str = "fake-test-object";

        const OBJECT_PATH_METAVAR: &'static str = "FAKE_TEST_OBJECT_PATH";

        #[inline]
        fn object_path_cli_help() -> bpaf::Doc {
            "This is a fake object type with fake object paths. This message should not appear to you if you are an end user.".into()
        }
    }
}

// SDSB - Manage initramfs generators, kernels, and self-controlled secure boot.
// Copyright (C) 2023 Matti Bryce <mattibryce at protonmail dot com>
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <https://www.gnu.org/licenses/>.
