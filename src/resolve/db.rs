//! Module for the various databases and sub-databases used by this program.

use std::collections::hash_map;

use crate::objects::{self, path};

/// Error that has some capabilities that let it be used as an error when traversing multi-type
/// databases.
pub trait DbTraversalError {
    /// Create some form of error indicating that no database exists that can handle the given path
    /// type. By default, delegates to [`DbTraversalError::unknown_path_type_dynamic`]
    #[inline]
    fn unknown_path_type<ForType: objects::ObjectType>(
        path: path::ObjectPathRef<'_, ForType>,
    ) -> Self
    where
        Self: Sized,
    {
        let (erased, meta) = path.erase_with_meta();
        Self::unknown_path_type_dynamic(erased, Some(meta))
    }

    fn unknown_path_type_dynamic(
        path: path::TypeErasedObjectPathRef<'_>,
        metadata: Option<objects::ObjectTypeDynMeta>,
    ) -> Self;
}

impl<T: DbTraversalError> DbTraversalError for Box<T> {
    #[inline]
    fn unknown_path_type<ForType: objects::ObjectType>(
        path: path::ObjectPathRef<'_, ForType>,
    ) -> Self
    where
        Self: Sized,
    {
        Box::new(T::unknown_path_type(path))
    }

    #[inline]
    fn unknown_path_type_dynamic(
        path: path::TypeErasedObjectPathRef<'_>,
        metadata: Option<objects::ObjectTypeDynMeta>,
    ) -> Self {
        Box::new(T::unknown_path_type_dynamic(path, metadata))
    }
}

/// Build implementation methods recursively for a database type containing other database
/// types.
#[macro_export]
macro_rules! build_db_constructor_tree {
    ($name:ident { $($field_mode:ident $field:ident: $field_type:ty),* }) => {
        impl <SP: ::core::marker::Copy, H: ::core::default::Default> $name<SP, H> {
            pub fn with_search_provider(search_provider: SP) -> Self {
                Self {
                    $($field: $crate::build_db_constructor_tree!{
                        @field_value_default $field_mode $field_type; H; search_provider
                    }),*
                }
            }
        }

        impl <SP: ::core::marker::Copy, H: ::core::clone::Clone> $name<SP, H> {
            pub fn with_search_provider_and_hasher(search_provider: SP, hasher: H) -> Self {
                Self {
                    $($field: $crate::build_db_constructor_tree! {
                        @field_value_clone $field_mode $field_type; hasher; search_provider
                    }),*
                }
            }
        }
    };
    // The "base" initializer mode (bare ResolverDB) with the given mode
    (@field_value_default leaf $field_type:ty; $hasher:ty; $search_provider:expr) => {{
        <$field_type>::with_search_provider_and_hasher(
            $search_provider,
            <$hasher as ::core::default::Default>::default()
        )
    }};
    (@field_value_default node $field_type:ty; $hasher:ty; $search_provider:expr) => {{
        <$field_type>::with_search_provider($search_provider)
    }};
    (@field_value_clone leaf $field_type:ty; $hasher:expr; $search_provider:expr) => {{
        <$field_type>::with_search_provider_and_hasher(
            $search_provider,
            $hasher.clone()
        )
    }};
    (@field_value_clone node $field_type:ty; $hasher:expr; $search_provider:expr) => {{
        <$field_type>::with_search_provider_and_hasher(
            $search_provider,
            $hasher.clone()
        )
    }}
}

// TODO: Build a full hierarchical db structure macro - avoid duplication of the various delegation
// info
pub mod data {
    use core::hash::BuildHasher;
    use std::collections::hash_map;

    use crate::{
        object_type_resolver_delegate, platform,
        resolve::{
            search_provider,
            single_type_db::{ResolverDB, ResolverDBFullError},
        },
    };

    pub struct LinuxDB<SP, H = hash_map::RandomState> {
        pub initramfs_db: ResolverDB<platform::linux::initramfs::InitRAMFSGen, SP, H>,
    }

    build_db_constructor_tree! {LinuxDB {leaf initramfs_db: ResolverDB::<_, _, _>}}

    object_type_resolver_delegate! {
        impl [SP: search_provider::SearchProvider, H: BuildHasher] FlatResolver for LinuxDB<SP, H>
            where [SP::Error: 'static] error Box<ResolverDBFullError<SP>>
        {
            <platform::linux::initramfs::InitRAMFSGen> => self => &mut self.initramfs_db => &self.initramfs_db
        }
    }

    pub struct PlatformDB<SP, H = hash_map::RandomState> {
        pub platforms: ResolverDB<platform::Platform, SP, H>,
        pub linux_specific: LinuxDB<SP, H>,
    }

    build_db_constructor_tree! {PlatformDB {
        leaf platforms: ResolverDB<_, _, _>,
        node linux_specific: LinuxDB<_, _>
    }}

    object_type_resolver_delegate! {
        impl [SP: search_provider::SearchProvider, H: BuildHasher] FlatResolver for PlatformDB<SP, H>
            where [SP::Error: 'static] error Box<ResolverDBFullError<SP>>
        {
            <platform::linux::initramfs::InitRAMFSGen> => self => &mut self.linux_specific => &self.linux_specific,
            <platform::Platform> => self => &mut self.platforms => &self.platforms
        }
    }

    pub struct DataDB<SP, H = hash_map::RandomState> {
        pub platform_db: PlatformDB<SP, H>,
    }

    build_db_constructor_tree! {DataDB {
        node platform_db: PlatformDB<_, _>
    }}

    object_type_resolver_delegate! {
        impl [SP: search_provider::SearchProvider, H: BuildHasher] FlatResolver for DataDB<SP, H>
            where [SP::Error: 'static] error Box<ResolverDBFullError<SP>>
        {
            <platform::linux::initramfs::InitRAMFSGen> => self => &mut self.platform_db => &self.platform_db,
            <platform::Platform> => self => &mut self.platform_db => &self.platform_db
        }
    }
}

pub mod config {
    pub struct ConfigDB;
}

pub struct FullDB<SP, H = hash_map::RandomState> {
    pub data: data::DataDB<SP, H>,
    pub config: config::ConfigDB,
}

// SDSB - Manage initramfs generators, kernels, and self-controlled secure boot.
// Copyright (C) 2023 Matti Bryce <mattibryce at protonmail dot com>
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <https://www.gnu.org/licenses/>.
