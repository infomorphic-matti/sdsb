//! Module abstracting the discovery of paths inside the `xdg` structures. Used for testing as well
//! as maybe in future allowing CLI overrides of config and data definitions.

use std::{fmt::Debug, io, path::PathBuf, str::FromStr};

use chain_trans::Trans;
use thiserror::Error;
use xdg::BaseDirectories;

use crate::objects::{path::ObjectPathRef, ObjectType, XDGCategory};

/// The entry(s) found for a named path that has been searched for in specific config structure.
///
/// Both are optional - an entry of a given name may have a file, a directory, both, or neither (if
/// it was not found at the given level).
#[derive(Debug, Clone)]
pub struct PerCfgDataEntry {
    pub directory: Option<PathBuf>,
    pub file: Option<toml::Table>,
}

/// Provides search through "config" and "data" for raw config files (auto-parsed as TOML and
/// returned as a collection of [`toml::Table`]), as well as for directories of the same name.
///
/// Acts as a proxy for xdg, essentially, but more modular.
pub trait SearchProvider {
    type Error: Debug + std::error::Error;

    /// Find the relevant file and/or directory entries in priority order for the given object type
    /// and path (lowest priority to highest).
    fn find_entries<ForType: ObjectType>(
        &self,
        object_path: ObjectPathRef<'_, ForType>,
    ) -> Result<Vec<PerCfgDataEntry>, Self::Error>;

    /// Add another search provider that "overrides" this one, i.e. is "higher" in the resolution
    /// chain
    #[inline]
    fn override_this_with<H: SearchProvider<Error = Self::Error>>(
        self,
        higher_priority: H,
    ) -> OverridingSearchProvider<Self, H>
    where
        Self: Sized,
    {
        OverridingSearchProvider {
            lower_priority: self,
            higher_priority,
        }
    }
}

impl<'a, T: ?Sized + SearchProvider> SearchProvider for &'a T {
    type Error = T::Error;

    #[inline]
    fn find_entries<ForType: ObjectType>(
        &self,
        object_path: ObjectPathRef<'_, ForType>,
    ) -> Result<Vec<PerCfgDataEntry>, Self::Error> {
        T::find_entries(*self, object_path)
    }
}

impl<'a, T: ?Sized + SearchProvider> SearchProvider for &'a mut T {
    type Error = T::Error;

    #[inline]
    fn find_entries<ForType: ObjectType>(
        &self,
        object_path: ObjectPathRef<'_, ForType>,
    ) -> Result<Vec<PerCfgDataEntry>, Self::Error> {
        T::find_entries(&**self, object_path)
    }
}

/// Simple flattening implementation
impl<T: SearchProvider> SearchProvider for Option<T> {
    type Error = T::Error;

    #[inline]
    fn find_entries<ForType: ObjectType>(
        &self,
        object_path: ObjectPathRef<'_, ForType>,
    ) -> Result<Vec<PerCfgDataEntry>, Self::Error> {
        self.as_ref()
            .map(move |s| s.find_entries(object_path))
            .unwrap_or_else(|| Ok(Vec::new()))
    }
}

/// Search provider that is constructed over an iterator of other search providers. Works as long
/// as the iterator can be [`Clone::clone`]'d, because that lets you scan repeatedly.
#[derive(Debug, Clone)]
pub struct IteratingSearchProvider<SPIterator> {
    pub iter: SPIterator,
}

impl<SPIterator> From<SPIterator> for IteratingSearchProvider<SPIterator> {
    #[inline]
    fn from(value: SPIterator) -> Self {
        Self { iter: value }
    }
}

impl<SPIterator: Iterator + Clone> SearchProvider for IteratingSearchProvider<SPIterator>
where
    SPIterator::Item: SearchProvider,
{
    type Error = <SPIterator::Item as SearchProvider>::Error;

    fn find_entries<ForType: ObjectType>(
        &self,
        object_path: ObjectPathRef<'_, ForType>,
    ) -> Result<Vec<PerCfgDataEntry>, Self::Error> {
        self.iter
            .clone()
            .map(|ephemeral_provider| ephemeral_provider.find_entries(object_path.clone()))
            .try_fold(Vec::new(), |all_paths, maybe_next_found_configs| {
                match maybe_next_found_configs {
                    Ok(next_found_cfgs) => Ok(all_paths.trans_mut(|v| v.extend(next_found_cfgs))),
                    Err(e) => Err(e),
                }
            })
    }
}

/// Iterator extension trait to ease the creation of [`IteratingSearchProvider`]
pub trait IntoIteratorExt: IntoIterator {
    /// Convert this iterator of search providers into one that treats the later search providers
    /// as higher priority specifications. See [`IteratingSearchProvider`]
    #[inline]
    fn into_overriding_search_provider(self) -> IteratingSearchProvider<Self::IntoIter>
    where
        Self: Sized,
        Self::IntoIter: Sized,
    {
        IteratingSearchProvider::from(self.into_iter())
    }

    /// Convert this iterator of search providers into one that treats the later search providers
    /// as lower-priority specifications. See [`IteratingSearchProvider`] and
    /// [`core::iter::DoubleEndedIterator`]. This is only usable if the iterator provided is
    /// double-ended.
    #[inline]
    fn into_reversed_overriding_search_provider(
        self,
    ) -> IteratingSearchProvider<core::iter::Rev<Self::IntoIter>>
    where
        Self: Sized,
        Self::IntoIter: Sized + core::iter::DoubleEndedIterator,
    {
        IteratingSearchProvider::from(self.into_iter().rev())
    }
}

impl<T: IntoIterator> IntoIteratorExt for T {}

#[derive(Debug, Clone)]
pub struct OverridingSearchProvider<LowerPriority, HigherPriority> {
    pub lower_priority: LowerPriority,
    pub higher_priority: HigherPriority,
}

impl<LowerPriority: SearchProvider, HigherPriority> SearchProvider
    for OverridingSearchProvider<LowerPriority, HigherPriority>
where
    HigherPriority: SearchProvider<Error = LowerPriority::Error>,
{
    type Error = LowerPriority::Error;

    #[inline]
    fn find_entries<ForType: ObjectType>(
        &self,
        object_path: ObjectPathRef<'_, ForType>,
    ) -> Result<Vec<PerCfgDataEntry>, Self::Error> {
        let mut lower_priority = self.lower_priority.find_entries(object_path.clone())?;
        let higher_priority = self.higher_priority.find_entries(object_path)?;
        lower_priority.extend(higher_priority);
        Ok(lower_priority)
    }
}

/// Error type when searching for relevant toml files and directories using the XDG provider or any
/// other directory-hierarchy provider.
#[derive(Debug, Error)]
pub enum DirectorySearcherError {
    #[error("Could not read TOML file at path {1} - error: {0}")]
    CouldntReadTomlFile(#[source] io::Error, PathBuf),
    #[error("Could not parse TOML file at path {1} - error: {0}")]
    CouldntParseTomlFile(#[source] toml::de::Error, PathBuf),
    #[error("Directory location at {0} was not a directory (may be a file, or broken symlink, or other thing)")]
    DirLocationWasNotDir(PathBuf),
    #[error(
        "Directory location at {1} could not be checked for it's type or existence - error: {0}"
    )]
    DirLocationWasUncheckable(#[source] io::Error, PathBuf),
}

/// Trait to ease the building of [`SearchProvider`]s when you just want to load from directories
/// easily.  
pub trait DirectoryHierarchy {
    /// Get the directories to search in for data-type entries, from *highest* priority to *lowest*
    /// priority (the inverse of most things in this program).
    fn get_data_dirs(&self) -> Vec<PathBuf>;

    /// Get the directories to search in for config-type entries, from *highest* priority to *lowest*
    /// priority (the inverse of most things in this program).
    fn get_config_dirs(&self) -> Vec<PathBuf>;
}

/// Provides a common utility interface for searching through config and data hierarchies when they
/// are directories on-disk
#[derive(Default, Debug, Clone, PartialEq, Eq)]
pub struct DirectoryHierarchySearcher<H: DirectoryHierarchy>(pub H);

impl<H: DirectoryHierarchy> DirectoryHierarchySearcher<H> {
    #[inline]
    pub fn new(h: H) -> Self {
        h.into()
    }
}

impl<H: DirectoryHierarchy> From<H> for DirectoryHierarchySearcher<H> {
    #[inline]
    fn from(value: H) -> Self {
        Self(value)
    }
}

impl<H: DirectoryHierarchy> SearchProvider for DirectoryHierarchySearcher<H> {
    type Error = DirectorySearcherError;

    fn find_entries<ForType: ObjectType>(
        &self,
        object_path: ObjectPathRef<'_, ForType>,
    ) -> Result<Vec<PerCfgDataEntry>, Self::Error> {
        // This includes the .toml extension.
        let relative_file_path = object_path.build_relative_fs_path();

        // The search path is in preferred- to less-preferred order, whereas we want to go the
        // other way. We use `rev()` to do this.
        let search_paths = match ForType::XDG_CATEGORY {
            XDGCategory::Data => self.0.get_data_dirs(),
            XDGCategory::Config => self.0.get_config_dirs(),
        };

        // rev() ensures that we go in the correct order (least- to most- prioritised)
        search_paths
            .iter()
            .rev()
            .map(|search_path_entry| search_path_entry.join(&relative_file_path))
            .map(
                |mut full_path| -> Result<PerCfgDataEntry, DirectorySearcherError> {
                    // Try to obtain the toml data and parse it
                    let toml_data: Option<toml::Table> =
                        match std::fs::read_to_string(&full_path) {
                            Ok(s) => toml::Table::from_str(&s).map(Some).map_err(
                                |toml_parsing_error| {
                                    DirectorySearcherError::CouldntParseTomlFile(
                                        toml_parsing_error,
                                        full_path.clone(),
                                    )
                                },
                            )?,
                            Err(e) => match e.kind() {
                                std::io::ErrorKind::NotFound => None,
                                _ => {
                                    return Err(DirectorySearcherError::CouldntReadTomlFile(
                                        e, full_path,
                                    ))
                                }
                            },
                        };

                    // remove the .toml extension from the file path and do the directory checks
                    full_path.set_extension("");

                    let directory_path = match std::fs::metadata(&full_path) {
                        Ok(metadata) => {
                            if metadata.is_dir() {
                                Some(full_path)
                            } else {
                                return Err(DirectorySearcherError::DirLocationWasNotDir(
                                    full_path,
                                ));
                            }
                        }
                        Err(e) => match e.kind() {
                            io::ErrorKind::NotFound => None,
                            _ => {
                                return Err(DirectorySearcherError::DirLocationWasUncheckable(
                                    e, full_path,
                                ))
                            }
                        },
                    };

                    Ok(PerCfgDataEntry {
                        directory: directory_path,
                        file: toml_data,
                    })
                },
            )
            .collect::<Result<Vec<_>, DirectorySearcherError>>()
    }
}

impl DirectoryHierarchy for BaseDirectories {
    #[inline]
    fn get_data_dirs(&self) -> Vec<PathBuf> {
        self.get_data_dirs()
    }

    #[inline]
    fn get_config_dirs(&self) -> Vec<PathBuf> {
        self.get_config_dirs()
    }
}

pub type XdgSearchProvider = DirectoryHierarchySearcher<BaseDirectories>;

/// Data subdir for local directory hierarchy specifiers
pub const LOCAL_DIRECTORY_DATA_SUFFIX: &str = "data";

/// Config subdir for local directory hierarchy specifiers
pub const LOCAL_DIRECTORY_CONFIG_SUFFIX: &str = "config";

/// Information on a local directory that should be searched in, with [`LOCAL_DIRECTORY_CONFIG_SUFFIX`] being the folder
/// inside for config files and [`LOCAL_DIRECTORY_DATA_SUFFIX`] being the folder inside for data files.
#[derive(Debug, Clone, PartialEq, Eq)]
pub struct LocalDirectory {
    pub local_base_directory: PathBuf,
}

impl DirectoryHierarchy for LocalDirectory {
    #[inline]
    fn get_data_dirs(&self) -> Vec<PathBuf> {
        vec![self
            .local_base_directory
            .clone()
            .trans_mut(|p| p.push(LOCAL_DIRECTORY_DATA_SUFFIX))]
    }

    #[inline]
    fn get_config_dirs(&self) -> Vec<PathBuf> {
        vec![self
            .local_base_directory
            .clone()
            .trans_mut(|p| p.push(LOCAL_DIRECTORY_CONFIG_SUFFIX))]
    }
}

impl From<PathBuf> for LocalDirectory {
    #[inline]
    fn from(value: PathBuf) -> Self {
        Self {
            local_base_directory: value,
        }
    }
}

pub type SingleLocalSearchProvider = DirectoryHierarchySearcher<LocalDirectory>;

/// Directory just for config files.
#[derive(Debug, Clone, PartialEq, Eq)]
pub struct ConfigDirectory {
    pub config_directory: PathBuf,
}

impl DirectoryHierarchy for ConfigDirectory {
    #[inline]
    fn get_data_dirs(&self) -> Vec<PathBuf> {
        vec![]
    }

    #[inline]
    fn get_config_dirs(&self) -> Vec<PathBuf> {
        vec![self.config_directory.clone()]
    }
}

impl From<PathBuf> for ConfigDirectory {
    #[inline]
    fn from(value: PathBuf) -> Self {
        Self {
            config_directory: value,
        }
    }
}

pub type SingleConfigSearchProvider = DirectoryHierarchySearcher<ConfigDirectory>;

/// Directory just for data files.
#[derive(Debug, Clone, PartialEq, Eq)]
pub struct DataDirectory {
    pub data_directory: PathBuf,
}

impl DirectoryHierarchy for DataDirectory {
    #[inline]
    fn get_data_dirs(&self) -> Vec<PathBuf> {
        vec![self.data_directory.clone()]
    }

    #[inline]
    fn get_config_dirs(&self) -> Vec<PathBuf> {
        vec![]
    }
}

impl From<PathBuf> for DataDirectory {
    #[inline]
    fn from(value: PathBuf) -> Self {
        Self {
            data_directory: value,
        }
    }
}

pub type SingleDataSearchProvider = DirectoryHierarchySearcher<DataDirectory>;

// SDSB - Manage initramfs generators, kernels, and self-controlled secure boot.
// Copyright (C) 2023 Matti Bryce <mattibryce at protonmail dot com>
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <https://www.gnu.org/licenses/>.
