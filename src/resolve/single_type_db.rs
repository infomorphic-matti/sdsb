//! Module with the implementation of single-type resolving database structures as well as many
//! error types.

use core::convert::Infallible;
use std::{
    self,
    collections::{hash_map::RandomState, HashMap, HashSet},
    fmt::Display,
    hash::BuildHasher,
};

use chain_trans::Trans;
use serde::Deserialize;
use slog::{self, slog_debug, slog_error, slog_info, slog_o, slog_warn};
use smallvec::{self, SmallVec};
use thiserror::Error;

use super::{
    db::{self, DbTraversalError},
    search_provider, INHERIT_PATH_KEY, OVERRIDE_INDICATOR_KEY,
};
use crate::{
    logutils::null_logger,
    objects,
    objects::{
        path::{
            logger_impls::IntoLogDisplay, ObjectPathBuf, ObjectPathRef, TypeErasedObjectPathBuf,
            TypeErasedObjectPathRef,
        },
        ObjectType, ObjectTypeDynMeta,
    },
    structured_delta,
    utils::{
        toml_parse_utils,
        toml_parse_utils::{
            build_override_set_and_extract_property, EntryModifier, OverridingAction,
        },
    },
};

/// Database for the given object type capable of resolving from an arbitrary resolver.
#[derive(Debug)]
pub struct ResolverDB<ForType, SearchProvider, S = RandomState> {
    /// Provider for resolving value requests when they aren't already loaded.
    pub(crate) search_provider: SearchProvider,
    /// All loaded data.     
    ///
    /// NOTE: In future we might switch the key out for an Arc (as well as the entries inside
    /// config files) - this would enable much better avoidance of large numbers of copies.
    pub(crate) defined_names: HashMap<objects::path::ObjectPathBuf<ForType>, ForType, S>,
}

/// Error that can come from trying to access an object without attempting an FS load.
///
/// These errors always have dynamic meta context associated.
#[derive(Debug, Clone, Error)]
pub enum ResolverDBCacheAccessError {
    #[error("Object at {0} (type: {1}) not loaded or nonexistent in your configuration")]
    NonLoadedObject(TypeErasedObjectPathBuf, ObjectTypeDynMeta),
}

impl ResolverDBCacheAccessError {
    /// Indicate that the object was not found in the configuration.
    #[inline]
    pub fn object_not_found<ForType: ObjectType>(object_path: ObjectPathBuf<ForType>) -> Self {
        let (p, m) = object_path.erase_with_meta();
        Self::NonLoadedObject(p, m)
    }

    /// Get the object name that failed to load and the dynamic metadata
    #[inline]
    pub fn relevant_object(&self) -> (TypeErasedObjectPathRef<'_>, ObjectTypeDynMeta) {
        match self {
            ResolverDBCacheAccessError::NonLoadedObject(path, meta) => {
                (path.as_objpath_ref(), *meta)
            }
        }
    }
}

/// Error when parsing the object tree for overrides and inheritance calculations, or with e.g.
/// locating another object.
#[derive(Debug, Error)]
pub enum OverrideConstructionError {
    #[error("Invalid 'override' key value type - should be boolean, was {invalid_toml_type}")]
    OverrideKeyInvalidType { invalid_toml_type: &'static str },
    #[error(transparent)]
    InheritancePathParseFailure(#[from] toml::de::Error),
}

impl OverrideConstructionError {
    /// Invalid type for an override key.
    #[inline]
    pub fn invalid_override_key_type(invalid_value: &toml::Value) -> Self {
        Self::OverrideKeyInvalidType {
            invalid_toml_type: invalid_value.type_str(),
        }
    }

    /// Error when parsing inheritance path
    #[inline]
    pub fn inheritance_path_parsing_failure(err: toml::de::Error) -> Self {
        Self::InheritancePathParseFailure(err)
    }
}

/// Error when traversing a collection of different object dbs or loading from a search provider.
#[derive(Debug, Error)]
pub enum ResolverDBRefObjectTraversalError<SP: search_provider::SearchProvider>
where
    SP::Error: 'static,
{
    #[error("No resolver database provided for object type {0} - this is a programmer error")]
    NoDatabaseForObjectType(ObjectTypeDynMeta),
    #[error(transparent)]
    ProviderError(<SP as search_provider::SearchProvider>::Error),
    #[error("Inheritance cycle found while loading object")]
    InheritanceCycle,
    /// Error parsing overrides and inheritance
    #[error(transparent)]
    OverrideConstruction(#[from] Box<OverrideConstructionError>),
    #[error(transparent)]
    ObjectParsingError(#[from] structured_delta::DeserializerError),
}

impl<SP: search_provider::SearchProvider> ResolverDBRefObjectTraversalError<SP> {
    /// Indicate that the search provider failed to provide a result for a path.
    #[inline]
    pub fn search_provider_failed(sp_err: SP::Error) -> Self {
        Self::ProviderError(sp_err)
    }

    /// Indicate that an inheritance cycle was found while loading an object
    #[inline]
    pub fn inheritance_cycle() -> Self {
        Self::InheritanceCycle
    }

    /// Indicate that no database was available for the given object type
    #[inline]
    pub fn no_database_for_object_type(obj_type_metadata: ObjectTypeDynMeta) -> Self {
        Self::NoDatabaseForObjectType(obj_type_metadata)
    }
}

impl<SP: search_provider::SearchProvider> db::DbTraversalError
    for ResolverDBRefObjectTraversalError<SP>
{
    #[inline]
    fn unknown_path_type_dynamic(
        path: objects::path::TypeErasedObjectPathRef<'_>,
        metadata: Option<objects::ObjectTypeDynMeta>,
    ) -> Self {
        Self::no_database_for_object_type(metadata.unwrap_or(path.derive_fallback_meta()))
    }
}

#[derive(Debug, Error)]
pub enum ResolverDBFullError<SP: search_provider::SearchProvider>
where
    SP::Error: 'static,
{
    ErrorResolvingObject {
        /// The actual error during accessing directories or doing other things.
        #[source]
        deepest_loading_error: ResolverDBRefObjectTraversalError<SP>,
        /// the object path we tried to resolve
        object_path: (TypeErasedObjectPathBuf, ObjectTypeDynMeta),
        /// The stack of failed object loads as a consequence of this (from shallowest to deepest,
        /// does not include the object path - the closest to the actual failed object is the last
        /// in the vector) - only applies when doing deep recursive loading.
        parent_stack: Vec<(TypeErasedObjectPathBuf, ObjectTypeDynMeta)>,
    },
    NonExistentObject {
        /// The deepest internal "non existant object" error.
        #[source]
        deepest_access_error: ResolverDBCacheAccessError,
        /// The stack of failed object loads that depended on this. Note that this is only used in
        /// the case of recursive validation resolving. The *last* value that is on this is the
        /// deepest in the stack of failures (the closest to the actual object that failed is the
        /// last element).
        ///
        /// This also embeds the type name if available on call - there is no inbuilt way to go
        /// from type ID to type name, so...
        parent_stack: Vec<(TypeErasedObjectPathBuf, ObjectTypeDynMeta)>,
    },
}

impl<SP: search_provider::SearchProvider> ResolverDBFullError<SP> {
    /// Indicate that there was an error resolving the given object type from the search provider.
    pub fn search_provider_loading_error<ForType: ObjectType>(
        object_path: ObjectPathBuf<ForType>,
        error: SP::Error,
    ) -> Self {
        Self::ErrorResolvingObject {
            deepest_loading_error: ResolverDBRefObjectTraversalError::search_provider_failed(error),
            object_path: object_path.erase_with_meta(),
            parent_stack: vec![],
        }
    }

    /// Indicate the presence of an inheritence cycle
    pub fn inheritance_cycle<ForType: ObjectType>(path: ObjectPathBuf<ForType>) -> Self {
        Self::ErrorResolvingObject {
            deepest_loading_error: ResolverDBRefObjectTraversalError::inheritance_cycle(),
            object_path: path.erase_with_meta(),
            parent_stack: vec![],
        }
    }

    /// Indicate an error constructing override or inheritance chains
    pub fn override_construction_failure<ForType: ObjectType>(
        path: ObjectPathBuf<ForType>,
        error: OverrideConstructionError,
    ) -> Self {
        Self::ErrorResolvingObject {
            deepest_loading_error: Box::new(error).into(),
            object_path: path.erase_with_meta(),
            parent_stack: vec![],
        }
    }

    /// Indicate that the object doesn't exist within the current configuration after an attempt to
    /// load it
    pub fn object_not_found<ForType: ObjectType>(path: ObjectPathBuf<ForType>) -> Self {
        Self::NonExistentObject {
            deepest_access_error: ResolverDBCacheAccessError::object_not_found(path),
            parent_stack: vec![],
        }
    }

    /// Error when parsing the object from a toml override set.
    pub fn object_parsing_error<ForType: ObjectType>(
        path: ObjectPathBuf<ForType>,
        deser_error: structured_delta::DeserializerError,
    ) -> Self {
        Self::ErrorResolvingObject {
            deepest_loading_error: deser_error.into(),
            object_path: path.erase_with_meta(),
            parent_stack: vec![],
        }
    }

    /// Error indicating there is no database for the given (static) path type when using
    /// multi-db resolution.
    pub fn no_database_for_type<ForType: ObjectType>(path: ObjectPathBuf<ForType>) -> Self {
        Self::ErrorResolvingObject {
            deepest_loading_error: DbTraversalError::unknown_path_type(path.as_ref()),
            object_path: path.erase_with_meta(),
            parent_stack: vec![],
        }
    }

    /// Error indicating there is no database for the given (dynamic) path type when using multi-db
    /// resolution. Prefer [ResolverDBFullError::no_database_for_type]
    pub fn no_database_for_type_dynamic(
        path: TypeErasedObjectPathBuf,
        metadata: Option<ObjectTypeDynMeta>,
    ) -> Self {
        let metadata = metadata.unwrap_or_else(|| path.derive_fallback_meta());
        Self::ErrorResolvingObject {
            deepest_loading_error: DbTraversalError::unknown_path_type_dynamic(
                path.as_objpath_ref(),
                Some(metadata),
            ),
            object_path: (path, metadata),
            parent_stack: vec![],
        }
    }
}

impl<SP: search_provider::SearchProvider> Display for ResolverDBFullError<SP>
where
    SP::Error: 'static,
{
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        match self {
            ResolverDBFullError::ErrorResolvingObject {
                deepest_loading_error,
                object_path: (object_path, object_metadata),
                parent_stack,
            } => {
                f.write_fmt(format_args!("Error reading object {object_path} (of type: {object_metadata}) - \n{deepest_loading_error}"))?;
                for (raw_object_name, raw_object_metadata) in
                    parent_stack.as_slice().into_iter().rev()
                {
                    f.write_fmt(format_args!(
                        "\nRequired by: {raw_object_name} (of type: {raw_object_metadata})"
                    ))?;
                }
                Ok(())
            }
            ResolverDBFullError::NonExistentObject {
                deepest_access_error,
                parent_stack,
            } => {
                let (deepest_path, deepest_path_meta) = deepest_access_error.relevant_object();
                f.write_fmt(format_args!(
                    "Object {deepest_path} (of type: {deepest_path_meta}) does not exist."
                ))?;
                for (raw_object_name, raw_object_meta) in parent_stack.as_slice().into_iter().rev()
                {
                    f.write_fmt(format_args!(
                        "\nRequired by: {raw_object_name} of type {raw_object_meta}"
                    ))?;
                }
                Ok(())
            }
        }
    }
}

impl<SP: search_provider::SearchProvider> objects::internal_references::ReferencedObjectVisitorError
    for ResolverDBFullError<SP>
where
    SP::Error: 'static,
{
    fn record_current_object_failure_dynamic(
        &mut self,
        current_object_path: &objects::path::TypeErasedObjectPathRef<'_>,
        metadata: Option<ObjectTypeDynMeta>,
    ) {
        let metadata = metadata.unwrap_or_else(|| current_object_path.derive_fallback_meta());
        match self {
            ResolverDBFullError::ErrorResolvingObject {
                deepest_loading_error: _,
                object_path: _,
                parent_stack,
            } => {
                parent_stack.push((current_object_path.to_owned(), metadata));
            }
            ResolverDBFullError::NonExistentObject {
                deepest_access_error: _,
                parent_stack,
            } => parent_stack.push((current_object_path.to_owned(), metadata)),
        };
    }
}

impl<SP: search_provider::SearchProvider> db::DbTraversalError for ResolverDBFullError<SP> {
    #[inline]
    fn unknown_path_type_dynamic(
        path: objects::path::TypeErasedObjectPathRef<'_>,
        metadata: Option<objects::ObjectTypeDynMeta>,
    ) -> Self {
        Self::no_database_for_type_dynamic(path.to_owned(), metadata)
    }

    #[inline]
    fn unknown_path_type<ForType: objects::ObjectType>(
        path: objects::path::ObjectPathRef<'_, ForType>,
    ) -> Self
    where
        Self: Sized,
    {
        Self::no_database_for_type(path.to_owned())
    }
}

impl<ForType, SP, S> ResolverDB<ForType, SP, S> {
    #[inline]
    pub fn with_search_provider_and_hasher_and_capacity(
        search_provider: SP,
        hash_builder: S,
        capacity: usize,
    ) -> Self {
        Self {
            search_provider,
            defined_names: HashMap::with_capacity_and_hasher(capacity, hash_builder),
        }
    }

    #[inline]
    pub fn with_search_provider_and_hasher(search_provider: SP, hash_builder: S) -> Self {
        Self {
            search_provider,
            defined_names: HashMap::with_hasher(hash_builder),
        }
    }
}

impl<ForType, SP, H: Default> ResolverDB<ForType, SP, H> {
    #[inline]
    pub fn with_search_provider(search_provider: SP) -> Self {
        Self {
            search_provider,
            defined_names: HashMap::<_, _, H>::default(),
        }
    }

    #[inline]
    pub fn with_search_provider_and_capacity(search_provider: SP, capacity: usize) -> Self {
        Self {
            search_provider,
            defined_names: HashMap::with_capacity_and_hasher(capacity, Default::default()),
        }
    }
}

impl<ForType, SP, S: BuildHasher> ResolverDB<ForType, SP, S> {
    /// If the given entry is loaded, then retrieve it. This is efficient if it is not loaded, and
    /// returns an Option.
    #[inline]
    pub fn resolve_if_loaded(
        &self,
        object_path: ObjectPathRef<'_, ForType>,
    ) -> Option<&'_ ForType> {
        self.defined_names.get(object_path.as_str())
    }

    /// See if we have an object at the given location already loaded. Note - this is not efficient
    /// in the case where we don't have it (it copies the object path to produce error messages). This
    /// system is designed for preloading to occur using the standard resolve mechanism before you
    /// use this :)
    pub fn resolve_without_loading<'s>(
        &'s self,
        object_path: ObjectPathRef<'_, ForType>,
    ) -> Result<&'s ForType, ResolverDBCacheAccessError>
    where
        ForType: ObjectType,
    {
        self.resolve_if_loaded(object_path).ok_or_else(|| {
            ResolverDBCacheAccessError::NonLoadedObject(
                object_path.to_owned().erase(),
                ObjectTypeDynMeta::from_type::<ForType>(),
            )
        })
    }

    /// This attempts to actually load the raw toml files from the search provider. It performs
    /// override and inheritance calculations, and produces a complete toml override set or an
    /// appropriate error in override and inheritance resolution.
    ///
    /// Used inside [`ResolverDB::resolve_and_load_unchecked`] when an object path is not already
    /// loaded in the database, before parsing is applied.
    pub(crate) fn load_toml_inheritance_hierarchy_for_path(
        &self,
        object_path: ObjectPathBuf<ForType>,
        logger: slog::Logger,
    ) -> Result<structured_delta::OverrideTable, Box<ResolverDBFullError<SP>>>
    where
        SP: search_provider::SearchProvider,
        ForType: ObjectType,
    {
        slog_info!(logger, "Loading object");

        // Objects that have already been inherited from. Used to prevent cycles.
        let mut inherited_from = HashSet::new();
        let mut maybe_next_inheritance = Some(object_path.to_owned());
        // finalized overrides, but with the lowest/base-est tables at the end.
        let mut reverse_priority_finalized_overrides: SmallVec<[_; 20]> = smallvec::smallvec![];
        // Perform validation of the override key's value. `is_base_table` indicates whether or not
        // the override key value is being tested in the base table (i.e. a warning should be
        // printed - the return value is still correct but override is useless in the base
        // table so...)
        let take_and_validate_override_key = |is_base_table: bool,
                                              table: &mut toml::Table|
         -> Result<bool, _> {
            match table.remove(OVERRIDE_INDICATOR_KEY) {
                None | Some(toml::Value::Boolean(false)) => Ok(false),
                Some(toml::Value::Boolean(true)) => {
                    if is_base_table {
                        slog_warn!(logger, "override key is set to true in a base toml file");
                    }
                    Ok(true)
                }
                Some(toml::Value::String(s))
                    if s.as_str() == crate::structured_delta::constants::DELETE_OPTION =>
                {
                    Ok(false)
                }
                Some(v) => {
                    // If the key is not valid, then we error
                    let err = OverrideConstructionError::invalid_override_key_type(&v);
                    slog_error!(logger, "could not parse override chain - invalid override key type"; "error" => %err);
                    Err(err)
                }
            }
        };

        // Remove and perform validation of the `inherit` keys's value, if present, including
        // managing "del" values.
        let take_and_validate_inherit_key = |table: &mut toml::Table| -> Result<
            toml_parse_utils::EntryModifier<ObjectPathBuf<ForType>>,
            _,
        > {
            match table.remove(INHERIT_PATH_KEY) {
                None => Ok(toml_parse_utils::EntryModifier::NoChange),
                Some(toml::Value::String(s))
                    if s.as_str() == crate::structured_delta::constants::DELETE_OPTION =>
                {
                    Ok(toml_parse_utils::EntryModifier::Delete)
                }
                Some(v) => ObjectPathBuf::deserialize(v)
                    .map(toml_parse_utils::EntryModifier::NewData)
                    .map_err(OverrideConstructionError::inheritance_path_parsing_failure),
            }
        };

        while let Some(next_inheritance) = maybe_next_inheritance.take() {
            slog_debug!(logger, "Inheriting..."; "inheriting-from-path" => next_inheritance.as_ref().into_log_display());
            if inherited_from.contains(next_inheritance.as_str()) {
                return Err(
                    ResolverDBFullError::inheritance_cycle(next_inheritance).trans(Box::new)
                );
            }

            let full_components = self
                .search_provider
                .find_entries(next_inheritance.as_ref())
                .map_err(|e| {
                    ResolverDBFullError::search_provider_loading_error(next_inheritance.clone(), e)
                })?;

            let toml_components = full_components.into_iter().flat_map(|v| v.file);

            let maybe_override_set = build_override_set_and_extract_property::<_, _, _, _, Vec<_>>(
                toml_components,
                |base_table| {
                    let _override_key = take_and_validate_override_key(true, base_table)?;
                    take_and_validate_inherit_key(base_table).map(EntryModifier::reduce_in_base)
                },
                |override_table| {
                    let is_override = match take_and_validate_override_key(false, override_table) {
                        Ok(v) => v,
                        Err(e) => return OverridingAction::Fail(e),
                    };
                    let inheritance_specifier = match take_and_validate_inherit_key(override_table)
                    {
                        Ok(v) => v,
                        Err(e) => return OverridingAction::Fail(e),
                    };
                    if is_override {
                        OverridingAction::Override {
                            modifier: inheritance_specifier,
                        }
                    } else {
                        OverridingAction::Rebase {
                            new_value: inheritance_specifier.reduce_in_base(),
                        }
                    }
                },
                |existing, delta| -> Result<_, Infallible> { Ok(delta.simple_apply(existing)) },
            );

            let override_set = match maybe_override_set {
                Ok(Some(v)) => v,
                Ok(None) => {
                    return Err(
                        ResolverDBFullError::object_not_found(next_inheritance).trans(Box::new)
                    )
                }
                Err(e) => match e {
                    crate::utils::toml_parse_utils::OverrideSetBuilderError::Extraction(e) => {
                        return Err(ResolverDBFullError::override_construction_failure(
                            next_inheritance,
                            e,
                        )
                        .trans(Box::new))
                    }
                    crate::utils::toml_parse_utils::OverrideSetBuilderError::Application(
                        infallible,
                    ) => match infallible {},
                },
            };

            let toml_parse_utils::OverrideSetOutput {
                base_table,
                overrides,
                value: maybe_inherit_from_path,
            } = override_set;

            // Inheritance requires pushing the inherited stuff to the bottom of the override
            // stack, so we do that. We will extract a base table at the end.
            let reverse_priority_iterator = overrides
                .into_iter()
                .rev()
                .chain(core::iter::once(base_table));
            reverse_priority_finalized_overrides.extend(reverse_priority_iterator);
            // Also add the current path to the set of inherited-from values, to prevent cycles.
            inherited_from.insert(next_inheritance);
            // This means that whatever the latest thing inherited from, will then be recursed
            // into, if it actually *does* inherit from something.
            maybe_next_inheritance = maybe_inherit_from_path
        }
        let mut table_priority_order_iter = reverse_priority_finalized_overrides.into_iter().rev();
        let base_table = table_priority_order_iter.next().expect("If the object was found, then this should never be empty. If it was not found, then we should have returned early :/");
        let override_tables: SmallVec<_> = table_priority_order_iter.map(Some).collect();

        Ok(structured_delta::OverrideTable {
            base_table,
            overrides: override_tables,
        })
    }

    /// Perform loading on the given object path without any checking of sub-objects. Returns a
    /// reference to the object if loaded, or a pretty layered error type if it could not be loaded
    /// (or just didn't exist!)
    ///
    /// This has to do things like apply deltas and inheritance. It gets... pretty gnarly
    pub fn resolve_and_load_unchecked(
        &mut self,
        object_path: ObjectPathRef<'_, ForType>,
        logger: Option<slog::Logger>,
    ) -> Result<&mut ForType, Box<ResolverDBFullError<SP>>>
    where
        ForType: ObjectType,
        SP: search_provider::SearchProvider,
    {
        // Note that if we want to actually return the reference, we have to do it later. `matching` on the optional will not
        // work, because rust thinks that even in the `None` case the HashMap is still borrowed :/
        // Instead, check for `is_some` and then go.
        //
        // If we already have it, then just return it.
        match self.defined_names.get_mut(object_path.as_str()).is_some() {
            true => {
                return self
                    .defined_names
                    .get_mut(object_path.as_str())
                    .expect("just checked")
                    .trans(Ok)
            }
            false => {}
        };

        let logger = logger
            .unwrap_or_else(null_logger)
            .new(slog_o! {"loading-object-path" => object_path.to_owned().into_log_display()});

        slog_debug!(logger, "Loading object from search-provider");
        // Now we want to extract the final override hierarchy
        let final_override_chain =
            self.load_toml_inheritance_hierarchy_for_path(object_path.to_owned(), logger.clone())?;

        slog_debug!(logger, "Loaded inheritance files from disk, parsing");
        let resulting_object = ForType::deserialize(
            final_override_chain.trans(Into::<structured_delta::OverrideValue>::into),
        )
        .map_err(|e| {
            ResolverDBFullError::object_parsing_error(object_path.to_owned(), e).trans_inspect(
                |e| {
                    slog_error!(logger, "Error parsing object"; "error" => %e);
                },
            )
        })?;

        match self
            .defined_names
            .insert(object_path.to_owned(), resulting_object)
        {
            None => {}
            Some(_old_value) => {
                unreachable!("We already checked for an existing value right at the start")
            }
        };

        Ok(self
            .defined_names
            .get_mut(object_path.as_str())
            .expect("Just inserted"))
    }
}

// SDSB - Manage initramfs generators, kernels, and self-controlled secure boot.
// Copyright (C) 2023 Matti Bryce <mattibryce at protonmail dot com>
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <https://www.gnu.org/licenses/>.
