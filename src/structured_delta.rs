//! Mini-library for specifying structured deltas for configs. Primarily built for toml stuff.
//! HOW DOES THIS WORK:
//! * This library needs information on the structure of the config being constructed.
//! * The primary way to do this... implement a deserializer that hooks into the implementation for
//! [`toml::value::Value`] and [`tom::Table`], and allows providing multiple values in priority
//! override order, while intervening to calculate deltas. It naturally consumes everything like
//! the normal implementations do.

pub mod enumeration;
pub mod macros;
pub mod seq;
pub mod simpletoml;
pub mod structure;

#[cfg(feature = "backtrace")]
use std::backtrace::Backtrace;

use chain_trans::Trans;
use serde::de::{self, IntoDeserializer};
use smallvec::{smallvec, SmallVec};
use thiserror::Error;

use crate::forward_to_highest_priority;

/// Easy constants for use when processing deltas.
pub mod constants {
    pub const DELETE_OPTION: &str = "<DEL>";
}

/// utils for parsing.
pub(crate) mod utils {
    use std::borrow::Cow;

    /// Convert an integer index into a string key, with accelerated (non-allocating) values
    /// for low indices - which with tuple variants are what will be used the overwhelming
    /// majority of the time.
    pub fn accelerated_int_string(index: usize) -> Cow<'static, str> {
        match index {
            0 => "0".into(),
            1 => "1".into(),
            2 => "2".into(),
            3 => "3".into(),
            4 => "4".into(),
            5 => "5".into(),
            6 => "6".into(),
            7 => "7".into(),
            8 => "8".into(),
            9 => "9".into(),
            10 => "10".into(),
            11 => "11".into(),
            12 => "12".into(),
            13 => "13".into(),
            14 => "14".into(),
            15 => "15".into(),
            other => other.to_string().into(),
        }
    }
}

const OVERRIDE_STACK_ELEMS: usize = 4;

/// Trait indicating that this represents some collection of values in priority order (that can
/// always produce an output). It should simply output the highest-priority value directly without
/// attempting to combine them - it is, after all, a *simple priority order*.  
pub trait SimplePriority: Sized {
    type ResultingValue;

    fn extract_highest_priority_value(self) -> Self::ResultingValue;
}

// TODO: Embed "rearrangement" metadata to allow tracing the exact top-level value that caused
// errors. Should be Clone/Copy and in general lightweight, probably a generic parameter too,
// including on errors.
#[derive(Debug, PartialEq, Clone)]
pub struct OverrideValue {
    /// Base toml value, pre-overrides.
    pub base_value: toml::Value,

    /// Optionally specified overrides. The first value is the lowest priority and the last value
    /// is the highest priority.  
    pub overrides: SmallVec<[Option<toml::Value>; OVERRIDE_STACK_ELEMS]>,
}

impl OverrideValue {
    /// Extract the highest priority value via simple iterator stuff.
    ///
    /// Used for basic override systems with primitives.
    #[inline]
    pub fn extract_highest_priority_value(self) -> toml::Value {
        self.overrides.into_iter().fold(
            self.base_value,
            |curr_highest_priority, maybe_higher_priority| {
                maybe_higher_priority.unwrap_or(curr_highest_priority)
            },
        )
    }

    /// Extract an iterator that iterates over all values from lowest priority
    /// to highest priority - if specified. Does not include unspecified values, but it
    /// will always include at least one because the base value must be present.
    #[inline]
    pub fn into_upwards_priority_value_iterator(
        self,
    ) -> impl Iterator<Item = toml::Value> + DoubleEndedIterator {
        core::iter::once(self.base_value).chain(self.overrides.into_iter().flatten())
    }

    /// Construct an override set with the given base value and no overrides.
    #[inline]
    pub fn with_only_base_value(base_value: toml::Value) -> Self {
        Self {
            base_value,
            overrides: smallvec![],
        }
    }

    /// Add a new value to override the previous collection.
    #[inline]
    pub fn add_another_override(&mut self, new_override: toml::Value) {
        self.overrides.push(Some(new_override))
    }

    /// Get the highest-priority value by reference
    #[inline]
    pub fn extract_highest_priority_value_ref(&self) -> &toml::Value {
        self.overrides.iter().map(Option::as_ref).fold(
            &self.base_value,
            |curr_highest_priority, maybe_higher_priority| {
                maybe_higher_priority.unwrap_or(curr_highest_priority)
            },
        )
    }

    /// Implementation of [`core::iter::traits::FromIterator`] because we can't implement it on
    /// `Option<Self>` due to orphan rules.
    pub fn from_iter<I: IntoIterator<Item = toml::Value>>(iter: I) -> Option<Self> {
        let mut iterator = iter.into_iter();
        match iterator.next() {
            Some(base_value) => {
                let mut s = OverrideValue::with_only_base_value(base_value);
                s.overrides = iterator.map(Some).collect();
                Some(s)
            }
            // No initial value
            None => None,
        }
    }

    /// Check directly if this is a "deleted" override value - using [`constants::DELETE_OPTION`]
    #[inline]
    pub fn is_deleted(&self) -> bool {
        match self.extract_highest_priority_value_ref() {
            toml::Value::String(v) if v.as_str() == constants::DELETE_OPTION => true,
            _ => false,
        }
    }

    /// Reverse the priority of all items.
    pub fn reverse(self) -> Self {
        self.into_upwards_priority_value_iterator()
            .rev()
            .trans(Self::from_iter)
            .expect("Must have at least 1 value because the start of the iterator does")
    }
}

impl SimplePriority for OverrideValue {
    type ResultingValue = toml::Value;

    /// Extract the highest priority value via simple iterator stuff.
    ///
    /// Used for basic override systems with primitives.
    #[inline]
    fn extract_highest_priority_value(self) -> toml::Value {
        self.extract_highest_priority_value()
    }
}

/// The ultimate error for our multi-toml deserializer mechanism.
#[derive(Debug, Error)]
pub enum DeserializerError {
    #[error("{0}")]
    SimpleTOML(
        #[from] toml::de::Error,
        #[cfg(feature = "backtrace")]
        #[backtrace]
        Backtrace,
    ),
    #[error(transparent)]
    SeqConstruction(
        #[cfg_attr(feature = "backtrace", backtrace)]
        #[from]
        seq::ctor::SeqDeserializerConstructionError,
    ),
    #[error(transparent)]
    EnumTagDeserializerConstruction(
        #[cfg_attr(feature = "backtrace", backtrace)]
        #[from]
        enumeration::OverrideTaggedEnumDeserializerError,
    ),
    #[error(transparent)]
    StructureDeserializerConstruction(
        #[cfg_attr(feature = "backtrace", backtrace)]
        #[from]
        structure::StructureDeserializerCreationError,
    ),
}

#[cfg(not(feature = "backtrace"))]
impl de::Error for DeserializerError {
    #[inline]
    fn custom<T>(msg: T) -> Self
    where
        T: std::fmt::Display,
    {
        DeserializerError::SimpleTOML(toml::de::Error::custom(msg))
    }
}

#[cfg(feature = "backtrace")]
impl de::Error for DeserializerError {
    #[inline]
    fn custom<T>(msg: T) -> Self
    where
        T: std::fmt::Display,
    {
        DeserializerError::SimpleTOML(toml::de::Error::custom(msg), Backtrace::capture())
    }
}

impl<'de> de::Deserializer<'de> for OverrideValue {
    type Error = DeserializerError;

    fn deserialize_any<V>(self, _visitor: V) -> Result<V::Value, Self::Error>
    where
        V: de::Visitor<'de>,
    {
        Err(de::Error::custom(
            "deserialize_any unsupported for override deserializer",
        ))
    }

    forward_to_highest_priority! {
        bool i8 i16 i32 i64 i128 u8 u16 u32 u64 u128 f32 f64 char
        str string bytes byte_buf unit unit_struct
    }

    fn deserialize_option<V>(self, visitor: V) -> Result<V::Value, Self::Error>
    where
        V: de::Visitor<'de>,
    {
        // The first part of this where things get complex.
        //
        // Inner values are interpreted as `some`, because if it was `none`, then the
        // key wouldn't be mentioned. However, in the case of the override values,
        // an empty key just indicates "no override". What we want is a way of indicating
        // *deletion* of an overridden key.
        //
        // To do this we use [`constants::DELETE_OPTION`]
        //
        // Note: this is not a manual try fold - a manual try fold would stop at the first
        // [`None`], but this continues on in case there is more data.
        #[allow(clippy::manual_try_fold)]
        let final_option: Option<OverrideValue> = self.overrides.into_iter().fold(
            Some(OverrideValue::with_only_base_value(self.base_value)),
            |existing_value, new_possible_value| match (existing_value, new_possible_value) {
                (None, None) => None,
                (_, Some(toml::Value::String(s))) if s.as_str() == constants::DELETE_OPTION => None,
                (None, Some(new_base)) => Some(OverrideValue::with_only_base_value(new_base)),
                (Some(v), None) => Some(v),
                (Some(v0), Some(extra_v)) => {
                    Some(v0.trans_mut(|a| a.add_another_override(extra_v)))
                }
            },
        );
        match final_option {
            Some(value_to_parse) => Ok(visitor.visit_some(value_to_parse)?),
            None => visitor.visit_none(),
        }
    }

    // Newtype structs should be considered thin wrappers around normal
    // data as per https://serde.rs/impl-deserializer.html
    //
    // In particular, we don't do any prioritisation on them - all prioritisation
    // will be done on the inner value extracted by the visitor.
    #[inline]
    fn deserialize_newtype_struct<V>(
        self,
        _name: &'static str,
        visitor: V,
    ) -> Result<V::Value, Self::Error>
    where
        V: de::Visitor<'de>,
    {
        visitor.visit_newtype_struct(self)
    }

    /// Perform deserialization of sequence of *unknown* length.
    ///
    /// This can only be done from an array.
    fn deserialize_seq<V>(self, visitor: V) -> Result<V::Value, Self::Error>
    where
        V: de::Visitor<'de>,
    {
        let mut seq_deserializer = seq::ctor::SeqDeserializerCtor::new_from_overrides(
            self,
            seq::ctor::SeqDeserializerCtorInitMechanism::NoSizeHint,
        )?
        .make_seq_deserializer();
        visitor.visit_seq(&mut seq_deserializer)
    }

    // For tuple and tuple structs, we have actual type information, so we allow the option of the
    // user using the table-init form of seq deserializer.
    fn deserialize_tuple<V>(self, len: usize, visitor: V) -> Result<V::Value, Self::Error>
    where
        V: de::Visitor<'de>,
    {
        let mut seq_deserializer = seq::ctor::SeqDeserializerCtor::new_from_overrides(
            self,
            seq::ctor::SeqDeserializerCtorInitMechanism::ArrayOrIntKeyedMapSize(len),
        )?
        .make_seq_deserializer();
        visitor.visit_seq(&mut seq_deserializer)
    }

    #[inline]
    fn deserialize_tuple_struct<V>(
        self,
        _name: &'static str,
        len: usize,
        visitor: V,
    ) -> Result<V::Value, Self::Error>
    where
        V: de::Visitor<'de>,
    {
        self.deserialize_tuple(len, visitor)
    }

    #[inline]
    fn deserialize_map<V>(self, visitor: V) -> Result<V::Value, Self::Error>
    where
        V: de::Visitor<'de>,
    {
        let map_deser = structure::StructureDeserializer::try_create_map(self)?;
        visitor.visit_map(map_deser)
    }

    #[inline]
    fn deserialize_struct<V>(
        self,
        _name: &'static str,
        fields: &'static [&'static str],
        visitor: V,
    ) -> Result<V::Value, Self::Error>
    where
        V: de::Visitor<'de>,
    {
        let structured_deser = structure::StructureDeserializer::try_create(self, fields)?;
        visitor.visit_map(structured_deser)
    }

    // This is for tagged enums.
    //
    // Note that we explicitly look for
    fn deserialize_enum<V>(
        self,
        name: &'static str,
        _variants: &'static [&'static str],
        visitor: V,
    ) -> Result<V::Value, Self::Error>
    where
        V: de::Visitor<'de>,
    {
        if matches!(
            self.extract_highest_priority_value_ref(),
            toml::Value::String(_)
        ) {
            // Shortcut to variant deserializer for unit variant specifiers (string instead of
            // table containing a string key and variant value).
            match self.extract_highest_priority_value() {
                toml::Value::String(variant_name) => {
                    visitor.visit_enum(variant_name.into_deserializer())
                }
                _ => unreachable!("Just checked for matches."),
            }
        } else {
            let enumeration_vis =
                enumeration::OverrideTaggedEnumDeserializer::build_from_overrides(name, self)?;
            visitor.visit_enum(enumeration_vis)
        }
    }

    #[inline]
    fn deserialize_identifier<V>(self, visitor: V) -> Result<V::Value, Self::Error>
    where
        V: de::Visitor<'de>,
    {
        self.deserialize_string(visitor)
    }

    #[inline]
    fn deserialize_ignored_any<V>(self, visitor: V) -> Result<V::Value, Self::Error>
    where
        V: de::Visitor<'de>,
    {
        // We know the value is ignored, so we just visit a unit type.
        //
        // If the value is ignored it will generally be a [`serde::de::IgnoredAny`] which, well,
        // always just deserializes to itself. The deserialization of structured deltas is not
        // completely structured (we can't use `deserialize_any`), but it's structured *enough* to
        // be able to ignore a value.
        visitor.visit_unit()
    }

    #[inline]
    fn is_human_readable(&self) -> bool {
        self.base_value.is_human_readable()
    }
}

#[derive(Debug, PartialEq, Clone)]
pub struct OverrideTable {
    /// Base toml table, pre-overrides.
    pub base_table: toml::Table,

    /// Optionally specified overrides. The first value is the lowest priority and the last value
    /// is the highest priority
    pub overrides: SmallVec<[Option<toml::Table>; OVERRIDE_STACK_ELEMS]>,
}

impl From<OverrideTable> for OverrideValue {
    fn from(value: OverrideTable) -> Self {
        OverrideValue {
            base_value: toml::Value::Table(value.base_table),
            overrides: value
                .overrides
                .into_iter()
                .map(|a| a.map(toml::Value::Table))
                .collect(),
        }
    }
}

impl<'de> de::IntoDeserializer<'de, <OverrideValue as de::Deserializer<'de>>::Error>
    for OverrideTable
{
    type Deserializer = OverrideValue;

    #[inline]
    fn into_deserializer(self) -> Self::Deserializer {
        self.into()
    }
}

impl OverrideTable {
    #[inline]
    pub fn into_deserializer(self) -> OverrideValue {
        self.into()
    }
}

/// Construct an [`OverrideTable`] from a series of toml definitions as described in
/// [`toml::toml`].
///
/// Each definition is in it's own `{}` block, and the first specified toml is the lowest-priority
/// structure.
#[macro_export]
macro_rules! toml_overrides {
    ({$($base_toml_tts:tt)*}$(,)? $({$($toml_tts:tt)*}$(,)?)*) => {
        $crate::structured_delta::OverrideTable {
            base_table: ::toml::toml! { $($base_toml_tts)* },
            overrides: ::smallvec::smallvec![
                $(::core::option::Option::Some(::toml::toml! { $($toml_tts)* })),*
            ]
        }
    };
}

/// Parse some toml overrides like constructed with [`crate::toml_overrides`] into a type.
///
/// Assumes they are valid and will panic if not. The first value provided is the lowest priority,
/// unless you prefix the whole macro with `rev` and then the first value will be the highest
/// priority
///
/// If you prefix with `res` then this will return the full result rather than unwrapping, and if
/// you prefix with `revres` or `resrev`then it will do reversing but return the full result rather than
/// unwrapping.
///
/// Various keys can be added to modify the result too, after the `rev`/`res` declaration,
/// contained in a `+[]`. They are applied in order. Current keys are:
/// * `report` - this wraps the error type in the experimental `std::error::Report` if backtraces are enabled, with pretty-printing turned on.
#[macro_export]
macro_rules! toml_parse_overrides {
    (@apply_map => [report $($remaining_map_names:ident)*] $output:expr) => { {
        #[cfg(feature = "backtrace")]
        fn wraperr<T, E: ::std::error::Error>(v: ::core::result::Result<T, E>) -> ::core::result::Result<T, ::std::error::Report<E>> {
             v.map_err(|e| ::std::error::Report::new(e).pretty(true).show_backtrace(true))
        }

        #[cfg(not(feature = "backtrace"))]
        #[inline]
        fn wraperr<R>(r: R) -> R {
            r
        }
        let applied = wraperr($output);
        $crate::toml_parse_overrides!(@apply_map => [$($remaining_map_names)*] applied)
    } };
    (@apply_map $(=> [])? $output:expr) => { $output };
    (@overrides {$($tt:tt)*}) => {$crate::toml_overrides! { $($tt)* }.into_deserializer() };
    (@rev overrides {$($tt:tt)*}) => {$crate::toml_parse_overrides! {@overrides {$($tt)*}}.reverse() };
    // Internal - the params in the [] should be one of the @override-type entries.
    // Gets the result
    (@res $parse_ty:ty [$($tt:tt)*] $([$($map_tts:tt)*])?) => {{
        let override_values = $crate::toml_parse_overrides! { $($tt)* };
        $crate::toml_parse_overrides!{@apply_map $(=> [$($map_tts)*])? <$parse_ty as ::serde::de::Deserialize>::deserialize(override_values)}
    }};
    ($(+[$($map_tts:tt)*])? $parse_type:ty: {$($tt:tt)*} ) => {$crate::toml_parse_overrides! {@res $parse_type [@overrides {$($tt)*}] $([$($map_tts)*])? }.unwrap()};
    (rev $(+[$($map_tts:tt)*])? $parse_type:ty: {$($tt:tt)*} ) => {$crate::toml_parse_overrides! {@res $parse_type [@rev overrides {$($tt)*}] $([$($map_tts)*])? }.unwrap()};
    (res $(+[$($map_tts:tt)*])? $parse_type:ty: {$($tt:tt)*} ) => {$crate::toml_parse_overrides! {@res $parse_type [@overrides {$($tt)*}] $([$($map_tts)*])? }};
    (resrev $(+[$($map_tts:tt)*])? $parse_type:ty: {$($tt:tt)*} ) => {$crate::toml_parse_overrides! {@res $parse_type [@rev overrides {$($tt)*}] $([$($map_tts)*])? }};
    (revres $(+[$($map_tts:tt)*])? $parse_type:ty: {$($tt:tt)*} ) => {$crate::toml_parse_overrides! {@res $parse_type [@rev overrides {$($tt)*}] $([$($map_tts)*])? }};
}

#[cfg(test)]
mod test {
    use std::collections::HashMap;

    /// Simply create a hashmap from string -> string without messy iterator stuff.
    macro_rules! stringmap {
        [$(($key:literal, $value:literal)),*] => {
            HashMap::<String, String>::from_iter([$(($key, $value),)*].into_iter().map(|(k, v)| (k.to_owned(), v.to_owned())))
        }
    }

    #[derive(Debug, serde::Deserialize, PartialEq, Eq)]
    #[serde(rename_all = "kebab-case")]
    enum SimpleEnum {
        A,
        #[serde(rename_all = "kebab-case")]
        B {
            embedded_bytes: HashMap<String, Vec<u8>>,
        },
        C(String, String),
        #[serde(rename_all = "kebab-case")]
        D {
            name: String,
            dob: String,
        },
        E(String),
    }

    #[test]
    fn simple_enum_override() {
        assert_eq!(
            SimpleEnum::D {
                name: "Alice".to_string(),
                dob: "13-07-1928".to_string()
            },
            toml_parse_overrides!(rev SimpleEnum: {
            {
                [d]
                    name = "Alice"
            }, {
                [d]
                    name = "Bob"
                    dob = "13-07-1928"
            }, {
                [a]
            }
            })
        );

        assert_eq!(
            SimpleEnum::E("HELLO".to_owned()),
            toml_parse_overrides!(rev SimpleEnum: {
                {
                    e = "HELLO"
                },
                {
                    d = { name = "Alice" }
                },
                {
                    d = { name = "Eve", dob = "TODAY, ON YOUR MACHINE RIGHT NOW 🤖" }
                },
                {
                    a = {}
                },
                {
                    e = "good bye"
                }
            })
        );

        assert_eq!(
            SimpleEnum::C("hi".to_owned(), "you".to_owned()),
            toml_parse_overrides!(rev SimpleEnum: {
                {
                    [c]
                    "0" = "<DEL>"
                    prepend = ["hi"]

                },
                {
                    c = ["hello", "you"]
                }
            })
        );

        assert_eq!(
            SimpleEnum::B {
                embedded_bytes: HashMap::from_iter([
                    ("boot.efi".to_owned(), vec![23, 2, 245, 87, 35, 9]),
                    ("run.efi".to_owned(), vec![83, 212, 98, 56, 10, 3, 0])
                ])
            },
            toml_parse_overrides!(rev +[report] SimpleEnum: {
                {
                    [b.embedded-bytes]
                    "memchk.efi" = "<DEL>"
                    "boot.efi" = [23, 2, 245, 87, 35, 9]
                },
                {
                    [b.embedded-bytes."memchk.efi"]
                    append = [3, 4]

                    [b.embedded-bytes."run.efi"]
                    append = [3, 0]
                },
                {
                    [b.embedded-bytes]
                    "memchk.efi" = [0, 0, 0, 0, 0]
                    "run.efi" = [83, 212, 98, 56, 10]
                },
            })
        )
    }

    #[derive(serde::Deserialize, Clone, Debug, PartialEq, Eq)]
    #[serde(rename_all = "kebab-case")]
    struct SimpleNamedPerson {
        name: String,
        pronouns: String,
        contact_details: HashMap<String, String>,
    }

    /// Testing for `Option` capability and real overriding.
    ///
    /// Almost had an error with that.
    #[derive(serde::Deserialize, Clone, Debug, PartialEq, Eq)]
    #[serde(rename_all = "kebab-case")]
    struct SimpleFriendship {
        person: SimpleNamedPerson,
        friend: SimpleNamedPerson,
        shared_frenemy: Option<SimpleNamedPerson>,
        frenemy_reason: Option<String>,
    }

    #[test]
    fn option_override_test() {
        assert_eq!(SimpleFriendship {
            person: SimpleNamedPerson {
                name: "Catherine".to_owned(),
                pronouns: "she/her".to_owned(),
                contact_details: stringmap![
                    ("email", "catty@example.com"),
                    ("matrix", "@catty:example.com")
                ]
            },
            friend: SimpleNamedPerson {
                name: "Adri".to_owned(),
                pronouns: "they/them".to_owned(),
                contact_details: stringmap![
                    ("gpg-key", "0xA3729FD920EC47833B"),
                    ("email", "adri-the-gay@example.com")
                ]
            },
            shared_frenemy: Some(SimpleNamedPerson {
                name: "Zach".to_owned(),
                pronouns: "he/they".to_owned(),
                contact_details: stringmap![
                    ("mastodon", "@zach-dm@example.com"),
                    ("email", "zacharina-of-time@example.com")
                ]
            }),
            frenemy_reason: Some("Zach beat Adri and Catherine in the Landlord's Game (Monopoly) when they were all 14 years old.".to_string())
        }, toml_parse_overrides!(rev SimpleFriendship: {
            {
                frenemy-reason = "Zach beat Adri and Catherine in the Landlord's Game (Monopoly) when they were all 14 years old."

                [shared-frenemy]
                pronouns = "he/they"

                [shared-frenemy.contact-details]
                twitter = "<DEL>"
                mastodon = "@zach-dm@example.com"
                email = "zacharina-of-time@example.com"
            },
            {
                [shared-frenemy]
                name = "Zach"
                pronouns = "he/him"
                
                [shared-frenemy.contact-details]
                email = "zach1201@example.com"
                twitter = "@zach1201"
            },
            {
                shared-frenemy = "<DEL>"
            },
            {
                [person]
                name = "Catherine"
                pronouns = "she/her"
                
                [person.contact-details]
                email = "catty@example.com"
                matrix = "@catty:example.com"

                [friend]
                name = "Adri"
                pronouns = "they/them"

                [friend.contact-details]
                gpg-key = "0xA3729FD920EC47833B"
                email = "adri-the-gay@example.com"

                [shared-frenemy]
                name = "Elon"
                pronouns = "he/him"
            },
        }))
    }
}

// SDSB - Manage initramfs generators, kernels, and self-controlled secure boot.
// Copyright (C) 2023 Matti Bryce <mattibryce at protonmail dot com>
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <https://www.gnu.org/licenses/>.
