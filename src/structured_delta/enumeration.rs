#[cfg(feature = "backtrace")]
use std::backtrace::Backtrace;

use serde::{
    de::{self, IntoDeserializer},
    Deserializer,
};
use smallvec::{smallvec, SmallVec};
use thiserror::Error;

use super::{DeserializerError, OverrideValue, OVERRIDE_STACK_ELEMS};

/// This essentially acts as a deserializer "of" a map (table) into an externally-tagged enum.
///
/// The implementation inside [`toml::value`] (`MapDeserializer`) can act both as an actual map
/// deserializer and a top-level externally tagged enum deserializer. However, in this override
/// system, real map deserialization acts slightly different to tagged enum deserialization.  
///
/// In particular, when combining two maps which have different keys in them, generally, they are
/// "union"'d recursively. However, when deserializing a top-level enum, this would result in
/// instantiating the enum with multiple variants.
///
/// So for deserializing externally-tagged enums, we need a different algorithm that completely
/// overrides/"deletes" the value of the internal tables and creates a variant deserializer only
/// from the tables for the most-prioritised key.
///
/// Note that in the actual enum implmeentation in the main deserializer, it is needed to also
/// allow for selecting simple enum variants (no extra payload) using strings. This must happen
/// before this is constructed.
pub(crate) struct OverrideTaggedEnumDeserializer {
    /// Iterator of values for the base table containing the single entry for a
    /// tagged enum.
    ///
    /// Should have exactly one entry.
    pub(crate) base_table_iter: <toml::Table as IntoIterator>::IntoIter,

    /// Iterators over other tables, in lowest -> highest priority order.
    ///
    /// Each iterator - if actually present - should contain exactly one entry.
    pub(crate) override_tagged_enum_table_iters:
        SmallVec<[Option<<toml::Table as IntoIterator>::IntoIter>; OVERRIDE_STACK_ELEMS]>,
}

/// Error when trying to construct an [`OverrideTaggedEnumDeserializer`] from a normal
/// [`super::OverrideValue`].
///
/// Note: you should manually check for if the override value is "meant" to be a unit-variant-only
/// indicator by testing for if it is a string, before calling the conversion function. If you
/// don't, an error about the type will be generated.
#[derive(Error, Debug)]
pub enum OverrideTaggedEnumDeserializerErrorInner {
    #[error("Enum value for {enumeration_name} was not a table - only tables (or strings for modes without extra data attached) are allowed. Value was of type {toml_type_str}")]
    NonTableInOverrideHierarchy {
        enumeration_name: &'static str,
        toml_type_str: &'static str,
    },
    #[error("Tagged enum table had zero entries, when it should have exactly one (a string key (indicating the selected variant of {enumeration_name}) containing the value that defines the variant or overrides select components of the variant)")]
    EmptyTable { enumeration_name: &'static str },
    #[error("Tagged enum table had {entry_number} entries, when it should have exactly one (a string key (indicating the selected variant of {enumeration_name}) containing the value that defines the variant or overrides select components of the variant)")]
    TooBigTable {
        enumeration_name: &'static str,
        entry_number: usize,
    },
}

#[cfg(feature = "backtrace")]
#[derive(Debug, Error)]
#[error("{source}")]
pub struct OverrideTaggedEnumDeserializerError {
    #[from]
    pub source: OverrideTaggedEnumDeserializerErrorInner,
    #[backtrace]
    pub backtrace: Backtrace,
}

#[cfg(not(feature = "backtrace"))]
#[derive(Debug, Error)]
#[error("{source}")]
pub struct OverrideTaggedEnumDeserializerError {
    #[from]
    pub source: OverrideTaggedEnumDeserializerErrorInner,
}

impl OverrideTaggedEnumDeserializer {
    /// Attempt to construct a tagged enum deserializer (the name should be provided for error
    /// reporting). You should check for if the highest-priority value is a string before you use
    /// this (as strings are allowed for unit variants and should be passed in as the EnumAccess to
    /// the visitor in the case they are present).
    pub(crate) fn build_from_overrides(
        enumeration_name: &'static str,
        overrides: OverrideValue,
    ) -> Result<Self, OverrideTaggedEnumDeserializerError> {
        // Attempts to extract the table as well as validate the # of entries in it.
        let try_extract_validate_table =
            |value: toml::Value| -> Result<toml::Table, OverrideTaggedEnumDeserializerError> {
                match value {
                    toml::Value::Table(t) => {
                        if t.is_empty() {
                            Err(OverrideTaggedEnumDeserializerErrorInner::EmptyTable {
                                enumeration_name,
                            }
                            .into())
                        } else if t.len() > 1 {
                            Err(OverrideTaggedEnumDeserializerErrorInner::TooBigTable {
                                enumeration_name,
                                entry_number: t.len(),
                            }
                            .into())
                        } else {
                            Ok(t)
                        }
                    }
                    invalid => Err(
                        OverrideTaggedEnumDeserializerErrorInner::NonTableInOverrideHierarchy {
                            toml_type_str: invalid.type_str(),
                            enumeration_name,
                        }
                        .into(),
                    ),
                }
            };

        let base_table_iter = try_extract_validate_table(overrides.base_value)?.into_iter();
        let override_table_iters: SmallVec<[Option<_>; OVERRIDE_STACK_ELEMS]> = overrides
            .overrides
            .into_iter()
            .map(|maybe_v| match maybe_v {
                Some(v) => Ok(Some(try_extract_validate_table(v)?.into_iter())),
                None => Ok(None),
            })
            .collect::<Result<_, OverrideTaggedEnumDeserializerError>>()?;

        Ok(Self {
            base_table_iter,
            override_tagged_enum_table_iters: override_table_iters,
        })
    }
}

impl<'de> de::EnumAccess<'de> for OverrideTaggedEnumDeserializer {
    type Error = DeserializerError;

    type Variant = OverrideVariantDeserializer;

    fn variant_seed<V>(mut self, seed: V) -> Result<(V::Value, Self::Variant), Self::Error>
    where
        V: de::DeserializeSeed<'de>,
    {
        let (mut variant_tag_key, mut curr_variant_base_val) = match self.base_table_iter.next() {
            Some(pair) => pair,
            None => {
                return Err(de::Error::custom(
                    "expected table with exactly 1 entry, found empty table",
                ));
            }
        };

        // The override values for the current variant of the externally tagged enum that is
        // selected.
        //
        // Resets every time the enum is found to actually be a different variant in an overriding
        // map.
        let mut curr_variant_override_vals: SmallVec<[Option<toml::Value>; OVERRIDE_STACK_ELEMS]> =
            smallvec![];
        // Iterate over the override tables.
        //
        // If the override table exists but is empty (has no enum variant tag key in it) then
        // we error out.
        for mut override_table_elems in self.override_tagged_enum_table_iters.into_iter().flatten()
        {
            let (curr_variant_tag_key, new_overriding_value) = match override_table_elems.next() {
                Some(pair) => pair,
                None => {
                    return Err(de::Error::custom(
                        "expected table with exactly one entry, found empty table",
                    ));
                }
            };

            // These are overrides for the same variant... push the new value onto
            // the current variant overrides as a Some().
            if curr_variant_tag_key == variant_tag_key {
                curr_variant_override_vals.push(Some(new_overriding_value));
            } else {
                // Reset the override values and set the new tag key and base value.
                curr_variant_override_vals.clear();
                variant_tag_key = curr_variant_tag_key;
                curr_variant_base_val = new_overriding_value;
            }
        }

        let parsed_variant_tag = seed
            .deserialize::<de::value::StringDeserializer<toml::de::Error>>(
                variant_tag_key.into_deserializer(),
            )?;
        let variant_deser = OverrideVariantDeserializer::new(OverrideValue {
            base_value: curr_variant_base_val,
            overrides: curr_variant_override_vals,
        });
        Ok((parsed_variant_tag, variant_deser))
    }
}

/// Deserializes the value of the (variant tag, value) pair in a single-entry externally
/// tagged enum table. This is designed to act like the version in [`toml::Value`] but with
/// override values instead of normal single values.
pub(crate) struct OverrideVariantDeserializer {
    /// Set of overided values for this enum variant. Should only include
    /// values for the highest-priority enum variant rather than all variants.
    pub(crate) override_set: OverrideValue,
}

impl OverrideVariantDeserializer {
    #[inline]
    pub(crate) fn new(overrides: OverrideValue) -> Self {
        Self {
            override_set: overrides,
        }
    }
}

impl<'de> de::VariantAccess<'de> for OverrideVariantDeserializer {
    type Error = DeserializerError;

    // The default form of this validates empty-table-ness.
    //
    // We could in theory just take the top-level override and only check that. However, that would
    // allow easy masking of errors and hacks, so we validate all values.
    fn unit_variant(self) -> Result<(), Self::Error> {
        self.override_set
            .into_upwards_priority_value_iterator()
            .try_for_each(|value| match value {
                toml::Value::Table(values) => {
                    if values.is_empty() {
                        Ok(())
                    } else {
                        Err(de::Error::custom("expected empty table"))
                    }
                }
                e => Err(de::Error::custom(format!(
                    "expected table, found {}",
                    e.type_str()
                ))),
            })
    }

    // Simply forward through
    #[inline]
    fn newtype_variant_seed<T>(self, seed: T) -> Result<T::Value, Self::Error>
    where
        T: de::DeserializeSeed<'de>,
    {
        seed.deserialize(self.override_set)
    }

    #[inline]
    fn tuple_variant<V>(self, len: usize, visitor: V) -> Result<V::Value, Self::Error>
    where
        V: de::Visitor<'de>,
    {
        // Parse the same as a tuple.
        self.override_set.deserialize_tuple(len, visitor)
    }

    #[inline]
    fn struct_variant<V>(
        self,
        fields: &'static [&'static str],
        visitor: V,
    ) -> Result<V::Value, Self::Error>
    where
        V: de::Visitor<'de>,
    {
        self.override_set.deserialize_struct("", fields, visitor)
    }
}

// SDSB - Manage initramfs generators, kernels, and self-controlled secure boot.
// Copyright (C) 2023 Matti Bryce <mattibryce at protonmail dot com>
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <https://www.gnu.org/licenses/>.
