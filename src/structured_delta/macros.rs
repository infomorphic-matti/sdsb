//! Utility macros for implementing the toml structured delta mechanism for config overrides.
//!
//! Based on the serde `forward_to_deserialize_any!` macros.

/// Helper macro when implementing the `Deserializer` part of override-based config file
/// formats.
///
/// Used to simply extract the highest-priority value present and then (without checking
/// intermediary-priority values for validity) calling the visitor on the [`serde::Deserializer`]
/// implementation of that value. It assumes the deserializer implements [`super::SimplePriority`].
///
/// Use this for, for example, primitives. It's used exactly the same as you would use the
/// `forward_to_deserialize_any!` macro from serde, but it also includes `any` as a
/// possibility.
///
///
/// ```edition2021
/// # use sdsb::structured_delta::{macros::forward_to_simple_priority, SimplePriority};
/// # use serde::{de::{value, Deserializer, Visitor}, forward_to_deserialize_any};
/// #
/// # struct MyDeserializer;
/// # struct MyInnerDeserializer;
/// #  
/// impl SimplePriority for MyDeserializer {
///     type ResultingValue = MyInnerDeserializer;
///     
///     fn extract_highest_priority_value(self) -> Self::ResultingValue {
///         unimplemented!()
///     }
/// }
/// #
/// # impl <'de> Deserializer<'de> for MyInnerDeserializer {
/// #     type Error = value::Error;
/// #     
/// #     fn deserialize_any<V>(self, _: V) -> Result<V::Value, Self::Error>
/// #     where
/// #         V: Visitor<'de>,
/// #     {
/// #         unimplemented!()
/// #     }
/// #
/// #     forward_to_deserialize_any! {
/// #         i8 i16 i32 i64 i128 u8 u16 u32 u64 u128 f32 f64 char str string
/// #         bytes byte_buf option unit unit_struct newtype_struct seq tuple
/// #         tuple_struct map struct enum identifier ignored_any bool
/// #     }
/// # }
/// #
/// #
/// # impl<'de> Deserializer<'de> for MyDeserializer {
/// #     type Error = value::Error;
/// #
/// #     fn deserialize_any<V>(self, _: V) -> Result<V::Value, Self::Error>
/// #     where
/// #         V: Visitor<'de>,
/// #     {
/// #         unimplemented!()
/// #     }
/// #
/// #[inline]
/// fn deserialize_bool<V>(self, visitor: V) -> Result<V::Value, Self::Error>
/// where
///     V: Visitor<'de>,
/// {
///     self.extract_highest_priority_value().deserialize_bool(visitor)
/// }
/// #
/// #     forward_to_deserialize_any! {
/// #         i8 i16 i32 i64 i128 u8 u16 u32 u64 u128 f32 f64 char str string
/// #         bytes byte_buf option unit unit_struct newtype_struct seq tuple
/// #         tuple_struct map struct enum identifier ignored_any
/// #     }
/// # }
/// ```
///
/// The `forward_to_highest_priority!` macro implements these simple forwarding
/// methods so that they forward directly to the output of
/// [`SimplePriority::extract_highest_priority_value`].
/// You can choose which methods to forward.
///
/// ```edition2021
/// # use sdsb::structured_delta::{macros::forward_to_simple_priority, SimplePriority};
/// # use serde::forward_to_deserialize_any;
/// # use serde::de::{value, Deserializer, Visitor};
/// #
/// # struct MyDeserializer;
/// # struct MyInnerDeserializer;
/// #  
/// impl SimplePriority for MyDeserializer {
///     type ResultingValue = MyInnerDeserializer;
///     
///     fn extract_highest_priority_value(self) -> Self::ResultingValue {
///         unimplemented!()
///     }
/// }
/// #
/// # impl <'de> Deserializer<'de> for MyInnerDeserializer {
/// #     type Error = value::Error;
/// #     
/// #     fn deserialize_any<V>(self, _: V) -> Result<V::Value, Self::Error>
/// #     where
/// #         V: Visitor<'de>,
/// #     {
/// #         unimplemented!()
/// #     }
/// #
/// #     forward_to_deserialize_any! {
/// #         i8 i16 i32 i64 i128 u8 u16 u32 u64 u128 f32 f64 char str string
/// #         bytes byte_buf option unit unit_struct newtype_struct seq tuple
/// #         tuple_struct map struct enum identifier ignored_any bool
/// #     }
/// # }
/// #
/// #
/// impl<'de> Deserializer<'de> for MyDeserializer {
/// #   type Error = value::Error;
/// #
///     forward_to_highest_priority! {
///         bool i8 i16 i32 i64 i128 u8 u16 u32 u64 u128 f32 f64 char str string
///         bytes byte_buf option unit unit_struct newtype_struct seq tuple
///         tuple_struct map struct enum identifier ignored_any any
///     }
/// }
/// ```
///
/// The macro assumes the convention that your `Deserializer` lifetime parameter
/// is called `'de` and that the `Visitor` type parameters on each method are
/// called `V`. A different type parameter and a different lifetime can be
/// specified explicitly if necessary.
///
/// ```edition2021
/// # use sdsb::structured_delta::{macros::forward_to_simple_priority, SimplePriority};
/// # use serde::forward_to_deserialize_any;
/// # use serde::de::{value, Deserializer, Visitor};
/// # use std::marker::PhantomData;
/// #
/// # struct MyDeserializer<V>(PhantomData<V>);
/// # struct MyInnerDeserializer;
/// #  
/// impl<V> SimplePriority for MyDeserializer<V> {
///     type ResultingValue = MyInnerDeserializer;
///     
///     fn extract_highest_priority_value(self) -> Self::ResultingValue {
///         unimplemented!()
///     }
/// }
/// #
/// # impl <'de> Deserializer<'de> for MyInnerDeserializer {
/// #     type Error = value::Error;
/// #     
/// #     fn deserialize_any<V>(self, _: V) -> Result<V::Value, Self::Error>
/// #     where
/// #         V: Visitor<'de>,
/// #     {
/// #         unimplemented!()
/// #     }
/// #
/// #     forward_to_deserialize_any! {
/// #         i8 i16 i32 i64 i128 u8 u16 u32 u64 u128 f32 f64 char str string
/// #         bytes byte_buf option unit unit_struct newtype_struct seq tuple
/// #         tuple_struct map struct enum identifier ignored_any bool
/// #     }
/// # }
/// #
/// #
/// #
/// # impl<'q, V> Deserializer<'q> for MyDeserializer<V> {
/// #     type Error = value::Error;
/// #
/// #
/// forward_to_highest_priority! {
///     <W: Visitor<'q>>
///     bool i8 i16 i32 i64 i128 u8 u16 u32 u64 u128 f32 f64 char str string
///     bytes byte_buf option unit unit_struct newtype_struct seq tuple
///     tuple_struct map struct enum identifier ignored_any any
/// }
/// # }
/// ```
#[macro_export(local_inner_macros)]
macro_rules! forward_to_highest_priority {
    (<$visitor:ident: Visitor<$lifetime:tt>> $($func:ident)*) => {
        $(forward_to_highest_priority_helper!{$func<$lifetime, $visitor>})*
    };
    // This case must be after the previous one.
    ($($func:ident)*) => {
        $(forward_to_highest_priority_helper!{$func<'de, V>})*
    };
}

#[doc(hidden)]
#[macro_export]
macro_rules! forward_to_highest_priority_method {
    ($func:ident<$l:tt, $v:ident>($($arg:ident : $ty:ty),*)) => {
        #[inline]
        fn $func<$v>(self, $($arg: $ty,)* visitor: $v) -> ::core::result::Result<$v::Value, Self::Error>
        where
            $v: ::serde::de::Visitor<$l>,
        {
            use $crate::structured_delta::SimplePriority;
            use ::serde::de::Deserializer;
            use ::core::convert::Into;
            let highest_priority = <Self as SimplePriority>::extract_highest_priority_value(self);
            <<Self as SimplePriority>::ResultingValue as Deserializer::<$l>>::$func(highest_priority, $($arg,)* visitor).map_err(Into::into)
        }
    };
}

#[doc(hidden)]
#[macro_export(local_inner_macros)]
macro_rules! forward_to_highest_priority_helper {
    (bool<$l:tt, $v:ident>) => {
        forward_to_highest_priority_method!{deserialize_bool<$l, $v>()}
    };
    (i8<$l:tt, $v:ident>) => {
        forward_to_highest_priority_method!{deserialize_i8<$l, $v>()}
    };
    (i16<$l:tt, $v:ident>) => {
        forward_to_highest_priority_method!{deserialize_i16<$l, $v>()}
    };
    (i32<$l:tt, $v:ident>) => {
        forward_to_highest_priority_method!{deserialize_i32<$l, $v>()}
    };
    (i64<$l:tt, $v:ident>) => {
        forward_to_highest_priority_method!{deserialize_i64<$l, $v>()}
    };
    (i128<$l:tt, $v:ident>) => {
        forward_to_highest_priority_method!{deserialize_i128<$l, $v>()}
    };
    (u8<$l:tt, $v:ident>) => {
        forward_to_highest_priority_method!{deserialize_u8<$l, $v>()}
    };
    (u16<$l:tt, $v:ident>) => {
        forward_to_highest_priority_method!{deserialize_u16<$l, $v>()}
    };
    (u32<$l:tt, $v:ident>) => {
        forward_to_highest_priority_method!{deserialize_u32<$l, $v>()}
    };
    (u64<$l:tt, $v:ident>) => {
        forward_to_highest_priority_method!{deserialize_u64<$l, $v>()}
    };
    (u128<$l:tt, $v:ident>) => {
        forward_to_highest_priority_method!{deserialize_u128<$l, $v>()}
    };
    (f32<$l:tt, $v:ident>) => {
        forward_to_highest_priority_method!{deserialize_f32<$l, $v>()}
    };
    (f64<$l:tt, $v:ident>) => {
        forward_to_highest_priority_method!{deserialize_f64<$l, $v>()}
    };
    (char<$l:tt, $v:ident>) => {
        forward_to_highest_priority_method!{deserialize_char<$l, $v>()}
    };
    (str<$l:tt, $v:ident>) => {
        forward_to_highest_priority_method!{deserialize_str<$l, $v>()}
    };
    (string<$l:tt, $v:ident>) => {
        forward_to_highest_priority_method!{deserialize_string<$l, $v>()}
    };
    (bytes<$l:tt, $v:ident>) => {
        forward_to_highest_priority_method!{deserialize_bytes<$l, $v>()}
    };
    (byte_buf<$l:tt, $v:ident>) => {
        forward_to_highest_priority_method!{deserialize_byte_buf<$l, $v>()}
    };
    (option<$l:tt, $v:ident>) => {
        forward_to_highest_priority_method!{deserialize_option<$l, $v>()}
    };
    (unit<$l:tt, $v:ident>) => {
        forward_to_highest_priority_method!{deserialize_unit<$l, $v>()}
    };
    (unit_struct<$l:tt, $v:ident>) => {
        forward_to_highest_priority_method!{deserialize_unit_struct<$l, $v>(name: &'static str)}
    };
    (newtype_struct<$l:tt, $v:ident>) => {
        forward_to_highest_priority_method!{deserialize_newtype_struct<$l, $v>(name: &'static str)}
    };
    (seq<$l:tt, $v:ident>) => {
        forward_to_highest_priority_method!{deserialize_seq<$l, $v>()}
    };
    (tuple<$l:tt, $v:ident>) => {
        forward_to_highest_priority_method!{deserialize_tuple<$l, $v>(len: usize)}
    };
    (tuple_struct<$l:tt, $v:ident>) => {
        forward_to_highest_priority_method!{deserialize_tuple_struct<$l, $v>(name: &'static str, len: usize)}
    };
    (map<$l:tt, $v:ident>) => {
        forward_to_highest_priority_method!{deserialize_map<$l, $v>()}
    };
    (struct<$l:tt, $v:ident>) => {
        forward_to_highest_priority_method!{deserialize_struct<$l, $v>(name: &'static str, fields: &'static [&'static str])}
    };
    (enum<$l:tt, $v:ident>) => {
        forward_to_highest_priority_method!{deserialize_enum<$l, $v>(name: &'static str, variants: &'static [&'static str])}
    };
    (identifier<$l:tt, $v:ident>) => {
        forward_to_highest_priority_method!{deserialize_identifier<$l, $v>()}
    };
    (ignored_any<$l:tt, $v:ident>) => {
        forward_to_highest_priority_method!{deserialize_ignored_any<$l, $v>()}
    };
    // Any can also have this impled
    (any<$l:tt, $v:ident>) => {
        forward_to_highest_priority_method!{deserialize_any<$l, $v>()}
    };
}

/// Our errors are often split into Inner (enum) and the main error type.
///
/// Each enum may implement `From` for any inner error types.
///
/// This macro allows implementing `From` for the outer error type from the error types inside the
/// inner enum, by using each from implementation in turn.
#[macro_export]
macro_rules! fromchain {
    ($outer:ty => $inner:ty => impl From<$($suberror:ty),*>) => {
        $(#[automatically_derived]
          impl ::core::convert::From::<$suberror> for $outer {
            #[inline]
            fn from(value: $suberror) -> Self {
                let inner_err = <$inner as ::core::convert::From::<$suberror>>::from(value);
                <$outer as ::core::convert::From::<$inner>>::from(inner_err)
            }
        })*
    };
}

// SDSB - Manage initramfs generators, kernels, and self-controlled secure boot.
// Copyright (C) 2023 Matti Bryce <mattibryce at protonmail dot com>
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <https://www.gnu.org/licenses/>.
