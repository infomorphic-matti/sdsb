use chain_trans::Trans;
use serde::de;

use super::{DeserializerError, OverrideValue};

pub mod action;
pub mod ctor;

pub(crate) enum SeqDeserializer<SetModeIter, ArrayModeIter, SimpleArrayModeIter> {
    /// For the case where the array is "set mode"
    SetMode(SetModeIter),
    /// For situations where the array has been overridden
    ArrayMode(ArrayModeIter),
    /// For the case where there is no overrides on a simple array.
    SimpleArrayMode(SimpleArrayModeIter),
}

impl<
        'de,
        SetModeIter: Iterator<Item = toml::Value>,
        ArrayModeIter: Iterator<Item = OverrideValue>,
        SimpleArrayModeIter: Iterator<Item = toml::Value>,
    > de::SeqAccess<'de> for SeqDeserializer<SetModeIter, ArrayModeIter, SimpleArrayModeIter>
{
    type Error = DeserializerError;

    fn next_element_seed<T>(&mut self, seed: T) -> Result<Option<T::Value>, Self::Error>
    where
        T: de::DeserializeSeed<'de>,
    {
        // We don't simply use a function here because value can be either toml::Value or
        // OverrideValue, and the signature of the function (due to lack of ability to use types
        // directly from the context of this function) would be ridiculous compared to just
        // copy-pasting.
        macro_rules! iter_deser {
            ($iter:ident, $seed:ident) => {
                match $iter.next() {
                    Some(value) => $seed.deserialize(value).map(Some).map_err(Into::into),
                    None => Ok(None),
                }
            };
        }

        match self {
            SeqDeserializer::SetMode(s_iter) => iter_deser!(s_iter, seed),
            SeqDeserializer::ArrayMode(a_iter) => iter_deser!(a_iter, seed),
            SeqDeserializer::SimpleArrayMode(sa_iter) => iter_deser!(sa_iter, seed),
        }
    }

    fn size_hint(&self) -> Option<usize> {
        /// Use the same hint system as in [`toml::value::SeqDeserializer`]
        #[inline]
        fn equal_upper_lower_size_hint(iter_hint: (usize, Option<usize>)) -> Option<usize> {
            match iter_hint {
                (lower, Some(upper)) if lower == upper => Some(upper),
                _ => None,
            }
        }

        match self {
            SeqDeserializer::SetMode(s) => s.size_hint(),
            SeqDeserializer::ArrayMode(a) => a.size_hint(),
            SeqDeserializer::SimpleArrayMode(sa) => sa.size_hint(),
        }
        .trans(equal_upper_lower_size_hint)
    }
}

// SDSB - Manage initramfs generators, kernels, and self-controlled secure boot.
// Copyright (C) 2023 Matti Bryce <mattibryce at protonmail dot com>
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <https://www.gnu.org/licenses/>.
