use std::{
    collections::{HashMap, HashSet},
    num::TryFromIntError,
};

use smallvec::SmallVec;
use thiserror::Error;

use crate::structured_delta::{constants, OverrideValue, OVERRIDE_STACK_ELEMS};

#[derive(Default, Debug)]
pub(crate) struct Action {
    pub appends: SmallVec<[toml::Value; OVERRIDE_STACK_ELEMS]>,
    pub prepends: SmallVec<[toml::Value; OVERRIDE_STACK_ELEMS]>,
    pub lstrip: usize,
    pub rstrip: usize,
    pub index_deletions: HashSet<usize>,
    /// Index overrides.
    pub index_overrides: HashMap<usize, toml::Value>,
}

impl Action {
    /// Try and parse this from a map.
    ///
    /// Note that this expects any `set = false/true` keys to be removed before parsing, otherwise
    /// they will be treated as an error.
    pub(crate) fn try_from_map(
        current_base_array_length: usize,
        map: toml::Table,
    ) -> Result<Self, ActionParseError> {
        use ActionParseError as E;
        let mut accumulated_state = Self::default();
        for (key, raw_value) in map.into_iter() {
            match (key.as_ref(), raw_value) {
                ("append", toml::Value::Array(a)) => {
                    accumulated_state.appends.extend(a.into_iter())
                }
                ("append", invalid) => {
                    return Err(E::KeyShouldBeArray {
                        key: "append",
                        type_str: invalid.type_str(),
                    })
                }
                ("prepend", toml::Value::Array(a)) => {
                    accumulated_state.prepends.extend(a.into_iter())
                }
                ("prepend", invalid) => {
                    return Err(E::KeyShouldBeArray {
                        key: "prepend",
                        type_str: invalid.type_str(),
                    })
                }
                ("lstrip", toml::Value::Integer(i)) => {
                    accumulated_state.lstrip = i.try_into().map_err(|e| E::IntOutOfRange {
                        key: "lstrip",
                        provided_int: i,
                        error: e,
                    })?;
                }
                ("lstrip", invalid) => {
                    return Err(E::KeyShouldBeInteger {
                        key: "lstrip",
                        type_str: invalid.type_str(),
                    })
                }
                ("rstrip", toml::Value::Integer(i)) => {
                    accumulated_state.rstrip = i.try_into().map_err(|e| E::IntOutOfRange {
                        key: "rstrip",
                        provided_int: i,
                        error: e,
                    })?;
                }
                ("rstrip", invalid) => {
                    return Err(E::KeyShouldBeInteger {
                        key: "rstrip",
                        type_str: invalid.type_str(),
                    })
                }
                (arbitrary_key, value) => match arbitrary_key.parse::<usize>() {
                    Ok(index_specifying_key) => {
                        // Now to determine if the index is in or out of range.
                        if !(0..current_base_array_length).contains(&index_specifying_key) {
                            return Err(E::IndexOverrideOutOfRange {
                                index: index_specifying_key,
                                input_array_len: current_base_array_length,
                            });
                        }
                        match value {
                            toml::Value::String(s) if s.as_str() == constants::DELETE_OPTION => {
                                accumulated_state
                                    .index_deletions
                                    .insert(index_specifying_key);
                            }
                            specified_index_override => {
                                accumulated_state
                                    .index_overrides
                                    .insert(index_specifying_key, specified_index_override);
                            }
                        }
                    }
                    Err(_) => return Err(E::UnknownKey { key }),
                },
            }
        }
        Ok(accumulated_state)
    }

    /// Try to actually apply this to an existing array of overrides.
    pub(crate) fn apply_to_array(
        self,
        mut overrides: Vec<OverrideValue>,
    ) -> Result<Vec<OverrideValue>, ActionApplyError> {
        let len_of_original_array = overrides.len();
        // Range of indices in the original array *not* to strip.
        if (self.lstrip + self.rstrip) > overrides.len() {
            return Err(ActionApplyErrorInner::LRStripTooLarge {
                lstrip_count: self.lstrip,
                rstrip_count: self.rstrip,
                input_array_len: len_of_original_array,
            }
            .into());
        }

        let nostrip_index_range = self.lstrip..(len_of_original_array - self.rstrip);
        let original_array_start_offset_inc = self.prepends.len();
        let original_array_backwards_end_offset = self.appends.len();

        let mut full: Vec<_> = self
            .prepends
            .into_iter()
            .map(OverrideValue::with_only_base_value)
            .collect();
        full.append(&mut overrides);
        full.extend(
            self.appends
                .into_iter()
                .map(OverrideValue::with_only_base_value),
        );

        let original_array_end_offset_excl = full.len() - original_array_backwards_end_offset;
        let original_array_index_range =
            original_array_start_offset_inc..original_array_end_offset_excl;
        // Original array region
        let original_array_region: &mut [OverrideValue] =
            &mut full[original_array_index_range.clone()];

        // Apply the overrides specified with ourselves, including with the offset
        for (index, override_value) in self.index_overrides.into_iter() {
            let curr_override_set = original_array_region.get_mut(index).ok_or_else(|| {
                ActionApplyErrorInner::InvalidExplicitIndex {
                    index,
                    input_array_len: len_of_original_array,
                }
            })?;
            curr_override_set.add_another_override(override_value);
        }

        // Perform deletions
        // Relies on this current index tracker.
        let mut curr_idx = 0;
        full.retain(|_| {
            let index = curr_idx;
            curr_idx += 1;
            let curr_idx = index;
            if original_array_index_range.contains(&curr_idx) {
                let idx_in_original_array = curr_idx - original_array_start_offset_inc;
                let strip_excluded = !nostrip_index_range.contains(&idx_in_original_array);
                let del_set_excluded = self.index_deletions.contains(&idx_in_original_array);
                !(strip_excluded || del_set_excluded)
            } else {
                // None of our deletions apply to anything outside the original array
                true
            }
        });
        Ok(full)
    }
}

/// Error when turning a toml table into an [ArrayNodeSeqAction]
#[derive(Debug, Error)]
pub enum ActionParseError {
    #[error("Key {key} should be an array, was actually a {type_str}")]
    KeyShouldBeArray {
        key: &'static str,
        type_str: &'static str,
    },
    #[error("Key {key} should be an integer, was actually a {type_str}")]
    KeyShouldBeInteger {
        key: &'static str,
        type_str: &'static str,
    },
    #[error("Provided {key} value {provided_int} out of usize range: {error}")]
    IntOutOfRange {
        key: &'static str,
        provided_int: i64,
        #[source]
        error: TryFromIntError,
    },
    #[error("Unknown key '{key}' present in map")]
    UnknownKey { key: String },
    #[error("Provided index override at '{index}' out of range ('0' inclusive to '{input_array_len}' exclusive)")]
    IndexOverrideOutOfRange {
        index: usize,
        input_array_len: usize,
    },
}

/// Error when applying an array-mode action to a valid existing array mode state.
#[derive(Error, Debug)]
pub enum ActionApplyErrorInner {
    #[error("Specified an invalid (out of range) index for override or deletion ({index}) - valid values are '0' (inclusive) to '{input_array_len}' (exclusive)")]
    InvalidExplicitIndex {
        index: usize,
        input_array_len: usize,
    },
    #[error("lstrip value {lstrip_count} and rstrip value {rstrip_count} too large - greater than all of input array's length (actual array length: {input_array_len})")]
    LRStripTooLarge {
        lstrip_count: usize,
        rstrip_count: usize,
        input_array_len: usize,
    },
}

#[derive(Debug, Error)]
#[error("{source}")]
pub struct ActionApplyError {
    #[from]
    pub source: ActionApplyErrorInner,
}

/// Module for testing the most complex part of this override process - the array
/// override system.
#[cfg(test)]
mod test {
    use chain_trans::prelude::*;
    use smallvec::smallvec;

    use super::Action;
    use crate::structured_delta::{OverrideValue, SimplePriority};

    // For overrides.
    macro_rules! ivovec {
        ($($expr:expr),+ $(,)?) => {
            vec![$(OverrideValue::with_only_base_value(toml::Value::Integer($expr)),)+]
        };
    }

    macro_rules! ivvec {
        ($($expr:expr),+ $(,)?) => {
            vec![$(toml::Value::Integer($expr),)+]
        };
    }

    macro_rules! ivsmallvec {
        ($($expr:expr),+ $(,)?) => {
            smallvec![$(toml::Value::Integer($expr),)+]
        };
    }

    /// Perform primitive extraction of the "highest priority" value from a vector of overrides.
    ///
    /// Mostly used to provide a simple means of getting a vector for comparison.
    fn primitive_flatten(values: Vec<OverrideValue>) -> Vec<toml::Value> {
        values
            .into_iter()
            .map(SimplePriority::extract_highest_priority_value)
            .collect()
    }

    #[test]
    fn simple_append_prepend() {
        let baseline = ivovec![0, 3, 10, 9, 9i64];

        let action = Action {
            prepends: ivsmallvec![0, 4, 1],
            appends: ivsmallvec![9, 3, 2],
            ..Default::default()
        };

        assert_eq!(
            action
                .apply_to_array(baseline)
                .unwrap()
                .trans(primitive_flatten),
            ivvec![0, 4, 1, 0, 3, 10, 9, 9, 9, 3, 2]
        );
    }
}

// SDSB - Manage initramfs generators, kernels, and self-controlled secure boot.
// Copyright (C) 2023 Matti Bryce <mattibryce at protonmail dot com>
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <https://www.gnu.org/licenses/>.
