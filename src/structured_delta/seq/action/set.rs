use std::collections::HashSet;

use smallvec::SmallVec;
use thiserror::Error;

use crate::structured_delta::{simpletoml, OVERRIDE_STACK_ELEMS};

#[derive(Debug, Default)]
pub(crate) struct Action {
    pub insert: SmallVec<[simpletoml::SimpleTomlValue; OVERRIDE_STACK_ELEMS]>,
    pub remove: SmallVec<[simpletoml::SimpleTomlValue; OVERRIDE_STACK_ELEMS]>,
}

impl Action {
    pub(crate) fn apply_to_set(
        self,
        mut values: HashSet<simpletoml::SimpleTomlValue>,
    ) -> HashSet<simpletoml::SimpleTomlValue> {
        for v in self.insert.into_iter() {
            values.insert(v);
        }
        for v in self.remove.into_iter() {
            values.remove(&v);
        }
        values
    }

    pub(crate) fn try_from_map(map: toml::Table) -> Result<Self, ActionParseError> {
        let mut accumulated_state = Self::default();
        pub(crate) fn append_conv_array<A: smallvec::Array<Item = simpletoml::SimpleTomlValue>>(
            key: &'static str,
            array: Vec<toml::Value>,
            key_vector: &mut SmallVec<A>,
        ) -> Result<(), ActionParseError> {
            key_vector.reserve(array.len());
            for maybe_simple_toml in array.into_iter() {
                // Use try_from rather than try_into here because toml::Value
                // has an inherent impl method also called try_into.
                let simple_toml = simpletoml::SimpleTomlValue::try_from(maybe_simple_toml)
                    .map_err(|err| ActionParseError::InvalidValueTypeForSet { key, err })?;
                key_vector.push(simple_toml);
            }
            Ok(())
        }

        for (map_key, map_value) in map.into_iter() {
            match (map_key.as_str(), map_value) {
                ("insert", toml::Value::Array(a)) => {
                    append_conv_array("insert", a, &mut accumulated_state.insert)?
                }
                ("insert", invalid) => {
                    return Err(ActionParseError::ExpectedArray {
                        key: "insert",
                        toml_type_str: invalid.type_str(),
                    })
                }
                ("remove", toml::Value::Array(a)) => {
                    append_conv_array("remove", a, &mut accumulated_state.remove)?
                }
                ("remove", invalid) => {
                    return Err(ActionParseError::ExpectedArray {
                        key: "remove",
                        toml_type_str: invalid.type_str(),
                    })
                }
                (_unknown_key, _) => return Err(ActionParseError::UnknownKey { key: map_key }),
            }
        }
        Ok(accumulated_state)
    }
}

/// Error when parsing a set mode action from a map.
#[derive(Debug, Error)]
pub enum ActionParseError {
    #[error("Unknown key {key} present in map")]
    UnknownKey { key: String },
    #[error("Expected definition of {key} in a set-mode action to be a toml array, was an {toml_type_str}")]
    ExpectedArray {
        key: &'static str,
        toml_type_str: &'static str,
    },
    #[error("Value in {key} not valid for a set mode array: {err}")]
    InvalidValueTypeForSet {
        key: &'static str,
        #[source]
        err: simpletoml::InvalidSetValueType,
    },
}

// SDSB - Manage initramfs generators, kernels, and self-controlled secure boot.
// Copyright (C) 2023 Matti Bryce <mattibryce at protonmail dot com>
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <https://www.gnu.org/licenses/>.
