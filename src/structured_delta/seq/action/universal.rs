#[cfg(feature = "backtrace")]
use std::backtrace::Backtrace;

use chain_trans::Trans;
use smallvec::SmallVec;
use thiserror::Error;

use super::{array, set};
use crate::{
    fromchain,
    structured_delta::{simpletoml, OVERRIDE_STACK_ELEMS},
};

/// Action encompassing sequence actions (as well as reinits).
#[derive(Debug)]
pub(crate) enum Action {
    ArrayMode(array::Action),
    SetMode(set::Action),
    Reinit(SmallVec<[toml::Value; OVERRIDE_STACK_ELEMS]>),
}

impl Action {
    /// Attempt to pars a universal seq action from a toml value. See the readme for info on the
    /// specifics.
    pub(crate) fn try_from_toml_value(
        value: toml::Value,
        current_base_array_length: usize,
    ) -> Result<Self, ActionParseError> {
        match value {
            toml::Value::Array(simple_value_reset) => Ok(Action::Reinit(simple_value_reset.into())),
            toml::Value::Table(mut raw_action) => {
                let is_set = raw_action
                    .remove("set")
                    .unwrap_or(toml::Value::Boolean(false))
                    .trans(|val| match val {
                        toml::Value::Boolean(extracted) => Ok(extracted),
                        invalid => Err(ActionParseErrorInner::InvalidSetSpecifierType {
                            toml_type_str: invalid.type_str(),
                        }),
                    })?;
                if is_set {
                    set::Action::try_from_map(raw_action)
                        .map(Self::SetMode)
                        .map_err(Into::into)
                } else {
                    array::Action::try_from_map(current_base_array_length, raw_action)
                        .map(Self::ArrayMode)
                        .map_err(Into::into)
                }
            }
            invalid => Err(ActionParseErrorInner::InvalidValueType {
                toml_type_str: invalid.type_str(),
            }
            .into()),
        }
    }
}

/// Error from trying to parse an unknown [`UniversalSeqAction`], which may be array-mode,
/// set-mode, or an array re-init action. See [`ActionParseError`] for the type that captures a
/// backtrace
#[derive(Debug, Error)]
pub enum ActionParseErrorInner {
    #[error(transparent)]
    UnparseableArrayModeAction(#[from] array::ActionParseError),
    #[error(transparent)]
    UnparseableSetModeAction(#[from] set::ActionParseError),
    #[error("Provided toml value was not a valid type (array or map), was {toml_type_str}")]
    InvalidValueType { toml_type_str: &'static str },
    #[error("Provided value for key 'set' was not a boolean (was: {toml_type_str}")]
    InvalidSetSpecifierType { toml_type_str: &'static str },
}

#[cfg(feature = "backtrace")]
#[derive(Debug, Error)]
#[error("{source}")]
pub struct ActionParseError {
    #[from]
    pub source: ActionParseErrorInner,
    #[backtrace]
    pub backtrace: Backtrace,
}

#[cfg(not(feature = "backtrace"))]
#[derive(Debug, Error)]
#[error("{source}")]
pub struct ActionParseError {
    #[from]
    pub source: ActionParseErrorInner,
}

fromchain! {ActionParseError => ActionParseErrorInner => impl From<array::ActionParseError, set::ActionParseError>}

/// Error from trying to apply a specified [`ArrayModeSeqAction`] or [`SetModeSeqAction`] to an
/// existing [`SeqDeserializer`]. Inner component - see [`ActionApplicationError`] for something
/// that captures a backtrace.
#[derive(Debug, Error)]
pub enum ActionApplyErrorInner {
    /// Error when applying a set-mode sequential action onto an unspecified-mode override
    /// array, which happens if one of the values in the initial array is not a valid type.
    #[error("Cannot convert the unspecified array into a set-mode array: {0}")]
    CannotBeSetMode(#[from] simpletoml::InvalidSetValueType),
    #[error("Cannot apply an array-mode sequence action on a set-mode array")]
    ArrayActionSetMode,
    #[error("Cannot apply a set-mode sequence action onto an array-mode array")]
    SetActionArrayMode,
    #[error(transparent)]
    ArrayModeError(#[from] array::ActionApplyError),
}

#[cfg(feature = "backtrace")]
#[derive(Debug, Error)]
#[error("{source}")]
pub struct ActionApplyError {
    #[from]
    pub source: ActionApplyErrorInner,
    #[backtrace]
    pub backtrace: Backtrace,
}

#[cfg(not(feature = "backtrace"))]
#[derive(Debug, Error)]
#[error("{source}")]
pub struct ActionApplyError {
    #[from]
    pub source: ActionApplyErrorInner,
}

fromchain! {
    ActionApplyError => ActionApplyErrorInner =>  impl From<array::ActionApplyError, simpletoml::InvalidSetValueType>
}

// SDSB - Manage initramfs generators, kernels, and self-controlled secure boot.
// Copyright (C) 2023 Matti Bryce <mattibryce at protonmail dot com>
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <https://www.gnu.org/licenses/>.
