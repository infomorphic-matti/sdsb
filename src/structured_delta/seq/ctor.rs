#[cfg(feature = "backtrace")]
use std::backtrace::Backtrace;
use std::collections::HashSet;

use smallvec::SmallVec;
use thiserror::Error;

use super::{action, SeqDeserializer};
use crate::structured_delta::{simpletoml, utils, OverrideValue, OVERRIDE_STACK_ELEMS};

pub(crate) enum SeqDeserializerCtor {
    // Indicates that it has not been specified by override that the value is either an array or
    // set type. Conversion to set type might cause an error.
    UnspecifiedMode(SmallVec<[toml::Value; OVERRIDE_STACK_ELEMS]>),
    ArrayMode(Vec<OverrideValue>),
    SetMode(HashSet<simpletoml::SimpleTomlValue>),
}

/// Error specifically from initializing or re-initializing the state of a seq deserializer ctor
/// (as opposed to parsing actions for it, or applying actions to it).
#[derive(Debug, Error)]
pub enum SeqDeserializerCtorInitError {
    #[error("Missing tuple-map key '{missing_table_index}', needed keys up to (but not including) '{needed_size}'")]
    MissingMapInitKey {
        /// When initializating via a tuple-like map-array construction, we need
        /// each index to have a key in the map. If one doesn't exist, then
        missing_table_index: usize,
        needed_size: usize,
    },
    #[error("Found tuple-map key '{non_valid_key}', should be an integer-string from '0' (inclusive) to '{needed_size}' (non-inclusive)")]
    InvalidMapInitKey {
        non_valid_key: String,
        needed_size: usize,
    },
    #[error("Found {toml_type_str}, expected an array value")]
    InitExpectedArray { toml_type_str: &'static str },
    #[error("Found array of length {actual_length}, needed one of length {needed_length}")]
    InitExpectedArrayOfSize {
        actual_length: usize,
        needed_length: usize,
    },
    #[error("Found {toml_type_str}, expected a tuple-map initializer of length {needed_length} or an array of length {needed_length}")]
    InitExpectedTupleMapOrArrayOfSize {
        toml_type_str: &'static str,
        needed_length: usize,
    },
}

/// Error when fully constructing a sequence deserializer from a raw set of override priority
/// values.
///
/// This includes:
/// * Errors constructing Array sequence actions and Set sequence actions from override values
/// * Errors actually applying them.
#[derive(Debug, Error)]
pub enum SeqDeserializerConstructionError {
    #[error(transparent)]
    FailedToApplyOverrideActions(
        #[cfg_attr(feature = "backtrace", backtrace)]
        #[from]
        action::universal::ActionApplyError,
    ),
    #[error("{0}")]
    OverrideActionParseFailure(
        #[from] action::universal::ActionParseError,
        #[cfg(feature = "backtrace")]
        #[cfg_attr(feature = "backtrace", backtrace)]
        Backtrace,
    ),
    #[error("{0}")]
    InitializationError(
        #[from] SeqDeserializerCtorInitError,
        #[cfg(feature = "backtrace")]
        #[cfg_attr(feature = "backtrace", backtrace)]
        Backtrace,
    ),
}

/// Tell the [`SeqDeserializerCtor`] how to construct itself.
#[derive(Debug, PartialEq, Eq, PartialOrd, Ord, Hash, Clone, Copy, Default)]
pub enum SeqDeserializerCtorInitMechanism {
    /// Don't provide any size hints - this option only allows constructing new sequential
    /// deserializer override values from toml arrays.
    #[default]
    NoSizeHint,
    /// Provide information about the size of the desired array. This option also only
    /// allows constructing new seq deserializer override values from toml arrays.
    ///
    /// This adds validation to the size of the initialization array.
    ArrayOnlySize(usize),
    /// Provide information about the size of the desired value sequence, and also allow
    /// attempts at constructing the base value from a map keyed on stringified versions of the
    /// index values `0` -> `size - 1`. This is analogous to the mechanism used inside
    /// [`toml::value::MapEnumDeserializer`] for tuple variants of enums, but also permits array
    /// values.
    ///
    /// Due to the structure of override files - in particular, the use of maps to also describe
    /// structured override commands for array values - you still can't use this sort of keyed map
    /// to reset the array to a new value. Using an actual array value works fine, however.
    ///
    /// The primary purpose of this is to maintain compatibility with toml's own mechanism for
    /// describing tuples.
    ArrayOrIntKeyedMapSize(usize),
}

impl SeqDeserializerCtorInitMechanism {
    /// The "needed size" of the array/table-init if specified.
    #[inline]
    fn needed_size(&self) -> Option<usize> {
        match self {
            SeqDeserializerCtorInitMechanism::NoSizeHint => None,
            SeqDeserializerCtorInitMechanism::ArrayOnlySize(v) => Some(*v),
            SeqDeserializerCtorInitMechanism::ArrayOrIntKeyedMapSize(v) => Some(*v),
        }
    }

    /// Validate a value for being an array of the right size if that is
    /// specified as a requirement.
    ///
    /// Does not work for the tuple table mechanism. That can only be used with [`SeqDeserializerCtorInitMechanism::init_base_value`],
    /// as it doesn't work for anything other than the "base" value (beyond that, maps are
    /// interpreted as constructing seqactions.
    #[inline]
    fn validate_array_len(
        &self,
        array: &[toml::Value],
    ) -> Result<(), SeqDeserializerCtorInitError> {
        let needed_size = self.needed_size().unwrap_or(array.len());
        if needed_size == array.len() {
            // Always true in the case that no needed array size was specified.
            Ok(())
        } else {
            Err(SeqDeserializerCtorInitError::InitExpectedArrayOfSize {
                actual_length: array.len(),
                needed_length: needed_size,
            })
        }
    }

    /// Try to match if the value is an array, and if so produce an initial state.
    ///
    /// Note that this is not capable of doing the table-type array construction and you should
    /// check ahead of time for the value being a map and call out to that when that is a necessary
    /// capability (more particularly, see [`SeqDeserializerCtorInitMechanism::init_base_value`]
    ///
    /// Essentially, this is a sort of semi-internal function.
    fn validate_array_ctor(
        &self,
        maybe_array: toml::Value,
    ) -> Result<SeqDeserializerCtor, SeqDeserializerCtorInitError> {
        use SeqDeserializerCtorInitError as E;
        match maybe_array {
            toml::Value::Array(a) => self
                .validate_array_len(a.as_ref())
                .map(|_| SeqDeserializerCtor::UnspecifiedMode(a.into())),

            v => Err(E::InitExpectedArray {
                toml_type_str: v.type_str(),
            }),
        }
    }

    /// Initialize the constructor with a base (unoverridden) value, using the method selected with
    /// the enum.
    ///
    /// Naturally, this can't apply any overrides, because it doesn't have access to them yet.
    fn init_base_value(
        &self,
        base_value: toml::Value,
    ) -> Result<SeqDeserializerCtor, SeqDeserializerCtorInitError> {
        use SeqDeserializerCtorInitError as E;
        use SeqDeserializerCtorInitMechanism as S;
        match *self {
            S::NoSizeHint | S::ArrayOnlySize(_) => self.validate_array_ctor(base_value),
            S::ArrayOrIntKeyedMapSize(needed_size) => {
                match base_value {
                    // Perform array validation using existing code
                    array_v @ toml::Value::Array(_) => self.validate_array_ctor(array_v),
                    toml::Value::Table(t) => {
                        SeqDeserializerCtor::try_tuple_map_style_init(needed_size, t)
                    }
                    v => Err(E::InitExpectedTupleMapOrArrayOfSize {
                        toml_type_str: v.type_str(),
                        needed_length: needed_size,
                    }),
                }
            }
        }
    }
}

impl SeqDeserializerCtor {
    /// Apply a set-mode action to this.
    ///
    /// In case the mode is currently unspecified, this implies that the array is of Set type, and
    /// will attempt conversion.
    fn apply_set_mode_action(
        self,
        set_mode_action: action::set::Action,
    ) -> Result<Self, action::universal::ActionApplyError> {
        match self {
            SeqDeserializerCtor::UnspecifiedMode(raw_vs) => {
                let new_set: HashSet<_> = raw_vs
                    .into_iter()
                    .map(simpletoml::SimpleTomlValue::try_from)
                    .collect::<Result<_, _>>()
                    .map_err(action::universal::ActionApplyErrorInner::CannotBeSetMode)?;
                let new_set = set_mode_action.apply_to_set(new_set);
                Ok(Self::SetMode(new_set))
            }
            SeqDeserializerCtor::ArrayMode(_) => {
                Err(action::universal::ActionApplyErrorInner::SetActionArrayMode.into())
            }
            SeqDeserializerCtor::SetMode(s) => Ok(Self::SetMode(set_mode_action.apply_to_set(s))),
        }
    }

    /// Apply an array-mode action to this.
    ///
    /// If the mode is currently unspecified, this implies that the array is of Array-mode type and
    /// will convert before attempting to apply the action.
    fn apply_array_mode_action(
        self,
        array_mode_action: action::array::Action,
    ) -> Result<Self, action::universal::ActionApplyError> {
        match self {
            SeqDeserializerCtor::UnspecifiedMode(raw_vs) => {
                let new_array: Vec<_> = raw_vs
                    .into_iter()
                    .map(OverrideValue::with_only_base_value)
                    .collect();
                let new_array = array_mode_action.apply_to_array(new_array)?;
                Ok(Self::ArrayMode(new_array))
            }
            SeqDeserializerCtor::ArrayMode(vs) => {
                Ok(Self::ArrayMode(array_mode_action.apply_to_array(vs)?))
            }
            SeqDeserializerCtor::SetMode(_) => {
                Err(action::universal::ActionApplyErrorInner::ArrayActionSetMode.into())
            }
        }
    }

    /// Length of the array implied by the current state of the ctor.
    #[inline]
    fn curr_len(&self) -> usize {
        match self {
            SeqDeserializerCtor::UnspecifiedMode(v) => v.len(),
            SeqDeserializerCtor::ArrayMode(a) => a.len(),
            SeqDeserializerCtor::SetMode(s) => s.len(),
        }
    }

    /// Attempt to perform the tuple-map style initialization, as documented in
    /// [`SeqDeserializerCtorInitMechanism`] and the implementation in
    /// [`toml::value::MapEnumDeserializer`].
    fn try_tuple_map_style_init(
        needed_size: usize,
        mut input_table: toml::Table,
    ) -> Result<Self, SeqDeserializerCtorInitError> {
        use SeqDeserializerCtorInitError as E;
        let mut values = SmallVec::with_capacity(needed_size);
        for index in 0..needed_size {
            let key = utils::accelerated_int_string(index);
            let value_for_index =
                input_table
                    .remove(key.as_ref())
                    .ok_or_else(|| E::MissingMapInitKey {
                        missing_table_index: index,
                        needed_size,
                    })?;
            values.push(value_for_index)
        }
        // Check that the table doesn't have extra keys.
        if let Some((excess_key, _)) = input_table.into_iter().next() {
            return Err(E::InvalidMapInitKey {
                non_valid_key: excess_key,
                needed_size,
            });
        }

        Ok(Self::UnspecifiedMode(values))
    }

    /// Construct a new sequence deserializer ctor from overrides.
    ///
    /// See the documentation of [`SeqDeserializerCtorInitMechanism`] for information on what this
    /// parameter is for.
    pub(crate) fn new_from_overrides(
        override_outer_value: OverrideValue,
        initialization_and_size_mode: SeqDeserializerCtorInitMechanism,
    ) -> Result<Self, SeqDeserializerConstructionError> {
        // This works in a similar way as the enum variant override system, except instead of
        // resetting based on new variant tags, we reset based on array values rather than on new
        // single variant keys.
        let (mut curr_state, unparsed_override_actions) = (
            initialization_and_size_mode.init_base_value(override_outer_value.base_value)?,
            override_outer_value.overrides,
        );

        // Attempt to parse the override values appropriately.
        for potential_override_action in unparsed_override_actions.into_iter().flatten() {
            let action = action::universal::Action::try_from_toml_value(
                potential_override_action,
                curr_state.curr_len(),
            )?;
            curr_state = match action {
                action::universal::Action::ArrayMode(array_action) => {
                    curr_state.apply_array_mode_action(array_action)?
                }
                action::universal::Action::SetMode(set_action) => {
                    curr_state.apply_set_mode_action(set_action)?
                }
                action::universal::Action::Reinit(new_array) => {
                    initialization_and_size_mode.validate_array_len(&new_array)?;
                    Self::UnspecifiedMode(new_array)
                }
            };
        }
        Ok(curr_state)
    }

    /// Construct an actual sequential deserializer out of the components making up this
    /// contructor.
    pub(crate) fn make_seq_deserializer(
        self,
    ) -> SeqDeserializer<
        impl Iterator<Item = toml::Value>,
        impl Iterator<Item = OverrideValue>,
        impl Iterator<Item = toml::Value>,
    > {
        match self {
            SeqDeserializerCtor::UnspecifiedMode(simple_v) => {
                SeqDeserializer::SimpleArrayMode(simple_v.into_iter())
            }
            SeqDeserializerCtor::ArrayMode(override_array) => {
                SeqDeserializer::ArrayMode(override_array.into_iter())
            }
            SeqDeserializerCtor::SetMode(simple_toml_set) => SeqDeserializer::SetMode(
                simple_toml_set
                    .into_iter()
                    .map(simpletoml::SimpleTomlValue::into),
            ),
        }
    }
}

// SDSB - Manage initramfs generators, kernels, and self-controlled secure boot.
// Copyright (C) 2023 Matti Bryce <mattibryce at protonmail dot com>
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <https://www.gnu.org/licenses/>.
