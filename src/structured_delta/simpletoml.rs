use thiserror::Error;

/// Toml value types that can be used in a set.
#[derive(PartialEq, Eq, PartialOrd, Ord, Hash, Debug)]
pub enum SimpleTomlValue {
    String(String),
    Integer(i64),
    Boolean(bool),
}

impl From<SimpleTomlValue> for toml::Value {
    #[inline]
    fn from(v: SimpleTomlValue) -> toml::Value {
        match v {
            SimpleTomlValue::String(s) => toml::Value::String(s),
            SimpleTomlValue::Integer(i) => toml::Value::Integer(i),
            SimpleTomlValue::Boolean(b) => toml::Value::Boolean(b),
        }
    }
}

/// Error indicating that the given toml value type can't be used in `set`-type
/// array overrides.
#[derive(Debug, Clone, Copy, PartialEq, Eq, PartialOrd, Ord, Error)]
#[error("Invalid toml type {0} for performing set-like overrides")]
pub struct InvalidSetValueType(&'static str);

impl TryFrom<toml::Value> for SimpleTomlValue {
    type Error = InvalidSetValueType;

    #[inline]
    fn try_from(value: toml::Value) -> Result<Self, Self::Error> {
        match value {
            toml::Value::String(s) => Ok(Self::String(s)),
            toml::Value::Integer(i) => Ok(Self::Integer(i)),
            toml::Value::Boolean(b) => Ok(Self::Boolean(b)),
            non_set_friendly => Err(InvalidSetValueType(non_set_friendly.type_str())),
        }
    }
}

impl From<String> for SimpleTomlValue {
    #[inline]
    fn from(value: String) -> Self {
        Self::String(value)
    }
}

impl From<bool> for SimpleTomlValue {
    #[inline]
    fn from(value: bool) -> Self {
        Self::Boolean(value)
    }
}

impl From<i64> for SimpleTomlValue {
    #[inline]
    fn from(value: i64) -> Self {
        Self::Integer(value)
    }
}

// SDSB - Manage initramfs generators, kernels, and self-controlled secure boot.
// Copyright (C) 2023 Matti Bryce <mattibryce at protonmail dot com>
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <https://www.gnu.org/licenses/>.
