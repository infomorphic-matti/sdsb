//! Module for visiting override-structures.
//!
//! This is different than simple maps because we can provide better error messages and simpler
//! code when we have a list of fields.

use chain_trans::Trans;
use serde::de::{self, IntoDeserializer};
use smallvec::SmallVec;
use thiserror::Error;

use super::{DeserializerError, OverrideValue, OVERRIDE_STACK_ELEMS};

/// [`serde::de::MapAccess`] tailored specifically for structs. Takes advantage of knowing the list
/// of fields.
///
/// Using this with zero specified fields results in deserializing a normal map.
pub(crate) struct StructureDeserializer {
    /// Next field in the list of field names to attempt to access.
    ///
    /// Should start at zero. Incremented on every `next_key_seed`, to the next key to try.
    ///
    /// Note that some keys may not have a value and hence should be skipped.
    ///
    /// When this is >= to the length of the fields, reduce to generic map traversal behaviour.
    /// When it's done, this is set to None
    next_field_index: Option<usize>,
    fields: &'static [&'static str],
    /// Available table values. Non-table values are not allowed for normal structs.
    /// This functions as a form of override, where the last value is highest priority.
    /// When constructing this, non-present table values should be eliminated.
    table_hierarchy: SmallVec<[toml::Table; OVERRIDE_STACK_ELEMS]>,
    /// The value for the most recent key found that actually has at least one entry in the table hierarchy
    /// containing a base value.
    just_discovered_override_value: Option<OverrideValue>,
}

impl StructureDeserializer {
    /// Construct a new deserializer that can take advantage of known map structure.
    #[inline]
    pub fn create(
        table_hierarchy: SmallVec<[toml::Table; OVERRIDE_STACK_ELEMS]>,
        known_fields: &'static [&'static str],
    ) -> Self {
        Self {
            next_field_index: Some(0),
            fields: known_fields,
            table_hierarchy,
            just_discovered_override_value: None,
        }
    }

    /// Try to construct a new deserializer from some override values.
    pub fn try_create(
        overrides: OverrideValue,
        known_fields: &'static [&'static str],
    ) -> Result<Self, StructureDeserializerCreationError> {
        let table_hierarchy = overrides
            .into_upwards_priority_value_iterator()
            .map(|v| match v {
                toml::Value::Table(t) => Ok(t),
                invalid => Err(StructureDeserializerCreationError::NonTableValue {
                    toml_type_str: invalid.type_str(),
                }),
            })
            .collect::<Result<SmallVec<_>, _>>()?;
        Ok(Self::create(table_hierarchy, known_fields))
    }

    /// Create a deserializer for a map - this is equivalent to a structure with zero known fields
    /// for efficient lookup of the structure keys first.
    #[inline]
    pub fn try_create_map(
        overrides: OverrideValue,
    ) -> Result<Self, StructureDeserializerCreationError> {
        Self::try_create(overrides, &[])
    }
}

/// Error when trying to initialize a structure deserializer from a set of override values.
#[derive(Debug, Error)]
pub enum StructureDeserializerCreationError {
    #[error(
        "Non-table value (type: {toml_type_str}) used when trying to define or override map value"
    )]
    NonTableValue { toml_type_str: &'static str },
}

impl<'de> de::MapAccess<'de> for StructureDeserializer {
    type Error = DeserializerError;

    fn next_key_seed<K>(&mut self, seed: K) -> Result<Option<K::Value>, Self::Error>
    where
        K: de::DeserializeSeed<'de>,
    {
        'found_next_kv: loop {
            let Some(next_value_index) = self.next_field_index.as_mut() else {
                // In this case we already finished
                break 'found_next_kv Ok(None);
            };

            // On each iteration, we strip out all empty tables unconditionally.
            self.table_hierarchy.retain(|tbl| !tbl.is_empty());

            // If there are no tables to extract values from then we are also finished.
            if self.table_hierarchy.is_empty() {
                // we are finished
                self.next_field_index = None;
                break 'found_next_kv Ok(None);
            }

            if *next_value_index < self.fields.len() {
                // We are within 'known fields'.
                let field_name = self.fields[*next_value_index];
                // Increment the next_value_index for whoever gets this thing next.
                *next_value_index += 1;
                // Scan through the tables in priority order to build up an override value
                // for this key. If no override values exist then this field is unspecified
                // anywhere and we have to try the next one (afaik the Option in the result is only
                // for when there are no more keys anywhere in future at all).
                let maybe_override_set = self
                    .table_hierarchy
                    .iter_mut()
                    .map(|tbl| tbl.remove(field_name))
                    .flatten()
                    .trans(OverrideValue::from_iter);
                if let Some(field_overrides) = maybe_override_set {
                    if !field_overrides.is_deleted() {
                        self.just_discovered_override_value = Some(field_overrides);
                        // We have our next value - break out of the loop with the parsed key
                        break 'found_next_kv seed
                            .deserialize(field_name.into_deserializer())
                            .map(Some);
                    };
                };
                // This was not an ok key, so the loop continues onward.
            } else {
                // This is the part where we go into generic key traversal mode.
                // We need to traverse *all* keys made available by *any* overriding table.
                // In every key, we do the same as above and remove the key entries from any table
                // that has one for the given key.
                //
                // To effectively traverse all the tables, we work up the priority list - first
                // emptying the lowest-priority table of all keys, then the second-lowest, and so
                // forth, popping each table as it becomes empty.
                //
                // In practice, we just retain only non-empty tables in all cases for all
                // iterations.
                //
                // At this point we know that all the tables present are non-empty. If there are no
                // more tables, we're done. But we already checked for that.
                let base_table = &mut self.table_hierarchy[0];
                // Unfortunately, we can't just "take an arbitrary K/V pair" from the table
                // Furthermore, we can't turn it into a by-value iterator and then back again
                // So unfortunately, we have to clone a key :(
                let next_key = base_table
                    .keys()
                    .next()
                    .expect("All tables are non-empty")
                    .clone();
                // Perform bog-standard override stuff
                let override_set = self
                    .table_hierarchy
                    .iter_mut()
                    .map(|tbl| tbl.remove(&next_key))
                    .flatten()
                    .trans(OverrideValue::from_iter)
                    .expect("Base value must exist because first table is non-empty");

                if !override_set.is_deleted() {
                    // Then this is a valid key and value and should be returned
                    self.just_discovered_override_value = Some(override_set);
                    // We found our next refined k/v pair.
                    //
                    // Parse the key and break
                    break 'found_next_kv seed.deserialize(next_key.into_deserializer()).map(Some);
                }
            }
        }
    }

    #[inline]
    fn next_value_seed<V>(&mut self, seed: V) -> Result<V::Value, Self::Error>
    where
        V: de::DeserializeSeed<'de>,
    {
        match self.just_discovered_override_value.take() {
            Some(override_value) => seed.deserialize(override_value),
            None => Err(de::Error::custom(
                "value is missing (no next_key_seed call?)",
            )),
        }
    }
}

// SDSB - Manage initramfs generators, kernels, and self-controlled secure boot.
// Copyright (C) 2023 Matti Bryce <mattibryce at protonmail dot com>
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <https://www.gnu.org/licenses/>.
