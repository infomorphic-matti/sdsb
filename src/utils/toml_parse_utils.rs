//! Utils for handling toml files

use super::vecunify::UniVector;

/// Indicate what to do with the next table from an iterator of overrides when extracting a value.
#[derive(Debug, Clone)]
pub enum OverridingAction<V, O, E> {
    /// Reset the chain of table overrides to start with a new base table with a new extracting
    /// value.
    Rebase { new_value: V },
    /// Add this table to the chain of override values and supply a value to override the existing
    /// extracted value with
    Override { modifier: O },
    /// Indicate a failure to actually extract either a new initial value from the table or a
    /// failure to extract an override from the table.
    Fail(E),
}

#[derive(Debug, Clone)]
pub enum OverrideSetBuilderError<ExtractionError, ApplicationError> {
    Extraction(ExtractionError),
    Application(ApplicationError),
}

/// Override set with an extracted value.
pub struct OverrideSetOutput<Overrides: UniVector<toml::Table>, Extracted> {
    /// Base extracted toml table
    pub base_table: toml::Table,
    /// Tables that override the base table, from lowest priority to highest.
    pub overrides: Overrides,
    /// Extracted value
    pub value: Extracted,
}

/// Build an override set, and also simultaneously extract some simple overriden property
///
/// `toml_components` - The actual toml tables to extract the given overrides from
/// `take_initial_value_and_override_determinant` - Take the initial value to be extracted from the
///   first found base table. The table is mutable, since it's often desirable to remove the
///   relevant keys for parsing the resulting override table set again.
/// `take_modification_or_rebase_and_override_determinant` - Get whether or not to override or
///   rebase (reset to new initial table and value) from the given table. This should perform any
///   modifications to the table for when that table is returned in the override set.
/// `try_apply_override` - In the case of having extracted an override that should be applied, this indicates how to apply
///   it to the existing extracted value.
pub fn build_override_set_and_extract_property<
    Extracted,
    ExtractOverrideError,
    Override,
    ApplyOverrideError,
    S: UniVector<toml::Table>,
>(
    toml_components: impl IntoIterator<Item = toml::Table>,
    take_initial_value_and_override_determinant: impl FnOnce(
        &mut toml::Table,
    )
        -> Result<Extracted, ExtractOverrideError>,
    mut take_modification_or_rebase_and_override_determinant: impl FnMut(
        &mut toml::Table,
    ) -> OverridingAction<
        Extracted,
        Override,
        ExtractOverrideError,
    >,
    mut try_apply_override: impl FnMut(Extracted, Override) -> Result<Extracted, ApplyOverrideError>,
) -> Result<
    Option<OverrideSetOutput<S, Extracted>>,
    OverrideSetBuilderError<ExtractOverrideError, ApplyOverrideError>,
> {
    let mut toml_components = toml_components.into_iter();
    let mut base_toml_value = match toml_components.next() {
        Some(v) => v,
        None => return Ok(None),
    };
    let mut overriding_toml_values = S::new();
    let mut extracted_value = take_initial_value_and_override_determinant(&mut base_toml_value)
        .map_err(OverrideSetBuilderError::Extraction)?;

    for mut next_table in toml_components {
        match take_modification_or_rebase_and_override_determinant(&mut next_table) {
            OverridingAction::Rebase { new_value } => {
                base_toml_value = next_table;
                overriding_toml_values.clear();
                extracted_value = new_value;
            }
            OverridingAction::Override { modifier } => {
                overriding_toml_values.extend(core::iter::once(next_table));
                extracted_value = try_apply_override(extracted_value, modifier)
                    .map_err(OverrideSetBuilderError::Application)?;
            }
            OverridingAction::Fail(e) => return Err(OverrideSetBuilderError::Extraction(e)),
        }
    }

    Ok(Some(OverrideSetOutput {
        base_table: base_toml_value,
        overrides: overriding_toml_values,
        value: extracted_value,
    }))
}

/// Modifier for overriding with map entries.
///
/// Has a variant for deletion
#[derive(Debug, Clone)]
pub enum EntryModifier<T> {
    NewData(T),
    Delete,
    NoChange,
}

impl<T> EntryModifier<T> {
    /// Reduce the value back to an option when in a base table. In particular, this turns
    /// `del`-mode items into [`None`] as there is nothing to delete.
    #[inline]
    pub fn reduce_in_base(self) -> Option<T> {
        match self {
            EntryModifier::NewData(v) => Some(v),
            EntryModifier::Delete => None,
            EntryModifier::NoChange => None,
        }
    }

    /// Apply this change to an optional value - note that this does not do complex overrides of
    /// internal structure of `T`, only a superficial replacement.
    #[inline]
    pub fn simple_apply(self, existing_value: Option<T>) -> Option<T> {
        match self {
            EntryModifier::NewData(v) => Some(v),
            EntryModifier::Delete => None,
            EntryModifier::NoChange => existing_value,
        }
    }
}

// SDSB - Manage initramfs generators, kernels, and self-controlled secure boot.
// Copyright (C) 2023 Matti Bryce <mattibryce at protonmail dot com>
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <https://www.gnu.org/licenses/>.
